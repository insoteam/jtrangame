package majiang;


import com.beimi.core.BMDataContext;
import com.beimi.core.engine.game.ActionTaskUtils;
import com.beimi.core.engine.game.CardType;
import com.beimi.core.engine.game.SearchCardType;
import com.beimi.util.rules.model.card.Card;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.MultiMap;
import org.junit.*;
import org.junit.Test;

import java.util.Collection;
import java.util.Map;

/**
 * @author huangzh on 2017-10-20 17:38
 * @email seenet2004@163.com
 * 说明：
 */
public class TackCardsTest {
    @Test
    public void testa(){
        Integer enable = Integer.getInteger("True");
        System.out.println(enable);
    }
    @Test
    public void testmutimap() {
        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();
        MultiMap<String, String> map = hazelcastInstance.getMultiMap("map");

        map.put("a", "1");
        map.put("a", "2");
        map.put("b", "3");
        System.out.println("PutMember:Done");

        hazelcastInstance.getMultiMap("map").put("a", "1");

        MultiMap<String, String> map2 = hazelcastInstance.getMultiMap("map");
        for (String key : map2.keySet()) {
            Collection<String> values = map2.get(key);
            System.out.println(String.format("%s -> %s\n", key, values));
        }

    }

    @Test
    public void testAutoPlayCard() {
        byte[] a = new byte[]{49, 43, 39, 36, 34, 32, 30, 28, 24, 23, 17, 13, 11, 10, 9, 7, 4, 3, 2, 0};
        byte[] b = new byte[]{51, 47, 46, 45, 42, 41, 35, 33, 31, 22, 21, 20, 19, 18, 8, 6, 1};
        byte[] c = new byte[]{53, 52, 50, 48, 44, 40, 38, 37, 29, 27, 26, 25, 16, 15, 14, 12, 5};
    }

    /**
     * 测试压制对手牌是否正确
     */
    @Test
    public void testSearchSupperCardType() {
        tstsupper(new byte[]{50,47, 46, 43, 41, 34, 32, 29, 19, 17, 10, 9, 7, 4}, new byte[]{51});  //FIXME 玩家出2,对方出2 居然也可以，得改
        tstsupper(new byte[]{53,52,44,42,41,40,38,33,32,22,20,15,11,9,2}, new byte[]{27,26,25,24});
        tstsupper(new byte[]{53,42}, new byte[]{52});
        tstsupper(new byte[]{53,43,42,40,35,34,32,31,29,23,17,15,14,12,8,7}, new byte[]{52});  //大鬼压小鬼

        tstsupper(new byte[]{53, 52}, new byte[]{45, 44});
        tstsupper(new byte[]{47, 45, 43, 35, 23, 21, 20, 16, 15, 14, 12}, new byte[]{52});
        tstsupper(new byte[]{53, 52, 46, 44, 31, 30, 28}, new byte[]{42});
        tstsupper(new byte[]{51, 49, 46, 45, 44, 42, 40, 38, 36, 22, 21}, new byte[]{0, 1, 3, 20});
        tstsupper(new byte[]{53, 52, 50, 48, 43, 32, 28, 23, 19, 18, 11, 9, 8, 7, 6, 4, 2}, new byte[]{15, 14, 13, 12});
        tstsupper(new byte[]{50, 48, 42, 37, 35, 32, 21, 19, 18, 17, 16, 14, 13, 9, 8, 7, 1}, new byte[]{0, 4, 5, 6});
        tstsupper(new byte[]{52, 49, 42, 40, 38, 32, 31, 30, 29, 21, 19, 18, 14, 12, 5, 4, 2}, new byte[]{9, 20, 22, 23});
        tstsupper(new byte[]{47, 45, 43, 40, 39, 37, 35, 33, 31, 29, 26, 21, 15, 13, 9, 8}, new byte[]{53});

        tstsupper(new byte[]{3, 13, 31, 42}, new byte[]{15});
        tstsupper(new byte[]{3, 13, 31, 42}, new byte[]{16});
        tstsupper(new byte[]{3, 13, 31, 42}, new byte[]{30});
        tstsupper(new byte[]{3, 13, 31, 42}, new byte[]{14, 15});
        tstsupper(new byte[]{3, 13, 31, 42, 43, 44}, new byte[]{14, 15});
        tstsupper(new byte[]{2, 3, 13, 31, 42, 43, 44}, new byte[]{14, 15});
        tstsupper(new byte[]{2, 3, 13, 31, 42, 43, 44}, new byte[]{12, 14, 15});
        tstsupper(new byte[]{2, 3, 13, 31, 42, 43, 44, 52, 53}, new byte[]{12, 14, 15});

    }

    private void tstsupper(byte[] handCards, byte[] lastCards) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("===================================================").append("\n");

        buffer.append("上家出牌：").append(Card.getInstance().cardsToString(lastCards)).append("");
        buffer.append("         牌型:");

        CardType lastCardType = ActionTaskUtils.identification(lastCards);
        if (lastCardType == null) {
            buffer.append("  不是正确的牌型！").append("\n");
            System.out.println(buffer.toString());
            assert false;
            return;
        } else {
            buffer.append(lastCardType.getCardTypeEnum().getName());
        }
        System.out.println(buffer.toString());

        System.out.println("手牌：" + Card.getInstance().cardsToString(handCards));

        CardType cardType = SearchCardType.getInstance().getMinSuppressCardType(handCards, lastCardType);

        if (cardType != null) {
            System.out.println("管住：" + Card.getInstance().cardsToString(cardType.getCards()) + "            牌型：" + cardType.getCardTypeEnum().getName());
        } else {
            System.out.println("不要");
        }
    }

    @org.junit.Test
    /**
     * 测试出牌是否正确
     */
    public void testCardType() {
        // com.beimi.core.engine.game.ActionTaskUtils.identification()
        testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 33, 35, 37}, BMDataContext.CardsTypeHEnum.FOUR_LINK_TWOSINGLE);

        testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 36, 37, 40}, BMDataContext.CardsTypeHEnum.FOUR_LINK_SINGLE);

        testCardTypeIsRight(new byte[]{25, 22}, null);

        testCardTypeIsRight(new byte[]{46, 28, 27, 18, 17, 12, 11, 10, 8, 19, 15, 13}, BMDataContext.CardsTypeHEnum.THREE_LINK_SINGLE);
        if (true) {
            testCardTypeIsRight(new byte[]{0}, BMDataContext.CardsTypeHEnum.SINGLE);
            testCardTypeIsRight(new byte[]{14}, BMDataContext.CardsTypeHEnum.SINGLE);
            testCardTypeIsRight(new byte[]{52}, BMDataContext.CardsTypeHEnum.SINGLE);
            testCardTypeIsRight(new byte[]{53}, BMDataContext.CardsTypeHEnum.SINGLE);
            testCardTypeIsRight(new byte[]{10, 15, 17, 22, 24}, BMDataContext.CardsTypeHEnum.SINGLE_LINK);
            testCardTypeIsRight(new byte[]{10, 15, 17, 22, 24, 28}, BMDataContext.CardsTypeHEnum.SINGLE_LINK);
            testCardTypeIsRight(new byte[]{22, 23}, BMDataContext.CardsTypeHEnum.PAIR);
            testCardTypeIsRight(new byte[]{22, 23, 24, 25, 28, 29}, BMDataContext.CardsTypeHEnum.PAIR_LINK);
            testCardTypeIsRight(new byte[]{22, 23, 24, 25, 28, 29, 32, 33}, BMDataContext.CardsTypeHEnum.PAIR_LINK);
            testCardTypeIsRight(new byte[]{20, 21, 22}, BMDataContext.CardsTypeHEnum.THREE);
            testCardTypeIsRight(new byte[]{20, 21, 24}, null);
            testCardTypeIsRight(new byte[]{20, 21, 22, 24}, BMDataContext.CardsTypeHEnum.THREE_SINGLE);
            testCardTypeIsRight(new byte[]{20, 21, 22, 24, 25}, BMDataContext.CardsTypeHEnum.THREE_PAIR);
            testCardTypeIsRight(new byte[]{20, 21, 22, 24, 25, 26}, BMDataContext.CardsTypeHEnum.THREE_LINK);
            testCardTypeIsRight(new byte[]{20, 21, 22, 24, 25, 26, 28}, null);

            testCardTypeIsRight(new byte[]{20, 21, 22, 24, 25, 26, 28, 29}, BMDataContext.CardsTypeHEnum.THREE_LINK_SINGLE);  //带单对的当成带单
            testCardTypeIsRight(new byte[]{20, 21, 22, 24, 25, 26, 28, 32}, BMDataContext.CardsTypeHEnum.THREE_LINK_SINGLE);  //带两单
            testCardTypeIsRight(new byte[]{20, 21, 22, 24, 25, 26, 28, 29, 32, 33}, BMDataContext.CardsTypeHEnum.THREE_LINK_PAIR);

            //四
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24}, BMDataContext.CardsTypeHEnum.FOUR_SINGLE);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 33}, BMDataContext.CardsTypeHEnum.FOUR_TWOSINGLE);

            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25}, BMDataContext.CardsTypeHEnum.FOUR_PAIR);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 33, 34}, BMDataContext.CardsTypeHEnum.FOUR_TWOPAIR);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 33, 34, 44, 45}, null);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 33, 34, 44}, null);

            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27}, BMDataContext.CardsTypeHEnum.FOUR_LINK);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28}, null);

            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 33}, BMDataContext.CardsTypeHEnum.FOUR_LINK_SINGLE);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 29}, BMDataContext.CardsTypeHEnum.FOUR_LINK_SINGLE);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 36, 37, 40}, BMDataContext.CardsTypeHEnum.FOUR_LINK_SINGLE);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 33, 37, 40}, BMDataContext.CardsTypeHEnum.FOUR_LINK_TWOSINGLE);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 33, 35, 37}, BMDataContext.CardsTypeHEnum.FOUR_LINK_TWOSINGLE);

            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 33, 34}, BMDataContext.CardsTypeHEnum.FOUR_LINK_PAIR);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 36, 37}, BMDataContext.CardsTypeHEnum.FOUR_LINK_PAIR);
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 36, 37, 40, 41, 2, 3}, BMDataContext.CardsTypeHEnum.FOUR_LINK_PAIR);

            //黑桃8,红桃8,草花8,方块8,黑桃9,红桃9,草花9,方块9,黑桃10,红桃10,草花10,方块10,黑桃Q,红桃Q,黑桃K,红桃K 这种情况 不知道有没有这么搞的
            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 36, 37, 40, 41}, BMDataContext.CardsTypeHEnum.FOUR_LINK_TWOPAIR);

            testCardTypeIsRight(new byte[]{20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 33, 34, 36, 37, 40, 41}, BMDataContext.CardsTypeHEnum.FOUR_LINK_TWOPAIR);

            testCardTypeIsRight(new byte[]{52, 53}, BMDataContext.CardsTypeHEnum.BOM_WANG);

            testCardTypeIsRight(new byte[]{22, 23, 24, 25}, null);

            testCardTypeIsRight(new byte[]{22, 23, 24, 25, 26, 27}, BMDataContext.CardsTypeHEnum.FOUR_PAIR);
            testCardTypeIsRight(new byte[]{22, 23, 24, 25, 26}, BMDataContext.CardsTypeHEnum.THREE_PAIR);

            testCardTypeIsRight(new byte[]{22, 23, 24, 25, 26, 32, 34}, null);
            testCardTypeIsRight(new byte[]{22, 23, 24, 25, 32, 34}, null);

            testCardTypeIsRight(new byte[]{22, 23, 24, 25, 28, 29, 32}, null);
            testCardTypeIsRight(new byte[]{22, 23, 24, 25, 32, 33}, null);

            testCardTypeIsRight(new byte[]{0, 1, 2, 3}, BMDataContext.CardsTypeHEnum.BOM);
            testCardTypeIsRight(new byte[]{0, 1, 2, 3, 4}, BMDataContext.CardsTypeHEnum.FOUR_SINGLE);
            testCardTypeIsRight(new byte[]{0, 1, 2, 3, 4, 11}, BMDataContext.CardsTypeHEnum.FOUR_TWOSINGLE);
            testCardTypeIsRight(new byte[]{0, 1, 2, 3, 4, 7}, BMDataContext.CardsTypeHEnum.FOUR_PAIR);
            testCardTypeIsRight(new byte[]{0, 1, 2, 3, 4, 8}, BMDataContext.CardsTypeHEnum.FOUR_TWOSINGLE);
        }
    }

    private void testCardTypeIsRight(byte[] cards, BMDataContext.CardsTypeHEnum cardTypeEnum) {
        String testUnitFunction = "com.beimi.core.engine.game.ActionTaskUtils.identification()";
        //System.out.println("本用例测试：" + testUnitFunction);
        String testUnitFun = "   \n 请检查 ：" + testUnitFunction;
        StringBuffer buffer = new StringBuffer();
        buffer.append(Card.getInstance().cardsToString(cards));
        /////////////////
        //被测试的方法
        CardType cardType = ActionTaskUtils.identification(cards);

        buffer.append("                牌型: ");
        if (cardType != null)
            buffer.append(cardType.getCardTypeEnum().getName());
        else
            buffer.append(cardType);

        System.out.println(buffer);
        if (cardTypeEnum == null)
            assert cardType == null : "当前：" + cardType.getCardTypeEnum().getName() + "<> 预期：为空" + testUnitFun;
        else {
            if (cardType == null)
                assert cardType != null : "当前：为空" + "<>" + "预期:" + cardTypeEnum.getName() + testUnitFun;
            else
                assert cardType.getCardTypeEnum() == cardTypeEnum : "当前：" + cardType.getCardTypeEnum().getName() + "<> 预期：" + cardTypeEnum.getName() + testUnitFun;
        }

    }
}
