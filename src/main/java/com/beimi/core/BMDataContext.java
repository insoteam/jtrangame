package com.beimi.core;

import com.beimi.core.engine.game.GameEngine;
import org.springframework.context.ApplicationContext;

public class BMDataContext {
	public static final String USER_SESSION_NAME = "user";
	public static final String GUEST_USER = "guest";
	public static final String IM_USER_SESSION_NAME = "im_user";
	public static final String GUEST_USER_ID_CODE = "BEIMIGUESTUSEKEY" ;
	public static final String SERVICE_QUENE_NULL_STR = "service_quene_null" ;
	public static final String DEFAULT_TYPE = "default"	;		//默认分类代码
	public static final String BEIMI_SYSTEM_DIC = "com.dic.system.template";
	public static final String BEIMI_SYSTEM_GAME_TYPE_DIC = "com.dic.game.type";
	public static final String BEIMI_SYSTEM_GAME_SCENE_DIC = "com.dic.scene.item";
	
	public static final String BEIMI_SYSTEM_GAME_ACCOUNT_CONFIG = "game_account_config";
	public static final String BEIMI_GAME_PLAYWAY = "game_playway";
	
	public static final String BEIMI_SYSTEM_AUTH_DIC = "com.dic.auth.resource";
	
	
	public static final String SYSTEM_ORGI = "beimi" ;    //租户ID
	
	private static int WebIMPort = 9081 ;
	
	private static boolean imServerRunning = false ;			//IM服务状态
	
	private static ApplicationContext applicationContext ;
	
	
	private static GameEngine gameEngine ;
	
	public static int getWebIMPort() {
		return WebIMPort;
	}

	public static void setWebIMPort(int webIMPort) {
		WebIMPort = webIMPort;
	}
	
	public static void setApplicationContext(ApplicationContext context){
		applicationContext = context ;
	}
	
	public static void setGameEngine(GameEngine engine){
		gameEngine = engine ;
	}
	/**
	 * 根据ORGI找到对应 游戏配置
	 * @param orgi
	 * @return
	 */
	public static String getGameAccountConfig(String orgi){
		return BEIMI_SYSTEM_GAME_ACCOUNT_CONFIG+"_"+orgi ;
	}
	
	public static ApplicationContext getContext(){
		return applicationContext ;
	}
	
	public static GameEngine getGameEngine(){
		return gameEngine; 
	}
	/**
	 * 系统级的加密密码 ， 从CA获取
	 * @return
	 */
	public static String getSystemSecrityPassword(){
		return "BEIMI" ;
	}
	
	public enum NameSpaceEnum{
		
		SYSTEM("/bm/system") ,
//		GAME("/bm/game");jtrangame
		GAME("/jtrangame");
		private String namespace ;
		
		public String getNamespace() {
			return namespace;
		}

		NameSpaceEnum(String namespace){
			this.namespace = namespace ;
		}
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum ModelType{
		ROOM;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum ConfigNames{
		GAMECONFIG,
		AICONFIG,
		ACCOUNTCONFIG,
		PLAYWAYCONFIG;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum UserDataEventType{
		SAVE,UPDATE,DELETE;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	
	public enum GameTypeEnum{
		MAJIANG,
		DIZHU,
		DEZHOU;
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
	
	public enum PlayerTypeEnum{
		AI,			//AI
		NORMAL,    //普通玩家
		OFFLINE;	//离线 托管玩家
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	/**
	 * THREE_SINGLE(name,ID,辅助带单双（1为单，2为双，3为带2张单，4为带两对，0为无辅助牌)
	 */
	public enum CardsTypeHEnum{

		SINGLE("单张",1,0),		              //单张      3~K,A,2     GROUPMAXPAGES=1 && GROUPS=1
		SINGLE_LINK("顺子", 2, 0),             //单张连   顺子             GROUPMAXPAGES=1 && GROUPS>=5 && MAXPAGES_LINK =TRUE

		PAIR("一对", 3, 0),		              //一对                  GROUPAMXPAGES=2 && GROUPS=1 && !HAVEWANG()
		PAIR_LINK("连对", 4, 0),                //两张连 连对 JJQQKK    GROUPMAXPAGES=2 && GROUPS>=3 && MAXPAGES_LINK =TRUE && CARDS=GROUPS*2

		THREE("三张", 5, 0),	                  //三张	     JJJ       GROUPMAXPAGES=3 && GROUPS=1
		THREE_SINGLE("三带一", 6, 1),           //三带1       JJJ|K      GROUPMAXPAGES=3 && GROUPS=2 && CARDS=4
		THREE_PAIR("三带一对", 7, 2),            //三带一对      JJJ|KK     GROUPMAXPAGES=3 && GROUPS=2 && CARDS=5
		THREE_LINK("飞机", 8, 0),                //三张连 飞机     JJJ|QQQ    GROUPMAXPAGES=3 && GROUPS=2 && MAXPAGES_LINK =TRUE
		THREE_LINK_SINGLE("三张连带单", 9, 1),//三张连带单 飞机  JJJ|QQQ|K|A  GROUPMAXPAGES=3 && GROUPS=4 && MAXPAGES_LINK =TRUE  &&CARDS=maxPagesLinkCount*2+maxPagesLinkCount
		THREE_LINK_PAIR("三张连带对", 10, 2), //三张连带对 飞机  JJJ|QQQ|KK|22 GROUPMAXPAGES=3 && GROUPS=4 && MAXPAGES_LINK=TRUE  &&CARDS=maxPagesLinkCount*2+maxPagesLinkCount*2

		FOUR_SINGLE("四带一", 11, 1),	          //四带一	 JJJJ|K             GROUPMAXPAGES=4 && GROUPS=2 && CARDS=5
		FOUR_TWOSINGLE("四带二", 12, 1),	      //四带二	 JJJJ|K|J           GROUPMAXPAGES=4 && GROUPS=3 && CARDS=6
		//FIXME xx这个不存在，应叫四带二
		FOUR_PAIR("四带对", 13, 2),	          //四带对   JJJJ|KK            GROUPMAXPAGES=4 && GROUPS=2 && CARDS=6
		FOUR_TWOPAIR("四带两对", 14, 2),	      //四带两对 JJJJ|KK|QQ         GROUPMAXPAGES=4 && GROUPS=3 && CARDS=8
		FOUR_LINK("四张连", 15, 0),             //四张连          JJJJ|QQQQ               GROUPMAXPAGES=4 && GROUPS=2 && MAXPAGES_LINK =TRUE
		FOUR_LINK_SINGLE("四张连带一", 16, 1),//四张连带一      JJJJ|QQQQ|K|A           GROUPMAXPAGES=4 && GROUPS=4 && MAXPAGES_LINK =TRUE && MAXLINKS=2 && CARDS=10
		                                          //四张连带一  JJJJ|QQQQ|KKKK|3|5|7       GROUPMAXPAGES=4 && GROUPS=6 && MAXPAGES_LINK =TRUE  MAXLINKS=3 && CARDS=15
		FOUR_LINK_TWOSINGLE("四带连带二", 17, 1),//四张连带二      JJJJ|QQQQ|4|6|7|9       GROUPMAXPAGES=4 && GROUPS=6 && MAXPAGES_LINK
		// =TRUE && MAXLINKS=2 && CARDS=12
		FOUR_LINK_PAIR("四张连带对", 18, 2),      //四张连带对      JJJJ|QQQQ|33|55        GROUPMAXPAGES=4 && GROUPS=4 && MAXPAGES_LINK =TRUE && MAXLINKS=2 && CARDS=12
		                                              //四张连带对    JJJJ|QQQQ|KKKK|33|55|77    GROUPMAXPAGES=4 && GROUPS=6 && MAXPAGES_LINK =TRUE  MAXLINKS=3 && CARDS=18
		FOUR_LINK_TWOPAIR("四张连带两对", 19, 2), //四张连带两对    JJJJ|QQQQ|33|55|77|88   GROUPMAXPAGES=4 && GROUPS=6 && MAXPAGES_LINK =TRUE && MAXLINKS=2 && CARDS=16

		BOM("炸弹", 20, 0),	                   //炸弹	JJJJ   GROUPMAXPAGES=4 && GROUPS=1
		BOM_WANG("王炸", 21, 0);	               //王炸	0+0    GROUPMAXPAGES=2 && GROUPS=1 && HAVEWANG()

		private final String name;
		private final int acc;
		private  int type ;


		CardsTypeHEnum(String name, int type, int acc){
			this.type = type ;
			this.name = name;
			this.acc = acc;   //辅牌  辅助带单双（1为单，2为双，3为带2张单，4为带两对，0为无辅助牌)
		}


		public int getType() {

			return type;
		}

		public int getAcc() {
			return acc;
		}
		public String getName() {
			return name;
		}
	}

	
	public enum MessageTypeEnum{
		JOINROOM,
		MESSAGE, 
		END,
		TRANS, STATUS , AGENTSTATUS , SERVICE, WRITING;
		
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}

	public static void setIMServerStatus(boolean running){
		imServerRunning = running ;
	}
	public static boolean getIMServerStatus(){
		return imServerRunning;
	}

	public enum GameStatusEnum {
		READY,			//AI
		NOTREADY,		//普通玩家
		MANAGED,
		PLAYING;	//离线 托管玩家
		public String toString(){
			return super.toString().toLowerCase() ;
		}
	}
}
