package com.beimi.core.engine.game.action;

import com.beimi.core.BMDataContext;
import com.beimi.core.engine.game.task.CreateTackCardsTask;
import com.beimi.core.statemachine.action.Action;
import com.beimi.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.beimi.core.statemachine.message.Message;
import com.beimi.util.cache.CacheHelper;
import com.beimi.util.rules.model.Board;
import com.beimi.web.model.GameRoom;
import org.apache.commons.lang3.StringUtils;

/**
 * 出牌动作
 *
 * 1、玩家
 * 玩家超时（25秒）不出牌，自动进入这个地方，进行自动出牌
 * 2、机器人
 * 机器人调用这里自动出牌
 * @author iceworld
 */
public class PlayCardsAction<T, S> implements Action<T, S> {
    @Override
    public void execute(Message<T> message, BeiMiExtentionTransitionConfigurer<T, S> configurer) {
        String room = (String) message.getMessageHeaders().getHeaders().get("room");
        if (StringUtils.isBlank(room))
            return;


        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(room, BMDataContext.SYSTEM_ORGI);
        if (gameRoom == null)
            return;

        //获得牌桌
        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        //??
        int timeout = (int) message.getMessageHeaders().getHeaders().get("interval");
        //默认将地主赋给下一个玩家  有何意义？？
        String nextPlayer = board.getBanker();
        if (!StringUtils.isBlank(board.getNextplayer())) {  //如果下个玩家存在，则将下个玩家赋给下一步
            nextPlayer = board.getNextplayer();
        }
        //通过队列
        CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new CreateTackCardsTask(timeout, nextPlayer, gameRoom, gameRoom.getOrgi()));
    }
}
