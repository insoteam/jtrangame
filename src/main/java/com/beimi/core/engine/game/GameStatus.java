package com.beimi.core.engine.game;

public class GameStatus {
	private String gamestatus ;
	
	private String userid ;

	public String getGamestatus() {
		return gamestatus;
	}
	public void setGamestatus(String gamestatus) {
		this.gamestatus = gamestatus;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}
}
