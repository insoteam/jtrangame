package com.beimi.core.engine.game;

/**
 * 抢地主发送给客户端的数据包
 * FIXME 应考虑统一数据包格式 一个个写太累人
 */
public class GameBoard implements java.io.Serializable {
    private static final long serialVersionUID = -907633644768054042L;

    //FIXME 这种向前端发消息的，可能统一数据包比较好
    public GameBoard(String userid, String sex, boolean docatch, boolean grab, int ratio, int catchTimes) {
        this.userid = userid;
        this.sex = sex;
        this.docatch = docatch;  //FIXME 这个前端好象没用到
        this.ratio = ratio;
        this.grab = grab;        //FIXME 这个是不是考虑换成isAccept
        this.catchtimes = catchTimes;
    }

    public GameBoard(String userid, byte[] lasthands, int ratio, int lastHandRatio) {
        this.userid = userid;
        this.lasthands = lasthands;
        this.ratio = ratio;
        this.lastHandRatio = lastHandRatio;
    }

    private byte[] lasthands;
    private String userid;          //当前用户ID
    private String sex;             //当前用户性别
    private boolean docatch;
    private boolean grab;           //true 时为地主
    private int catchtimes;         //抢地主的次数 ，用于决定播音是播的方式 第一个为叫地主，第二，三 是抢地主，第四个为我抢
    private int ratio;              //倍率

    private int lastHandRatio;      //底牌倍率

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public boolean isDocatch() {
        return docatch;
    }

    public void setDocatch(boolean docatch) {
        this.docatch = docatch;
    }

    public int getRatio() {
        return ratio;
    }

    public void setRatio(int ratio) {
        this.ratio = ratio;
    }

    public boolean isGrab() {
        return grab;
    }

    public void setGrab(boolean grab) {
        this.grab = grab;
    }

    public byte[] getLasthands() {
        return lasthands;
    }

    public void setLasthands(byte[] lasthands) {
        this.lasthands = lasthands;
    }

    public int getCatchtimes() {
        return catchtimes;
    }

    public void setCatchtimes(int catchtimes) {
        this.catchtimes = catchtimes;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getLastHandRatio() {
        return lastHandRatio;
    }

    public void setLastHandRatio(int lastHandRatio) {
        this.lastHandRatio = lastHandRatio;
    }
}
