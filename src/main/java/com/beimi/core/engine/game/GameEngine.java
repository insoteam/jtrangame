package com.beimi.core.engine.game;

import com.beimi.core.BMDataContext;
import com.beimi.core.engine.game.state.GameEvent;
import com.beimi.util.GameUtils;
import com.beimi.util.UKTools;
import com.beimi.util.cache.CacheHelper;
import com.beimi.util.client.NettyClients;
import com.beimi.util.rules.model.Board;
import com.beimi.util.rules.model.Player;
import com.beimi.util.rules.model.RecoveryData;
import com.beimi.util.rules.model.TakeCards;
import com.beimi.util.rules.model.card.Card;
import com.beimi.util.server.handler.BeiMiClient;
import com.beimi.web.model.GamePlayway;
import com.beimi.web.model.GameRoom;
import com.beimi.web.model.PlayUserClient;
import com.beimi.web.service.repository.jpa.GameRoomRepository;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.SocketIOServer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service(value = "beimiGameEngine")
public class GameEngine {

    @Autowired
    protected SocketIOServer server;

    /**
     * 玩家房间选择，新的请求， 如果当前玩家是断线重连， 或者是 退出后进入的，则第一步检查是否已在房间
     * 如果已在房间，直接返回
     *
     * @param userId   玩家ID
     * @param playway  玩法
     * @param room     房单
     * @param orgi     运营ID
     * @param playUser 玩家对象
     * @return 游戏事件
     */
    public GameEvent gameRequest(String userId, String playway, String room, String orgi, PlayUserClient playUser) {
        GameEvent gameEvent = null;
        String roomId = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userId, orgi);
        GamePlayway gamePlayway = (GamePlayway) CacheHelper.getSystemCacheBean().getCacheObject(playway, orgi);
        if (gamePlayway != null) {
            gameEvent = new GameEvent(gamePlayway.getPlayers(), gamePlayway.getCardsnum(), orgi);
            GameRoom gameRoom;
            if (!StringUtils.isBlank(roomId) && CacheHelper.getGameRoomCacheBean().getCacheObject(roomId, orgi) != null) {//
                //直接加入到 系统缓存 （只有一个地方对GameRoom进行二次写入，避免分布式锁）
                gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomId, orgi);
            } else {
                if (!StringUtils.isBlank(room)) {    //房卡游戏 , 创建ROOM
                    //创建房间并创建房间对应的队列缓存
                    gameRoom = this.creatGameRoom(gamePlayway, userId, true);   //当没有房间号时，创建一个房间
                    CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, orgi); //房单缓存 key为房间ID
                } else {    //大厅游戏 ， 撮合游戏
                    gameRoom = (GameRoom) CacheHelper.getQueneCache().poll(orgi);   //移除
                    if (gameRoom == null) {    //无房间ID ， 需要
                        gameRoom = this.creatGameRoom(gamePlayway, userId, false);
                        CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, orgi);
                    } else {
                        /**
                         * 如果当前房间到达了最大玩家数量，则不再加入到 撮合队列
                         */
                        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean2().getCacheObject(gameRoom.getId());
                        if ((playerList.size() + 1) < gamePlayway.getPlayers() && CacheHelper.getExpireCache().get(gameRoom.getId()) != null) {
                            CacheHelper.getQueneCache().offer(gameRoom, orgi);    //未达到最大玩家数量，加入到游戏撮合 队列，继续撮合
                        }
                        playUser.setPlayerindex(gameRoom.getPlayers() - playerList.size() - 1);//从后往前坐，房主进入以后优先坐在 首位
                    }
                }
            }
            if (gameRoom != null) {
                /**
                 * 如果当前房间到达了最大玩家数量，则不再加入到 撮合队列
                 */
                List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean2().getCacheObject(gameRoom.getId());
                if (playerList.size() == 0) {
                    gameEvent.setEvent(BeiMiGameEvent.ENTER.toString());
                } else {
                    gameEvent.setEvent(BeiMiGameEvent.JOIN.toString());
                }
                gameEvent.setGameRoom(gameRoom);
                gameEvent.setRoomid(gameRoom.getId());
                NettyClients.getInstance().joinRoom(userId, gameRoom.getId());
                List<PlayUserClient> gramPlayerList = CacheHelper.getGamePlayerCacheBean2().getCacheObject(gameRoom.getId());
                boolean inRoom = false;
                for (PlayUserClient playUserClient : gramPlayerList) {   //
                    if (playUserClient.getId().equals(userId)) {
                        inRoom = true;
                        break;
                    }
                }
                if (inRoom == false) {
                    playUser.setPlayerindex(gameRoom.getPlayers() - playerList.size());
                    playUser.setGamestatus(BMDataContext.GameStatusEnum.READY.toString());
                    playUser.setPlayertype(BMDataContext.PlayerTypeEnum.NORMAL.toString());
                    //CacheHelper.getGamePlayerCacheBean().put(gameRoom.getId(), playUser, orgi); //将用户加入到 room ， MultiCache
                    CacheHelper.getGamePlayerCacheBean2().put(gameRoom.getId(), playUser); //将用户加入到 room ， 单Cache
                }
                /**
                 *	不管状态如何，玩家一定会加入到这个房间
                 */
                CacheHelper.getRoomMappingCacheBean().put(userId, gameRoom.getId(), orgi);
            }
        }
        return gameEvent;
    }


    /**
     * 加入牌局
     *
     * @param sockClient  客户端SOCK
     * @param userClient  玩家
     * @param beiMiClient 客户端发来的消息
     */
    public void joinRoomRequest(SocketIOClient sockClient, PlayUserClient userClient, BeiMiClient beiMiClient) {
        //处理房间的事
        GameEvent gameEvent = gameRequest(beiMiClient.getUserid(), beiMiClient.getPlayway(),
                beiMiClient.getRoom(), beiMiClient.getOrgi(), userClient);
        if (gameEvent != null) {
            if (userClient != null) {
                userClient.setGamestatus(BMDataContext.GameStatusEnum.READY.toString());  //设置游戏状态
            }
            /**
             * 游戏状态 ， 玩家请求 游戏房间，活动房间状态后，发送事件给 StateMachine，由 StateMachine驱动 游戏状态 ， 此处只负责通知房间内的玩家
             * 1、有新的玩家加入（通知房中所有人）
             * 2、给当前新加入的玩家发送房间中所有玩家信息（不包含隐私信息，根据业务需求，修改PlayUserClient的字段，剔除掉隐私信息后发送）
             */
            //通知牌局中的已有人，有玩家加入  FIXME 这里可以是断线重连
            ActionTaskUtils.sendEvent(DiZhuCommand.JOIN_ROOM, UKTools.json(userClient), gameEvent.getGameRoom());
            //发送单一玩家消息
            sockClient.sendEvent(DiZhuCommand.PLAYERS, UKTools.json(CacheHelper.getGamePlayerCacheBean2()
                    .getCacheObject
                            (gameEvent.getRoomid())));

            /**
             * 当前是在游戏中还是 未开始
             */
            Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameEvent.getRoomid(), gameEvent.getOrgi());
            if (board != null) {
                Player currentPlayer = board.getPlayer(userClient.getId());
                if (currentPlayer != null) {
                    boolean automic = false;
                    if ((board.getLast() != null && board.getLast().getUserid().equals(currentPlayer.getPlayuser()))   //最后出牌是自已
                            || (board.getLast() == null && currentPlayer.getPlayuser().equals(board.getBanker()))   //自已是地主
                            ) {
                        automic = true;  //  表示不允许不出牌
                    }
                    ActionTaskUtils.sendEvent(DiZhuCommand.RECOVERY, UKTools.json(new RecoveryData(currentPlayer,
                            board.getLasthands(), board.getNextplayer(), 25, automic, board)), gameEvent.getGameRoom());

                }
//                if (board.isFinished()) {  //游戏已经结束
//                    BMDataContext.getGameEngine().finished(gameEvent.getRoomid(), gameEvent.getOrgi());
//                    //game.change(gameEvent);	//通知状态机 , 此处应由状态机处理异步执行
//                } else {
//                    //恢复数据  FIXME发送最后一次的状态即最后一次出的牌并带上当前手牌 每次发给玩家所有用户手牌及状态信息
//
//                }
            } else {
                //通知状态
                ActionTaskUtils.game().change(gameEvent);    //通知状态机 , 此处应由状态机处理异步执行
            }
//            ActionTaskUtils.game().change(gameEvent);    //通知状态机 , 此处应由状态机处理异步执行
        }
    }


    /**
     * 抢地主
     * 抢地主||不抢地主
     *
     * @param roomId
     * @param playUser
     * @param orgi
     * @param accept   true 为抢地主，false 为不抢地主
     */
    public void catchRequest(String roomId, PlayUserClient playUser, String orgi, boolean accept) {
        System.out.println("enter gameengine.catchRequest");
        //1、初始及较难
        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomId, orgi);
        if (gameRoom == null)
            return;

        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        if (board == null)
            return;

        //2、业务逻辑
        Player player = board.getPlayer(playUser.getId());
        board = ActionTaskUtils.doCatch(board, player, accept);  //设置抢地主的值  抢庄计数器+1

        //客户端接收：{userid,docatch,grab true为当地主,ratio倍率 }
        ActionTaskUtils.sendEvent(DiZhuCommand.CATCH_RESULT, UKTools.json(new GameBoard(player.getPlayuser(), player.getSex(),
                board.isDocatch(), player.isAccept(), board.getRatio(), board.getCatchtimes())), gameRoom);
        //通知下一个抢庄
        ActionTaskUtils.game().change(gameRoom, BeiMiGameEvent.NOTIFYCATCH.toString(), 1);
        //Board 缓存更新
        CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());
    }

    /**
     * 出牌，并校验出牌是否合规
     * 抢地主不在这里
     * 调用说明：
     * 1、超时出牌 auto = true playCards=null
     * 2、指定出牌及不出牌
     * 出牌：auto=false playCards=null
     * 不出牌：auto=false playCards!=null
     *
     * @param roomID         房间ID
     * @param playUserClient 当前玩家
     * @param orgi           运营ID
     * @param auto           是否自动出牌，超时/托管/AI会调用 = true   手动出牌的调用=false
     * @param playCards      出的牌
     */
    public void takeCardsRequest(final String roomID, final String playUserClient, final String orgi, final boolean auto,
                                 final byte[] playCards) {

        //1、初始及较难
        TakeCards takeCards;
        GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomID, orgi);
        if (gameRoom == null)
            return;

        //@获得当前牌局信息
        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        if (board == null)
            return;

        //2、业务逻辑
        //@获得玩家信息
        Player player = board.getPlayer(playUserClient);

        PlayUserClient curPlayUserClient = ActionTaskUtils.getPlayUserClient(gameRoom.getId(), player.getPlayuser(), orgi);
        System.out.println("=====================================");


        System.out.println("cards：" + Card.getInstance().cardBytesToString(player.getCards()) + "    " + Card.getInstance().cardsToString(player.getCards()));

        //2、调用出牌算法，获得要出的牌
        takeCards = board.takecard(player, auto, board.getLast(), playCards);

        System.out.println("当前玩家 [" + curPlayUserClient.getUsername() + "]剩余牌数：" + player.getCards().length);
        System.out.println("本次出牌：" + Card.getInstance().cardsToString(takeCards.getCards()) + Card.getInstance().cardBytesToString(takeCards.getCards()));
        System.out.println("玩家手牌：" + Card.getInstance().cardsToString(player.getCards()));

        //调takeCards某些值 并通知
        setSomthingAndNotify(orgi, takeCards, gameRoom, board, player);


    }


    private void setSomthingAndNotify(String orgi, TakeCards takeCards, GameRoom gameRoom, Board board, Player player) {
        //如果出错牌了 返回通知
        if (!takeCards.isAllow()) {
            ActionTaskUtils.sendEvent(DiZhuCommand.TAKE_CARDS, ActionTaskUtils.json(takeCards), gameRoom);
            return;
        }

        //正确出牌后的处理
        if (takeCards.getCards() != null) {  //FIXME 这种设置lastcard  的方法，如果本次发送发生失败或其它原因时，可能导至这里的值 错乱了
            //可以考虑加到列表中或什么方式
            board.setLast(takeCards);      //记录本次出的牌，用于下家知道上家上次出了什么牌
            takeCards.setDonot(false);    //出牌
        } else {
            takeCards.setDonot(true);     //不出牌
        }

        //设置是否王炸
        if (takeCards.getCardType() != null
                && (takeCards.getCardType().getCardTypeEnum() == BMDataContext.CardsTypeHEnum.BOM
                || takeCards.getCardType().getCardTypeEnum() == BMDataContext.CardsTypeHEnum.BOM_WANG)) {
            ActionTaskUtils.doBomb(board);
            ActionTaskUtils.sendEvent(DiZhuCommand.BOMB, board, gameRoom);
        }

        System.out.println("=====================================");


        Player next = board.nextPlayer(board.index(player.getPlayuser()));  //FIXME 这个算法不对，如果他已经出完牌了，应该就能直接判定是赢是输了
        if (next != null) {
            takeCards.setNextplayer(next.getPlayuser());
            board.setNextplayer(next.getPlayuser());

        }
        CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());
        /**
         * 判断下当前玩家是不是和最后一手牌 是一伙的，如果是一伙的，手机端提示 就是 不要， 如果不是一伙的，就提示要不起
         * FIXME 这个setSameside 有点多余，不是地主就是相同方，是地主就只有单人，只要知道自已是不是地主，出牌方是不是地主就够了
         */
        if (player.getPlayuser().equals(board.getBanker())) { //当前玩家是地主
            takeCards.setSameside(false);
        } else {
            if (board.getLast().getUserid().equals(board.getBanker())) { //最后一把是地主出的，然而我却不是地主
                takeCards.setSameside(false);
            } else {
                takeCards.setSameside(true);
            }
        }
        /**
         * 移除定时器，然后重新设置
         * FIXME 为何是这个时候移除呢，不该 在一进来时就移除定时器吗？
         */
        CacheHelper.getExpireCache().remove(gameRoom.getRoomid());
        //向客户端发送出牌消息 FIXME 这里 ActionTaskUtils.json(takeCards) 并没转成真正的JSON
        ActionTaskUtils.sendEvent(DiZhuCommand.TAKE_CARDS, ActionTaskUtils.json(takeCards), gameRoom);    //type字段用于客户端的音效

        /**
         * 牌出完了就算赢了
         * board 这里应该是总控平台，是否应该是isgameover 比较合适
         */
        if (board.isGameOver(player.getPlayuser())) {//出完了
            System.out.println("出完了哦！" + player.getPlayuser());
            takeCards.setNextplayer(null);
            //FIXME  为何必须得PUT回去数据才能得到更新，是否可以不这样，挺麻烦的
            CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());
            ActionTaskUtils.game().change(gameRoom, BeiMiGameEvent.ALLCARDS.toString(), 0);    //赢了，通知结算
        } else {
            PlayUserClient nextPlayUserClient = ActionTaskUtils.getPlayUserClient(gameRoom.getId(), takeCards.getNextplayer(), orgi);
            //判断下个玩家类型，发送不同的消息
            if (BMDataContext.PlayerTypeEnum.NORMAL.toString().equals(nextPlayUserClient.getPlayertype())) { //AI玩家
                ActionTaskUtils.game().change(gameRoom, BeiMiGameEvent.PLAYCARDS.toString(), 25);    //应该从 游戏后台配置参数中获取
            } else { //正常人
                ActionTaskUtils.game().change(gameRoom, BeiMiGameEvent.PLAYCARDS.toString(), 3);    //应该从游戏后台配置参数中获取
            }
        }
    }

    /**
     * 退出房间
     * 1、房卡模式，userid是房主，则解散房间
     * 2、大厅模式，如果游戏未开始并且房间仅有一人，则解散房间
     *
     * @param playUser 玩家
     * @param orgi     运营ID
     * @return 房间对象
     */
    public GameRoom leaveRoom(PlayUserClient playUser, String orgi) {
        GameRoom gameRoom = whichRoom(playUser.getId(), orgi);
        if (gameRoom != null) {
            List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean2().getCacheObject(gameRoom.getId());
            if (gameRoom.isCardroom()) {
                CacheHelper.getGameRoomCacheBean().delete(gameRoom.getId(), gameRoom.getOrgi());
                CacheHelper.getGamePlayerCacheBean2().delete(gameRoom.getId());
                UKTools.published(gameRoom, null, BMDataContext.getContext().getBean(GameRoomRepository.class), BMDataContext.UserDataEventType.DELETE.toString());
            } else {
                if (players.size() <= 1) {
                    //解散房间 , 保留 ROOM资源 ， 避免 从队列中取出ROOM
                    CacheHelper.getGamePlayerCacheBean2().delete(gameRoom.getId());
                } else {
                    CacheHelper.getGamePlayerCacheBean2().delete(gameRoom.getId(), playUser);
                }
            }
        }
        return gameRoom;
    }

    /**
     * 当前用户所在的房间.
     * 说明：根据玩家获得房单
     *
     * @param playerId 玩家ID
     * @param orgi     运营ID
     * @return 缓存的房间对象
     */
    private GameRoom whichRoom(String playerId, String orgi) {
        GameRoom gameRoom = null;
        String roomId = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playerId, orgi);
        if (!StringUtils.isBlank(roomId)) {
            //直接加入到 系统缓存 （只有一个地方对GameRoom进行二次写入，避免分布式锁）
            gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean().getCacheObject(roomId, orgi);
        }
        return gameRoom;
    }


    /**
     * 结束 当前牌局.
     *
     * @param roomId 房间ID
     * @param orgi   运营ID
     */
    public void finished(String roomId, String orgi) {
        if (!StringUtils.isBlank(roomId)) {//
            CacheHelper.getExpireCache().remove(roomId);
            CacheHelper.getBoardCacheBean().delete(roomId, orgi);
        }
    }

    /**
     * 创建新房间 ，需要传入房间的玩法 ， 玩法定义在 系统运营后台，玩法创建后，放入系统缓存 ， 客户端进入房间的时候，传入 玩法ID参数
     *
     * @param playway    玩法
     * @param userId     用户
     * @param isCardRoom 是否房卡类房间 TRUE为房卡 FALSE为大厅
     * @return 新的房间对象
     */
    private GameRoom creatGameRoom(GamePlayway playway, String userId, boolean isCardRoom) {
        GameRoom gameRoom = new GameRoom();
        gameRoom.setCreatetime(new Date());
        gameRoom.setRoomid(UKTools.getUUID());
        gameRoom.setUpdatetime(new Date());
        gameRoom.setCurpalyers(1);          //当前玩家数
        gameRoom.setCardroom(isCardRoom);     //是否房卡类
        gameRoom.setStatus(BeiMiGameEnum.CRERATED.toString());
        gameRoom.setCurrentnum(0);
        gameRoom.setMaster(userId);

        if (playway != null) {              //设置房间玩法
            gameRoom.setPlayway(playway.getId());
            gameRoom.setRoomtype(playway.getRoomtype());
            gameRoom.setPlayers(playway.getPlayers());
            gameRoom.setPlayers(playway.getPlayers());
            gameRoom.setCardsnum(playway.getCardsnum());
            gameRoom.setNumofgames(playway.getNumofgames());   //无限制
            gameRoom.setOrgi(playway.getOrgi());

            //offer等同于add 差异在于offer不报错，只返回false //gameRoom为值
            CacheHelper.getQueneCache().offer(gameRoom, playway.getOrgi());    //未达到最大玩家数量，加入到游戏撮合 队列，继续撮合
        }

        //更新数据
        UKTools.published(gameRoom, null, BMDataContext.getContext().getBean(GameRoomRepository.class), BMDataContext.UserDataEventType.SAVE.toString());

        return gameRoom;
    }
}
