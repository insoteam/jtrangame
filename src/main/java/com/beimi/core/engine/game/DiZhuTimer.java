package com.beimi.core.engine.game;

/**.
 * The type Di zhu timer.
 *
 * @author huangzh on 2017-11-14 13:58
 * seenet2004 @163.com
 * 说明：
 */
public final class DiZhuTimer {

    /**.
     * 指定客户端计时器读秒数
     */
    public static final int CATCH_TIME = 15;

    /**.
     *工具类隐藏构造器
     */
    private DiZhuTimer() {
    }
}
