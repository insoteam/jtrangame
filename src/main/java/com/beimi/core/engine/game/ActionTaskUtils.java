package com.beimi.core.engine.game;

import com.beimi.config.web.model.Game;
import com.beimi.core.BMDataContext;
import com.beimi.util.UKTools;
import com.beimi.util.cache.CacheHelper;
import com.beimi.util.client.NettyClients;
import com.beimi.util.rules.model.Board;
import com.beimi.util.rules.model.Player;
import com.beimi.util.server.handler.BeiMiClient;
import com.beimi.web.model.GameRoom;
import com.beimi.web.model.PlayUserClient;

import java.util.*;

import static com.beimi.core.BMDataContext.CardsTypeHEnum.BOM;
import static com.beimi.core.BMDataContext.CardsTypeHEnum.BOM_WANG;


public class ActionTaskUtils {

    /**
     * @return
     */
    public static Game game() {
        return BMDataContext.getContext().getBean(Game.class);
    }


    /**
     * 向每个客户端发送消息
     *
     * @param event
     * @param data
     * @param gameRoom
     */
    public static void sendEvent(final String event, Object data, final GameRoom gameRoom) {
        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean2().getCacheObject(gameRoom.getId());
        for (PlayUserClient user : players) {
            BeiMiClient client = NettyClients.getInstance().getClient(user.getId());
            if (client != null) {
                //调用SocketIOClient 发送消息事件
                client.getClient().sendEvent(event, data);
            }
        }
    }

    public static PlayUserClient getPlayUserClient(String roomid, String player, String orgi) {
        PlayUserClient playUserClient = null;
        List<PlayUserClient> players = CacheHelper.getGamePlayerCacheBean2().getCacheObject(roomid);
        for (PlayUserClient user : players) {
            if (player.equals(user.getId())) {
                playUserClient = user;
            }
        }
        return playUserClient;
    }

    public static Object json(Object data) {
        return UKTools.json(data);
    }

    /**
     * 临时放这里，重构的时候 放到 游戏类型的 实现类里
     *
     * @param board
     * @param player
     * @return
     */
    public static Board doCatch(Board board, Player player, boolean accept) {
        player.setAccept(accept); //抢地主
        player.setDocatch(true);  //抢过地主
        board.setDocatch(true);   //抢过地主

        if (accept) {    //抢了地主
            if (board.getCatchtimes() != 0) {  //系统指定的玩家抢地主不加倍  FIXME 是不是可以不管？
                board.setRatio(board.getRatio() * 2);  //抢过一次地主，倍数*2
            }
            board.setBanker(player.getPlayuser()); //先设为地主
        }
        board.setCatchtimes((byte) (board.getCatchtimes() + 1));
        return board;
    }

    public static void sleep(final int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 抢地主流局处理
     *
     * @param board
     */
    public static void catchDrawOrPlay(Board board, GameRoom gameRoom, Game game) {
        if (board.getBanker() == null) {   //如果没有地主
            System.out.println("======流局了！================================");
            sleep(1000);  //停一下，否则前端还来不急处理
            sendEvent(DiZhuCommand.CATCH_FAIL, json(null), gameRoom);   //通知客户端流局了

            CacheHelper.getBoardCacheBean().delete(gameRoom.getId(), gameRoom.getOrgi());  //删除board ，重新发牌时会重新产生一个新的
            game.change(gameRoom, BeiMiGameEvent.ENOUGH.toString(), 5);    //跳回前一个状态 ，重新发牌
        } else {//有地主   开始打牌，地主是最后一个抢了地主的人
            System.out.println("======开始打牌================================");
            //进入下一个状态，发底牌
            game.change(gameRoom, BeiMiGameEvent.LASTHAND.toString());
        }
    }

    /**
     * 获得当前该抢地主的玩家
     * 说明：直接了当的写
     * 根据系统提定的地主及抢的次数，获得下一个抢地主的人
     *
     * @param board 牌局
     * @return
     */
    public static Player getCatchPlayer(Board board) {
        //FIXME 此方式为欢乐斗地主，如果是经典方式按分数高者抢的话 可能得改一下
        System.out.println("borad catchcount:" + Byte.toString(board.getCatchtimes()));
        Player randomPlay = board.getRandomPlay();  //从第一个系统提定的庄家开始

        if (board.getCatchtimes() == 0) {
            return randomPlay;
        } else if (board.getCatchtimes() == 1) {
            return board.nextPlayer(board.getPlayerIndex(randomPlay));
        } else if (board.getCatchtimes() == 2) {
            Player nextPlayer = board.nextPlayer(board.getPlayerIndex(randomPlay));
            return board.nextPlayer(board.getPlayerIndex(nextPlayer));
        } else if (board.getCatchtimes() == 3) {
            /**
             * 1、如果第一个抢地主的人抢了地主，而其它人都不抢，这时不需要再抢了，可以结束抢地主的过程
             * 2、如果轮了一轮都没人抢，那也算流版版牌了
             */

            if (randomPlay.getPlayuser().equals(board.getBanker())
                    || board.getBanker() == null)
                return null;
            else
                return randomPlay;
        } else {
            return null;  //都抢过地主了
        }
    }

    /**
     * 验证出牌是否合规则
     *
     * @param curPlayCards 当前出的牌
     * @param lastCardType 上次出的牌型
     * @return true 为允许，false 不允许
     */
    public static boolean allow(byte[] curPlayCards, CardType lastCardType) {
        CardType playCardType = ActionTaskUtils.identification(curPlayCards);  //获得本次牌型
        return ActionTaskUtils.allow(playCardType, lastCardType); //合规，允许出牌
    }

    /**
     * 校验当前出牌是否合规
     *
     * @param playCardType
     * @param lastCardType
     * @return
     */
    public static boolean allow(CardType playCardType, CardType lastCardType) {
        //1、验证
        if (playCardType == null)  //没牌形肯定是不允许的
            return false;

        //2、具体验证
        boolean allow = false;
        switch (lastCardType.getCardTypeEnum()) {
            case BOM_WANG:   //对方是王炸，那不可能有比他大的，所以出的牌一定是错的
                allow = false;
                break;
            case BOM:    //对方是炸弹，本方也是炸弹且牌比对方大
                if (playCardType.getCardTypeEnum() == BOM && playCardType.getMaxValue() > lastCardType.getMaxValue()) {
                    allow = true;
                } else if (playCardType.getCardTypeEnum() == BOM_WANG) {  //王炸
                    allow = true;
                }
                break;
            default:   //对方是普通牌形，本方是炸弹或者牌形一至且大于长就可以
                if (playCardType.getCardTypeEnum() == BOM || playCardType.getCardTypeEnum() == BOM_WANG) {//对方普通，本方炸弹
                    allow = true;
                } else if ( //单张大鬼压小鬼的特殊处理
                        playCardType.getCardTypeEnum() == BMDataContext.CardsTypeHEnum.SINGLE //单张
                                && playCardType.getCardTypeEnum() == lastCardType.getCardTypeEnum()//同牌型
                                && playCardType.getMaxValue() == 13  //大小鬼的组
                                && playCardType.getCards()[0] > lastCardType.getCards()[0]//最大牌ID
                        ) {
                    allow = true;
                } else if (playCardType.getCardTypeEnum() == lastCardType.getCardTypeEnum()   //同一牌型
                        && playCardType.getMaxValue() > lastCardType.getMaxValue()  //最大组值>对方
                        && playCardType.getLinks() == lastCardType.getLinks()       //最大组连数
                        && playCardType.getCards().length == lastCardType.getCards().length  //张数一至
                        ) {
                    allow = true;
                }
                break;
        }
        return allow;
    }


    /**
     * 分类
     * FIXME 这里大小鬼也放在同一组里
     *
     * @param cards
     * @return Map<Integer , Integer> <牌面值,牌面值的牌数>
     */
    public static Map<Integer, Integer> type(byte[] cards) {
        Map<Integer, Integer> groupMap = new HashMap<Integer, Integer>();
        groupMap(cards, groupMap);
        return groupMap;
    }

    /**
     * 对传入的牌进行分组
     *
     * @param cards 传入的牌
     * @param isDes true为降序，否则为升序
     * @return
     */
    public static Map<Integer, Integer> groupCard(byte[] cards, final boolean isDes) {
        //1、先进行排序
        Map<Integer, Integer> groupMap = new TreeMap<Integer, Integer>(
                new Comparator<Integer>() {
                    public int compare(Integer obj1, Integer obj2) {
                        if (isDes) {
                            // 降序排序 按KEY的，不是按VALUE的
                            return obj2.compareTo(obj1);
                        } else {
                            return obj1.compareTo(obj2);
                        }
                    }
                }
        );

        //分组 key为组值（card/4) ，value为同组张数

        groupMap(cards, groupMap);
        return groupMap;
    }

    public static void groupMap(byte[] cards, Map<Integer, Integer> groupMap) {
        for (int i = 0; i < cards.length; i++) {
            int cardValue = cards[i] / 4;   //获得牌面值
            if (groupMap.get(cardValue) == null) {
                groupMap.put(cardValue, 1);
            } else {
                groupMap.put(cardValue, groupMap.get(cardValue) + 1);
            }
        }
    }

    /**
     * 牌型识别
     *
     * @param cards
     * @return
     */
    public static CardType identification(byte[] cards) {
        if (cards == null || cards.length == 0)
            return null;

        int cardsLength = cards.length;
        //1按牌面值从大到小分组，得出每组张数
        Map<Integer, Integer> groupMap = groupCard(cards, true);

        //2获取最大张数的最大面值那组
        Map.Entry<Integer, Integer> maxGroupEntry = getGroupEntry(groupMap, -1);
        int maxGroupPages = maxGroupEntry.getValue();
        int maxGroupPagesMaxKey = maxGroupEntry.getKey();

        boolean maxPagesIsLink = false;
        boolean maxPagesIsHaveWang = false;  //是否有王
        int maxPagesLinkCount = 0;
        int lastKey = 0;

        for (Map.Entry<Integer, Integer> entry : groupMap.entrySet()) {
            if (maxGroupPages == entry.getValue()) {
                if (maxPagesLinkCount == 0) { //第一次，找到并标记下初始值就可以，
                    lastKey = entry.getKey();
                    maxPagesIsLink = true;
                    if (maxGroupPagesMaxKey == getWangGroupKey()) {
                        maxPagesIsHaveWang = true;     //这样就可以知道最大组里有没王了
                    }

                } else {                      //第二次
                    //第二次进入循环
                    if (entry.getKey() != --lastKey) {  //因MAP是按KEY倒序的，所有用减的 ，又因连继必须 KEY 差值为1，所以这样就是判断是否连续
                        maxPagesIsLink = false;  //不相等，说明不连继了
                        break;
                    }
                }
                maxPagesLinkCount++;
            }
        }

        if (maxPagesLinkCount == 1) {
            maxPagesIsLink = false;
            maxPagesLinkCount = 0;
        }

        BMDataContext.CardsTypeHEnum cardsTypeHEnum = null;
        int groups = groupMap.size();  //组数
        switch (maxGroupPages) {  //最大分组牌数
            case 1:
                switch (groups) {//J  ;  JK ;JQK...
                    case 1:   //1种分组 J
                        cardsTypeHEnum = BMDataContext.CardsTypeHEnum.SINGLE; //单张
                        break;
                    default:
                        if (groups >= 5 && maxPagesIsLink) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.SINGLE_LINK; //单顺子
                        }
                        break;
                }
                ;
                break;
            case 2:
                switch (groups) {//JJ,JJQ,JJQQ,JJK,JJKK,大小王,JJQQKK...
                    case 1:
                        if (maxPagesIsHaveWang) {  //对子组里有王
                            cardsTypeHEnum = BOM_WANG;  //王炸
                        } else {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.PAIR;       //对子
                        }
                        break;
                    default:
                        if (groups >= 3 && !maxPagesIsHaveWang && maxPagesIsLink && cardsLength == groups * 2) {  //无王且连续且每组都是两张牌
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.PAIR_LINK;     //连对
                        }
                        break;
                }
                ;
                break;
            case 3:
                switch (groups) {
                    case 1:
                        cardsTypeHEnum = BMDataContext.CardsTypeHEnum.THREE;  //三张
                        break;    //无牌型  345
                    case 2: //可能是三带1，三带对 ,三张连（23）
                        switch (cardsLength) {
                            case 4:
                                cardsTypeHEnum = BMDataContext.CardsTypeHEnum.THREE_SINGLE;  //三带1
                                break;
                            case 5:
                                cardsTypeHEnum = BMDataContext.CardsTypeHEnum.THREE_PAIR;  //三带对
                                break;
                            case 6:
                                if (maxPagesIsLink) {
                                    cardsTypeHEnum = BMDataContext.CardsTypeHEnum.THREE_LINK;  //三张连
                                }
                                break;
                            default:
                                break;
                        }
                        ;
                        break;
                    case 3:
                        switch (cardsLength) {
                            case 8:
                                if (maxPagesIsLink) {
                                    cardsTypeHEnum = BMDataContext.CardsTypeHEnum.THREE_LINK_SINGLE;  //三张连带单
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                    case 4: //可能是三张连（33），三带2     JJJKQ
                    case 6:
                        if (maxPagesIsLink) {
                            if (cardsLength == maxPagesLinkCount * maxGroupPages + maxPagesLinkCount) {
                                cardsTypeHEnum = BMDataContext.CardsTypeHEnum.THREE_LINK_SINGLE;  //三张连带单
                            } else if (cardsLength == maxPagesLinkCount * maxGroupPages + maxPagesLinkCount * 2) {
                                cardsTypeHEnum = BMDataContext.CardsTypeHEnum.THREE_LINK_PAIR;    //三张连带对
                            }
                        }
                        break;
                    default:
                        break;
                }
                break;
            case 4:
                switch (groups) {
                    case 1:
                        cardsTypeHEnum = BOM;    //炸弹
                        break;
                    case 2:
                        if (cardsLength == 5) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_SINGLE;    //四带一
                        } else if (cardsLength == 6) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_PAIR;    //四带对
                        } else if (maxPagesIsLink) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK;    //四张连
                        }

                        break;
                    case 3:
                        if (cardsLength == 6) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_TWOSINGLE;    //四带二
                        } else if (cardsLength == 8) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_TWOPAIR;    //四带两对
                        } else if (cardsLength == 10) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_SINGLE;    //四张连带一
                        }
                        break;
                    case 4:
                        if (maxPagesIsLink && maxPagesLinkCount == 2 && cardsLength == 10) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_SINGLE;    //四张连带一
                        }

                        if (maxPagesIsLink && maxPagesLinkCount == 2 && cardsLength == 12) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_PAIR;    //四张连带对
                        }
                        break;
                    case 5:
                        if (maxPagesIsLink && maxPagesLinkCount == 2) {
                            if (cardsLength == 10)
                                cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_SINGLE;    //四张连带一
                            if (cardsLength == 12)
                                cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_TWOSINGLE;    //四张连带二
                        }
                        if (maxPagesIsLink && maxPagesLinkCount == 3) {
                            if (cardsLength == 16)
                                cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_TWOPAIR;    //四张连带两对
                            if (cardsLength == 15)
                                cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_SINGLE;    //四张连带一
                        }
                        break;
                    case 6:
                        if (maxPagesIsLink && maxPagesLinkCount == 2 && cardsLength == 12) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_TWOSINGLE;    //四张连带二
                        }

                        if (maxPagesIsLink && maxPagesLinkCount == 2 && cardsLength == 16) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_TWOPAIR;    //四张连带两对
                        }

                        if (maxPagesIsLink && maxPagesLinkCount == 3 && cardsLength == 15) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_SINGLE;    //四张连带一 3连
                        }

                        if (maxPagesIsLink && maxPagesLinkCount == 3 && cardsLength == 18) {
                            cardsTypeHEnum = BMDataContext.CardsTypeHEnum.FOUR_LINK_PAIR;    //四张连带两对  3连
                        }
                        break;
                    default:
                        break;
                }
                ;
                break;
            default:
                break;
        }

        if (cardsTypeHEnum == null)
            return null;
        else {
            CardType cardType = new CardType();
            cardType.setCards(cards);
            cardType.setCardTypeEnum(cardsTypeHEnum);
            cardType.setCardtype(cardsTypeHEnum.getType());
            cardType.setGroupPages(maxGroupPages);
            cardType.setMaxValue(maxGroupPagesMaxKey);
            cardType.setMinValue(maxGroupPagesMaxKey - maxPagesLinkCount);  //TODO 头晕，这个考虑用最大牌一连接数获得
            cardType.setLink(maxPagesIsLink);
            cardType.setLinks(maxPagesLinkCount);
            return cardType;
        }

    }

    public static int getWangGroupKey() {
        return 53 / 4;   //大鬼的值 /4
    }

    /**
     * 获得分组后的最大值 cards= 999966633 返回4
     *
     * @param groupMap
     * @param searchTopPages 传值为-1则查最大，否则查找定组
     * @return
     */
    private static Map.Entry<Integer, Integer> getGroupEntry(Map<Integer, Integer> groupMap, int searchTopPages) {
        int groupPagesTop = 0;
        Map.Entry<Integer, Integer> serchEntry = null;

        for (Map.Entry<Integer, Integer> entry : groupMap.entrySet()) {
            if (searchTopPages == -1) {  //查所有中最大组最大数
                if (groupPagesTop < entry.getValue()) { //降序时  //KK999922 maxGroupPages = 4 为9999这组
                    groupPagesTop = entry.getValue();
                    serchEntry = entry;
                }
            } else {                    //找指定第一个最大数相等的
                if (searchTopPages == entry.getValue()) {
                    return entry;       //找到就结束
                }
            }
        }
        return serchEntry;
    }

    /**
     * 临时放这里，重构的时候 放到 游戏类型的 实现类里
     *
     * @param board
     * @return
     */
    public static void doBomb(Board board) {
        board.setRatio(board.getRatio() * 2);
    }

    public static boolean isPeople(Player catchPlayer, GameRoom gameRoom) {
        /**
         * 地主抢完了即可进入玩牌的流程了，否则，一直发送 AUTO事件，进行抢地主
         */
        //FIXME 这个好象根本不需要，进到这里的不是AI，就是超时的，或者离线或者托管的
        boolean isPeople = true;   //是否真人
        List<PlayUserClient> users = CacheHelper.getGamePlayerCacheBean2().getCacheObject(gameRoom.getId());
        for (PlayUserClient playUser : users) {
            if (catchPlayer.getPlayuser().equals(playUser.getId())) {
                //托管或AI
                if (!playUser.getPlayertype().equals(BMDataContext.PlayerTypeEnum.NORMAL.toString())) {
                    //AI或托管，自动抢地主，后台配置 自动抢地主的触发时间，或者 抢还是不抢， 无配置的情况下，默认的是抢地主
                    isPeople = false;
                    break;
                }
            }
        }
        return isPeople;
    }


    ///////////////////////////////////////////////////////////////////////////
    //获得最小压制牌型   以下代码
    /////////////////////////////////////////////
}
