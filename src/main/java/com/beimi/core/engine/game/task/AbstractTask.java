package com.beimi.core.engine.game.task;

import com.beimi.config.web.model.Game;
import com.beimi.core.engine.game.ActionTaskUtils;
import com.beimi.util.UKTools;
import com.beimi.web.model.GameRoom;
import org.cache2k.expiry.ValueWithExpiryTime;

/**
 * 任务抽象类
 * 说明：
 *
 * @author huangzh 黄志海 seenet2004@163.com
 * @date 2017-11-16 1:57
 */
public abstract class AbstractTask implements ValueWithExpiryTime {
    /**
     * 一秒常数.
     * 说明：供子类调用
     */
    public static final int ONE_SECOND = 1000;
    protected Game game;

    public AbstractTask() {
        game = ActionTaskUtils.game();
    }

    public void sendEvent(final String event, final Object data, final GameRoom gameRoom) {
        ActionTaskUtils.sendEvent(event, data, gameRoom);
    }

    public Object json(Object data) {
        return UKTools.json(data);
    }


}
