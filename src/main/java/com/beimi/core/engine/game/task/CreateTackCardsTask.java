package com.beimi.core.engine.game.task;

import com.beimi.core.engine.game.IGameTask;
import org.cache2k.expiry.ValueWithExpiryTime;

import com.beimi.core.BMDataContext;
import com.beimi.web.model.GameRoom;

/**
 * 出牌.
 * 说明：
 * 计时器，默认25秒，超时执行
 *
 * @author zhangtianyi
 */
public class CreateTackCardsTask extends AbstractTask implements ValueWithExpiryTime, IGameTask {

    private long timer;
    private GameRoom gameRoom = null;
    private String orgi;
    private String player;

    public CreateTackCardsTask(long timer, String userid, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
        this.player = userid;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + timer * ONE_SECOND;    //5秒后执行
    }

    public void execute() {
        /**
         * 合并代码，玩家 出牌超时处理和 玩家出牌统一使用一处代码
         */
        BMDataContext.getGameEngine().takeCardsRequest(this.gameRoom.getId(), this.player, orgi, true, null);
    }
}
