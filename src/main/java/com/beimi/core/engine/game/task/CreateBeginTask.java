package com.beimi.core.engine.game.task;

import java.util.List;

import com.beimi.core.BMDataContext;
import com.beimi.core.engine.game.DiZhuCommand;
import com.beimi.util.UKTools;
import com.beimi.web.service.repository.es.PlayUserClientESRepository;
import com.beimi.web.service.repository.jpa.PlayUserClientRepository;
import org.cache2k.expiry.ValueWithExpiryTime;

import com.beimi.core.engine.game.BeiMiGameEvent;
import com.beimi.core.engine.game.IGameTask;
import com.beimi.core.engine.game.impl.UserBoard;
import com.beimi.util.GameUtils;
import com.beimi.util.cache.CacheHelper;
import com.beimi.util.client.NettyClients;
import com.beimi.util.rules.model.Board;
import com.beimi.web.model.GameRoom;
import com.beimi.web.model.PlayUserClient;
/**
 * 人凑齐 开始游戏.
 * 说明：
 * @author huangzh 黄志海 seenet2004@163.com
 * @date 2017-11-16 1:56
 * 
 */
public class CreateBeginTask extends AbstractTask implements ValueWithExpiryTime, IGameTask {

    private long timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public CreateBeginTask(final long timer, final GameRoom gameRoom, final String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public final long getCacheExpiryTime() {
        return System.currentTimeMillis() + timer * ONE_SECOND;    //5秒后执行
    }

    public final void execute() {
        System.out.println("enter execute CreateBeginTask");
        //这里已经完成了三个玩家的凑齐 进行发牌
        //List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean().getCacheObject(gameRoom.getId(), orgi);
        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean2().getCacheObject(gameRoom.getId());
        for (PlayUserClient playUserClient : playerList) {
            playUserClient.setGoldcoins(playUserClient.getGoldcoins() - 300);  //把房费扣了
            /**
             * 更新状态到 用户缓存
             */
            if (CacheHelper.getApiUserCacheBean().getCacheObject(playUserClient.getId(), orgi) != null) {
                CacheHelper.getApiUserCacheBean().put(playUserClient.getId(), playUserClient, orgi);
            }
        }
        /**
         *
         * 顺手 把牌发了，注：此处应根据 GameRoom的类型获取 发牌方式
         */
        Board board = GameUtils.playDiZhuGame(playerList, gameRoom, gameRoom.getCardsnum());

        //存入board
        CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi());

        for (PlayUserClient playUserClient : playerList) {
            /**
             * 每个人收到的 牌面不同，所以不用 ROOM发送广播消息，而是用遍历房间里所有成员发送消息的方式
             * 这里已把房费扣了 ，客户端获取player.goldcoins 更新金币显示
             */
            NettyClients.getInstance().sendGameEventMessage(playUserClient.getId(), DiZhuCommand.PLAY, super.json(new
                    UserBoard(board,
                    playUserClient.getId())));
            //更新数据库 因为更新了玩家金币
            UKTools.published(playUserClient, BMDataContext.getContext().getBean(PlayUserClientESRepository.class),
                    BMDataContext.getContext().getBean(PlayUserClientRepository.class));
        }

        CacheHelper.getGameRoomCacheBean().put(gameRoom.getId(), gameRoom, gameRoom.getOrgi());
        /**
         * 发送一个 Begin 事件
         * 告诉状态机通知抢地主  客户端发牌动画差不多一1.5秒，所以这里设2秒后通知抢地方
         */
        game.change(gameRoom, BeiMiGameEvent.NOTIFYCATCH.toString(), 2);
    }
}
