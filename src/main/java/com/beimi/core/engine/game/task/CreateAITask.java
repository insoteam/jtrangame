package com.beimi.core.engine.game.task;

import java.util.List;

import com.beimi.core.engine.game.DiZhuCommand;
import com.beimi.core.engine.game.IGameTask;
import org.cache2k.expiry.ValueWithExpiryTime;

import com.beimi.core.BMDataContext;
import com.beimi.core.engine.game.BeiMiGameEvent;
import com.beimi.util.GameUtils;
import com.beimi.util.cache.CacheHelper;
import com.beimi.web.model.GameRoom;
import com.beimi.web.model.PlayUser;
import com.beimi.web.model.PlayUserClient;
import com.corundumstudio.socketio.SocketIOServer;

/**
 * 创建AI任务.
 * 说明：当5秒没搓合成功时，创建AI玩家
 *
 * @author huangzh 黄志海 seenet2004@163.com
 * @date 2017-11-15 8:34
 */
public class CreateAITask extends AbstractTask implements ValueWithExpiryTime, IGameTask {

    private long timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public CreateAITask(long timer, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + timer * ONE_SECOND;    //timer秒后执行
    }


    public void execute() {
        //执行生成AI
        GameUtils.removeGameRoomQuene(gameRoom.getId(), orgi);  //移除房间队列不排队了 FIXME 这个函数好象可以不用这么搞
        //获得房间中已有的玩家
        List<PlayUserClient> playerList = CacheHelper.getGamePlayerCacheBean2().getCacheObject(gameRoom.getId());
        int aiCount = gameRoom.getPlayers() - playerList.size();   //获得不足的玩家人数
        for (int i = 0; i < aiCount; i++) {
            PlayUserClient playerUser = GameUtils.createAiUser();
            playerUser.setPlayerindex(gameRoom.getPlayers() - playerList.size());
            CacheHelper.getGamePlayerCacheBean2().put(gameRoom.getId(), playerUser); //将用户加入到 room ， MultiCache
            BMDataContext.getContext().getBean(SocketIOServer.class).getRoomOperations(gameRoom.getId()).sendEvent(DiZhuCommand
                    .JOIN_ROOM, super.json
                    (playerUser));
            playerList.add(playerUser);
        }
        /**
         * 发送一个 Enough 事件
         */
        game.change(gameRoom, BeiMiGameEvent.ENOUGH.toString());    //通知状态机 , 此处应由状态机处理异步执行
    }
}
