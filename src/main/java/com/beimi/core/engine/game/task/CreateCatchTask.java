package com.beimi.core.engine.game.task;

import com.beimi.core.engine.game.*;
import com.beimi.util.GameUtils;
import com.beimi.util.cache.CacheHelper;
import com.beimi.util.rules.model.Board;
import com.beimi.util.rules.model.Player;
import com.beimi.web.model.GameRoom;

/**
 * 抢地主任务.
 * 说明：
 *
 * @author huangzh 黄志海 seenet2004@163.com
 * @date 2017-11-15 15:51
 */
public class CreateCatchTask extends AbstractTask implements IGameTask {

    private long timer;
    private GameRoom gameRoom = null;

    public CreateCatchTask(long timer, GameRoom gameRoom) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + timer * ONE_SECOND;    //timer秒后执行  //进房间等待5秒，抢地主1秒
    }


    public void execute() {
        System.out.println("enter execute CreateCatchTask");
        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        if (board == null)
            return;

        /**
         * 抢地主，首个抢地主的人 在发牌的时候已经生成
         */
        Player catchPlayer = ActionTaskUtils.getCatchPlayer(board);  //从初始玩家开始一个个确认下一个抢地主的玩家
        if (catchPlayer != null) {  //还没抢完庄
            //自动不抢 不管真人假人  随机抢不抢  FIXME 以后考虑看是要怎么搞
            board = ActionTaskUtils.doCatch(board, catchPlayer, GameUtils.randomInst.nextBoolean());

            //AI或托管，发送抢地主结果
            sendEvent(DiZhuCommand.CATCH_RESULT, json(new GameBoard(catchPlayer.getPlayuser(), catchPlayer.getSex(),
                    catchPlayer.isAccept(), catchPlayer.isAccept(), board.getRatio(), board.getCatchtimes())), gameRoom);
            /**
             * 这里因为是AI或托管，本该抢完地主后，再次发送catch通知下家抢地主的
             * 为了赖得再写一遍，直接调用状态机，帮忙再来一次，就等同于通知下家抢了
             * 也可以不用再这个通知后再判断是否已经没有下家要抢地主了
             */
            //通知继续抢
            game.change(gameRoom, BeiMiGameEvent.NOTIFYCATCH.toString(), 1);
            CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, gameRoom.getOrgi()); //FIXME 为何要put回去，难道是克隆了吗  答：因为这hazelcast 的集成分布式缓存机制
        }
    }

}
