package com.beimi.core.engine.game;
/**
 * 牌的类型
 * @Author huangzh 黄志海 seenet2004@163.com
 * @Date 2017-10-19 10:03
 * 说明：
 */
import com.beimi.core.BMDataContext;

public class CardType implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	private byte[] cards;    //原始牌
	private int cardtype;    //牌型
	private BMDataContext.CardsTypeHEnum cardTypeEnum ;  //牌型 ENUM值  FIXME 没想明白为何 switch case BMDataContext.CardsTypeHEnum.xxx.getType会提示不对
	private int maxValue;    //最大值  即card / 4的值
	private int minValue;    //最小值  即card / 4的值
	private int groupPages;  //最大组牌张数
	private int groups;       //分组总数
	private boolean isLink;  //是否连
	private int links;        //几个连 JJJ|QQQ 为2
	private int cardnum;      //共几张牌


	public BMDataContext.CardsTypeHEnum getCardTypeEnum() {
		return cardTypeEnum;
	}

	public void setCardTypeEnum(BMDataContext.CardsTypeHEnum cardTypeEnum) {
		this.cardTypeEnum = cardTypeEnum;
	}

	public int getMinValue() {
		return minValue;
	}

	public void setMinValue(int minValue) {
		this.minValue = minValue;
	}
	public int getGroupPages() {
		return groupPages;
	}

	public void setGroupPages(int groupPages) {
		this.groupPages = groupPages;
	}

	public int getGroups() {
		return groups;
	}

	public void setGroups(int groups) {
		this.groups = groups;
	}


	public byte[] getCards() {
		return cards;
	}

	public void setCards(byte[] cards) {
		this.cards = cards;
	}

	public int getCardtype() {
		return cardtype;
	}

	public void setCardtype(int cardtype) {
		this.cardtype = cardtype;
	}

	public int getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}

	public boolean isLink() {
		return isLink;
	}

	public void setLink(boolean link) {
		isLink = link;
	}

	public int getLinks() {
		return links;
	}

	public void setLinks(int links) {
		this.links = links;
	}

	public int getCardnum() {
		return cards.length;
	}


}
