package com.beimi.core.engine.game;

import com.beimi.core.BMDataContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author huangzh on 2017-10-21 3:59
 * @email seenet2004@163.com
 * 说明：
 */
public class SearchCardType {
    public static SearchCardType _instance = null;

    public static SearchCardType getInstance() {
        if (_instance == null) {
            _instance = new SearchCardType();
        }
        return _instance;
    }

    /**
     * 查找可压制对手的牌
     * 说明：根据上家出的牌型 ，查找本家手牌可压制的牌
     * @param Cards  本家手里的牌
     * @param lastCardType  上家出的牌型
     * @return 出的牌
     */
    public byte[] searchSupperCardsByCardType(byte[] Cards, CardType lastCardType) {
        //获得最小压制牌形
        CardType suppressCardType = getMinSuppressCardType(Cards, lastCardType);

        if (suppressCardType == null)
            return null;
        else
            return suppressCardType.getCards();
    }

    /**
     * 获得最小压制牌型
     * @param cards 本家手里的牌
     * @param lastTakeCardsCardType 上家出的牌型
     * @return
     */
    public CardType getMinSuppressCardType(byte[] cards, CardType lastTakeCardsCardType) {
        //1 先找同类型最小压制的牌
        //1.1先找在我方是否有压制对手牌的对应牌形 依据 grouppages,groups,islink,links,cardnum，maxgroupvalue
        byte[] resultCards = angleAndGetCards(cards, lastTakeCardsCardType);

        return ActionTaskUtils.identification(resultCards);
    }

    private byte[] angleAndGetCards(byte[] cards, CardType lastTakeCardsCardType) {
        Map<Integer, Integer> groupMap = ActionTaskUtils.groupCard(cards, false);  //对牌进行分组，按升序
        //1、普通牌或BOM
        byte[] resultMainCards = getMainCards(cards, lastTakeCardsCardType, groupMap);

        //1.1如果对手是BOM 且1找到了压制牌，说明我方有压制的BOM，可直接返回
        if (lastTakeCardsCardType.getCardTypeEnum() == BMDataContext.CardsTypeHEnum.BOM && (resultMainCards != null && resultMainCards.length > 0)) {
            return resultMainCards;
        }

        //2、找辅牌或找王炸
        /**
         * 2找辅牌  例：333KK 主牌为333 辅牌为KK
         * 条件:有找到主牌，且主牌牌张数还不足对手牌张数时
         */
        //byte[] accCards = getAccCards(cards, resultMainCards, lastTakeCardsCardType);

        byte[] supCards = getAccCards(cards, resultMainCards, lastTakeCardsCardType);  //byteMerger(resultMainCards, accCards)

        /**
         * 3、如果为空，说明没同型牌，那找看看有没有炸或王炸
         *  前两1,2两步解决了，对手是普通牌或者为普通BOM的情况
         *  下面解决如果对手是普通牌，我方用BOM或王BOM压制
         */
        //查炸弹压制
        if (supCards == null || supCards.length == 0) {
            if (lastTakeCardsCardType.getCardTypeEnum() != BMDataContext.CardsTypeHEnum.BOM && lastTakeCardsCardType.getCardTypeEnum() != BMDataContext.CardsTypeHEnum.BOM_WANG) {
                //supCards = findBomCard();
                supCards = getBomCards(cards, lastTakeCardsCardType, groupMap);
            }
        }

        //找王炸压制
        if (supCards == null || supCards.length == 0) {
            //王炸
            Integer value = groupMap.get(53 / 4);
            if (value != null && value == 2) {
                return new byte[]{52, 53};
            }
        }
        return supCards;
    }

    private byte[] getBomCards(byte[] cards, CardType lastTakeCardsCardType, Map<Integer, Integer> groupMap) {
        byte[] supCards;
        List<Integer> keyList = new ArrayList<Integer>();
        for (Map.Entry<Integer, Integer> entry : groupMap.entrySet()) {
            if (entry.getValue() == 4) {
                keyList.add(entry.getKey());
                break;
            }
        }
        return findCardsByKeys(cards, keyList, 4);
    }

    private byte[] getMainCards(byte[] cards, CardType lastTakeCardsCardType, Map<Integer, Integer> groupMap) {
        List<Integer> keyList = new ArrayList<Integer>();

        byte[] resultMainCards = new byte[0];
        //1 找主要的牌
        for (Map.Entry<Integer, Integer> entry : groupMap.entrySet()) {
            if (entry.getKey() > lastTakeCardsCardType.getMinValue() && entry.getValue() == lastTakeCardsCardType.getGroupPages()) {
                //连牌的情况
                if (lastTakeCardsCardType.isLink()) {
                    if (keyList.size() <= lastTakeCardsCardType.getLinks()) {
                        if (keyList.size() == 0) {
                            keyList.add(entry.getKey());
                        } else {
                            //if 判断连续
                            if (keyList.get(keyList.size() - 1) == entry.getKey() - 1   //上一个值==本次值减1 说明还是连续的
                                    && entry.getKey() != 53 / 4) {  //王对是不能连的
                                keyList.add(entry.getKey());
                            } else {
                                keyList.clear();
                            }
                        }
                    }
                } else {
                    if (entry.getKey() > lastTakeCardsCardType.getMinValue() && entry.getValue() == lastTakeCardsCardType.getGroupPages()) {
                        keyList.add(entry.getKey());
                        break;
                    }
                }
            }

            if (entry.getKey()==53/4 && entry.getValue()==1 && entry.getKey() == lastTakeCardsCardType.getMinValue() && entry.getValue() == lastTakeCardsCardType.getGroupPages()){
                if (lastTakeCardsCardType.getCards()[0]==52){   //大鬼压小鬼
                    keyList.add(entry.getKey());
                    break;
                }
            }
        }

        if (lastTakeCardsCardType.isLink()) {
            if (lastTakeCardsCardType.getLinks() > 0 && keyList.size() == lastTakeCardsCardType.getLinks()) {
                resultMainCards = findCardsByKeys(cards, keyList, lastTakeCardsCardType.getGroupPages());
            }
            //连牌情况要考虑其它单张或对的处理

        } else {
            resultMainCards = findCardsByKeys(cards, keyList, lastTakeCardsCardType.getGroupPages());

        }
        return resultMainCards;
    }

    /**
     * @param cards
     * @param keyList
     * @param pages   每个List值 对应的张数
     * @return
     */
    private byte[] findCardsByKeys(byte[] cards, List<Integer> keyList, int pages) {
        if (keyList == null)
            return new byte[0];

        int index = 0;
        byte[] resultCards = new byte[pages * keyList.size()];
        for (int i = 0; i < cards.length; i++) {  //FIXME 这个算法好象每次都是个完整的循环，不大好 ，有空看看是否可优化
            for (Integer key : keyList) {
                if (key == cards[i] / 4 && index < resultCards.length) {
                    resultCards[index++] = cards[i];
                }
            }
        }
        return resultCards;
    }

    private byte[] getAccCards(byte[] cards, byte[] resultMainCards, CardType lastTakeCardsCardType) {
        //主牌为空或者主牌与足以压制对手 直接返回主牌
        if (resultMainCards.length <= 0 || resultMainCards.length >= lastTakeCardsCardType.getCards().length) {
            return resultMainCards;
        }

        //2.1移除主牌用到的牌
        byte[] remCards = removeCards(cards, resultMainCards);
        //2.2余牌重新分组
        byte[] accCards = new byte[0];
        Map<Integer, Integer> remGroupMap = ActionTaskUtils.groupCard(remCards, false);  //对牌进行分组，按升序

        List<Integer> cardList = new ArrayList<Integer>();

        //辅牌有几个对    说明：处理：三带，四带的情况 ; 有对就不会有单张，无对就是取单张，根据牌型决定
        int cardPairs = lastTakeCardsCardType.getCardTypeEnum().getAcc();
        int accPages = lastTakeCardsCardType.getCards().length - resultMainCards.length;   //辅牌张数
        int num = 0;

        for (Map.Entry<Integer, Integer> entry : remGroupMap.entrySet()) {

            if (cardPairs == 1) {  //辅牌为单
                if (num < accPages) {
                    if (entry.getValue() == 1) {
                        num++;
                        cardList.add(entry.getKey());
                    }
                } else {
                    break;
                }
            }

            if (cardPairs == 2) {//辅牌为对
                if (num * 2 < accPages) {
                    if (entry.getValue() == 2) {
                        num++;
                        cardList.add(entry.getKey());
                    }
                } else {
                    break;
                }
            }
        }

        accCards = findCardsByKeys(remCards, cardList, cardPairs);

        if (accPages != accCards.length) {  //找了辅牌，发现辅牌不足 干脆不出，放弃拆辅牌对或三
            return new byte[0];
        } else {
            return byteMerger(resultMainCards, accCards);  //合并
        }
    }

    /**
     * 移除出牌
     *
     * @param cards
     * @param playcards
     * @return
     */
    private byte[] removeCards(byte[] cards, byte[] playcards) {
        byte[] retCards = new byte[cards.length - playcards.length];
        int cardsindex = 0;
        for (int i = 0; i < cards.length; i++) {
            if (!isFound(cards[i], playcards)) {  //没找到就把牌记下，做为下次的手牌
                retCards[cardsindex++] = cards[i];
            }
        }
        return retCards;
    }

    private boolean isFound(byte card, byte[] playcards) {
        boolean found = false;
        for (int inx = 0; inx < playcards.length; inx++) {
            if (card == playcards[inx]) {
                found = true;
                break;
            }
        }
        return found;
    }

    private byte[] byteMerger(byte[] byte_1, byte[] byte_2) {
        byte[] byte_3 = new byte[byte_1.length + byte_2.length];
        System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
        System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
        return byte_3;
    }
}
