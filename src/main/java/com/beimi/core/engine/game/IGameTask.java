package com.beimi.core.engine.game;

/**
 * 游戏任务接口.
 */
public interface IGameTask {
    /**
     * 执行任务.
     */
    void execute();
}

