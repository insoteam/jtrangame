package com.beimi.core.engine.game.task;

import com.beimi.core.engine.game.DiZhuCommand;
import com.beimi.core.engine.game.IGameTask;
import com.beimi.util.UKTools;
import com.beimi.util.rules.model.Player;
import com.beimi.web.model.PlayUserClient;
import com.beimi.web.service.repository.es.PlayUserClientESRepository;
import com.beimi.web.service.repository.jpa.PlayUserClientRepository;
import org.cache2k.expiry.ValueWithExpiryTime;

import com.beimi.core.BMDataContext;
import com.beimi.util.cache.CacheHelper;
import com.beimi.util.rules.model.Board;
import com.beimi.web.model.GameRoom;

import java.util.List;

/**
 * .
 * 牌全出完了，进行结算
 */
public class CreateAllCardsTask extends AbstractTask implements ValueWithExpiryTime, IGameTask {

    private long timer;
    private GameRoom gameRoom = null;
    private String orgi;

    public CreateAllCardsTask(long timer, GameRoom gameRoom, String orgi) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
        this.orgi = orgi;
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + timer * ONE_SECOND;    //5秒后执行
    }

    public void execute() {
        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        board.setFinished(true);

        /**
         * 结算信息 ， 更新 玩家信息
         */

        //计算金币
        board.caleGold();

        List<PlayUserClient> playerList = (List<PlayUserClient>) CacheHelper.getGamePlayerCacheBean2().getCacheObject(gameRoom.getId());
        for (PlayUserClient playUserClient : playerList) {
            Player player = board.getPlayer(playUserClient.getId());
            playUserClient.setGoldcoins(playUserClient.getGoldcoins() + player.getGoldchangs());  //结算金币
            player.setGoldcoins(playUserClient.getGoldcoins());
            /**
             * 更新状态到 用户缓存
             */
            if (CacheHelper.getApiUserCacheBean().getCacheObject(playUserClient.getId(), orgi) != null) {
                CacheHelper.getApiUserCacheBean().put(playUserClient.getId(), playUserClient, orgi);
            }

            //更新数据库
            UKTools.published(playUserClient, BMDataContext.getContext().getBean(PlayUserClientESRepository.class),
                    BMDataContext.getContext().getBean(PlayUserClientRepository.class));
        }

        CacheHelper.getGamePlayerCacheBean2().put(gameRoom.getId(), playerList);   //更新缓存
        CacheHelper.getBoardCacheBean().put(gameRoom.getId(), board, orgi);        //更新缓存

        sendEvent(DiZhuCommand.ALL_CARDS, UKTools.json(board), gameRoom);    //通知所有客户端结束牌局，进入结算

        if (board.isFinished()) {
            BMDataContext.getGameEngine().finished(gameRoom.getId(), orgi);
        }
    }


}
