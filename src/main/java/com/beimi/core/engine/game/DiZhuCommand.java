package com.beimi.core.engine.game;

/**
 * 斗地主的消息 command.
 * 根据游戏类型不同，状态下的事件有所不同
 *
 * @author huangzh
 */
public final class DiZhuCommand {
    /**
     * 加入房间地COMMAND.
     * 说明：发送给客户端通知有其它玩家加入的COMMAND
     */
    public static final String JOIN_ROOM = "joinroom";

    /**
     * 每个玩家抢地主的结果COMMAND.
     * 说明：发送给客户端通知每个玩家抢地主的结果
     */
    public static final String CATCH_RESULT = "catchresult";

    /**
     * 每个玩家通知抢地主的COMMAND.
     * 说明：发送给客户端通知现在该谁抢地主，客户端根据附带的用户ID决定界面的变化
     */
    public static final String CATCH = "catch";

    /**
     * 抢地主流牌的COMMAND.
     * 说明:发送给客户端通知抢地主流局消息，客户端根据这个标志决定界面清理
     */
    public static final String CATCH_FAIL = "catchfail";

    /**
     * 每个玩家出牌的COMMAND.
     * 说明：发送给客户端通知当前出牌玩家出牌信息
     */
    public static final String TAKE_CARDS = "takecards";

    /**
     * 出牌过程中有炸弹的COMMAND.
     * 说明：发送给客户端通知当前玩家出炸弹或王炸了，客户端做相应倍数修改显示  FIXME 可能可以不这么搞
     */
    public static final String BOMB = "bomb";

    /**
     * 牌打完了的COMMAND.
     * 说明：通知客户端本局有玩家牌打完了，进行结算处理
     */
    public static final String ALL_CARDS = "allcards";

    /**
     * 金币不足的COMMAND.
     * 说明：通知客户端玩家，金币不足了
     */
    public static String GOLD_NOT_ENOUGH = "goldnotenough";

    /**
     * 底牌COMMAND.
     * 说明：通知客户端玩家，开始发底牌了
     * 对于客户端来说发底牌就意味着最终谁时地主，并把底牌发给他
     */
    public static String LAST_HANDS = "lasthands";
    /**
     * 开牌COMMAND
     * 说明：通知客户端玩家，开发了
     */
    public static String PLAY = "play";
    /**
     * 游戏状态COMMAND
     * 说明：客户端发送消息，服务端回复消息
     */
    public static java.lang.String GAME_STATUS = "gamestatus";
    /**
     * 游戏玩家COMMAND
     * 说明：通知客户端玩家，本局其它玩家信息
     */
    public static java.lang.String PLAYERS = "players";
    /**
     * 游戏恢复牌局COMMAND
     * 根据游戏状态，决定加入房单是是新起还是恢复牌局
     * 说明：客户端发加入牌局，服务端根据牌局状态恢复数据
     */
    public static String RECOVERY = "recovery";

    /**
     * 隐藏构造器.
     */
    private DiZhuCommand() {
    }
}
