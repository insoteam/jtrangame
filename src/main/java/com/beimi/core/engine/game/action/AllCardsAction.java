package com.beimi.core.engine.game.action;

import com.beimi.core.BMDataContext;
import com.beimi.core.engine.game.task.CreateAllCardsTask;
import com.beimi.core.statemachine.action.Action;
import com.beimi.core.statemachine.impl.BeiMiExtentionTransitionConfigurer;
import com.beimi.core.statemachine.message.Message;
import com.beimi.util.cache.CacheHelper;
import com.beimi.web.model.GameRoom;
import org.apache.commons.lang3.StringUtils;

/**
 * 反底牌发给地主.
 *
 * @param <T>
 * @param <S>
 * @author iceworld
 */
public class AllCardsAction<T, S> implements Action<T, S> {

    /**
     * 执行.
     *
     * @param message    消息处理器
     * @param configurer 配置
     */
    @Override
    public final void execute(final Message<T> message, final BeiMiExtentionTransitionConfigurer<T, S> configurer) {
        String room = (String) message.getMessageHeaders().getHeaders().get("room");
        if (!StringUtils.isBlank(room)) {
            GameRoom gameRoom = (GameRoom) CacheHelper.getGameRoomCacheBean()
                    .getCacheObject(room, BMDataContext.SYSTEM_ORGI);
            if (gameRoom != null) {
                CacheHelper.getExpireCache().put(gameRoom.getRoomid(), new CreateAllCardsTask(0,
                        gameRoom, gameRoom.getOrgi()));
            }
        }
    }
}
