package com.beimi.core.engine.game.task;

import com.beimi.core.engine.game.*;
import com.beimi.util.cache.CacheHelper;
import com.beimi.util.rules.model.Board;
import com.beimi.util.rules.model.Player;
import com.beimi.web.model.GameRoom;

import java.util.Random;

/**
 * 抢地主通知.
 * @author huangzh on 2017-11-14 10:26
 * @email seenet2004@163.com
 * 说明：
 */
public class CreateNotifyCatchTask extends AbstractTask implements IGameTask {
    private GameRoom gameRoom = null;
    private int timer;

    public CreateNotifyCatchTask(int timer, GameRoom gameRoom) {
        super();
        this.timer = timer;
        this.gameRoom = gameRoom;
    }

    @Override
    public void execute() {
        System.out.println("enter execute CreateCatchTask");
        Board board = (Board) CacheHelper.getBoardCacheBean().getCacheObject(gameRoom.getId(), gameRoom.getOrgi());
        if (board == null)
            return;
        /**
         * 抢地主，首个抢地主的人 在发牌的时候已经生成
         */
        Player catchPlayer = ActionTaskUtils.getCatchPlayer(board);  //从初始玩家开始一个个确认下一个抢地主的玩家
        if (catchPlayer != null) {  //还没抢完庄
            //发送客户端，通知当前该谁抢地主
            ActionTaskUtils.sendEvent(DiZhuCommand.CATCH, json(new GameBoard(catchPlayer.getPlayuser(), catchPlayer.getSex(),
                    board.isDocatch(), catchPlayer.isAccept(), board.getRatio(), board.getCatchtimes())), gameRoom);
            if (ActionTaskUtils.isPeople(catchPlayer, gameRoom)) {
                //如果是真人，那15秒后如果他不抢，就执行自动抢
                game.change(gameRoom, BeiMiGameEvent.CATCH.toString(), DiZhuTimer.CATCH_TIME);
            } else {
                //如果不是真人，那1秒后就自动抢 FIXME 为了效果真实性，可考虑把这个时间设置为一个15秒内的随机数
                game.change(gameRoom, BeiMiGameEvent.CATCH.toString(), new Random().nextInt(5)); //0至5之间的随机数
            }
        } else {
            //流局处理
            ActionTaskUtils.catchDrawOrPlay(board, gameRoom, game);
        }
    }

    @Override
    public long getCacheExpiryTime() {
        return System.currentTimeMillis() + timer * ONE_SECOND;    //timer秒后执行
    }
}
