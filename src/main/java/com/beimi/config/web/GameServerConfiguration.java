package com.beimi.config.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

import javax.annotation.PreDestroy;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import com.beimi.core.BMDataContext;
import com.beimi.util.UKTools;
import com.corundumstudio.socketio.AuthorizationListener;
import com.corundumstudio.socketio.Configuration;
import com.corundumstudio.socketio.HandshakeData;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.SpringAnnotationScanner;

/**
 * 服务器配置文件。
 * 说明：
 * 1、实现获得application.properties属性文件的配置值
 * 2、初始化三个BEAN
 * 1、webimport 就一个消息端口
 * 2、SocketIOServer 即nettly io
 * 3、SpringAnnotationScanner
 *
 * @author huangzh 黄志海 seenet2004@163.com
 * @date 2017-11-27 16:21
 */
@org.springframework.context.annotation.Configuration
public class GameServerConfiguration {
    @Value("${uk.im.server.host}")     //获得application.properties 配置文件的值
    private String host;

    @Value("${uk.im.server.port}")     //获得application.properties 配置文件的值
    private Integer port;

    @Value("${web.upload-path}")       //获得application.properties 配置文件的值
    private String path;

    @Value("${uk.im.server.threads}")  //获得application.properties 配置文件的值
    private String threads;

    private SocketIOServer server;

    @Bean(name = "webimport")
    public Integer getWebIMPort() {
        BMDataContext.setWebIMPort(port);
        return port;
    }

    @Bean
    public SocketIOServer socketIOServer() throws NoSuchAlgorithmException, IOException {
        Configuration config = new Configuration();
        //config.setHostname("localhost");            //FIXME 注：这个很坑爹 这个如果设了，在服务器上就是永远的拒绝连接
        config.setPort(port);

//		config.setSocketConfig(new SocketConfig());   //FIXME 以后应改成这种写法 对应最后面的几个优分设置
//		config.setOrigin("http://im.uckefu.com");
        //异常消息监听
        config.setExceptionListener(new SocketIoServerExceptionListener());

        File sslFile = new File(path, "ssl/https.properties");
        if (sslFile.exists()) {
            Properties sslProperties = new Properties();
            FileInputStream in = new FileInputStream(sslFile);
            sslProperties.load(in);
            in.close();
            if (!StringUtils.isBlank(sslProperties.getProperty("key-store")) && !StringUtils.isBlank(sslProperties.getProperty("key-store-password"))) {
                config.setKeyStorePassword(UKTools.decryption(sslProperties.getProperty("key-store-password")));
                InputStream stream = new FileInputStream(new File(path, "ssl/" + sslProperties.getProperty("key-store")));
                config.setKeyStore(stream);
            }
        }

//	    config.setSSLProtocol("https");
        int workThreads = !StringUtils.isBlank(threads) && threads.matches("[\\d]{1,6}") ? Integer.parseInt(threads) : 100;
        config.setWorkerThreads(workThreads);
//		config.setStoreFactory(new HazelcastStoreFactory());
        config.setAuthorizationListener(new AuthorizationListener() {
            public boolean isAuthorized(HandshakeData data) {
                return true;    //其他安全验证策略，IP，Token，用户名
            }
        });
        /**
         * 性能优化
         */
        config.getSocketConfig().setReuseAddress(true);   //使用固定端口
        config.getSocketConfig().setSoLinger(0);          //决定socket close 延迟
        config.getSocketConfig().setTcpNoDelay(true);     //游戏需要设置每个包不管包大小立即发送
        config.getSocketConfig().setTcpKeepAlive(true);   //保持连接

        return server = new SocketIOServer(config);       //配制文件设置完后，将他给SOCKETIO，构造器会复制一份
    }

    @Bean
    public SpringAnnotationScanner springAnnotationScanner(SocketIOServer socketServer) {
        return new SpringAnnotationScanner(socketServer);
    }

    @PreDestroy
    public void destory() {
        server.stop();
    }
}  