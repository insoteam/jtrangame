package com.beimi.config.web.model;

import com.beimi.core.engine.game.state.GameEvent;
import com.beimi.core.statemachine.impl.BeiMiMachineHandler;
import com.beimi.core.statemachine.impl.MessageBuilder;
import com.beimi.web.model.GameRoom;

public class Game {

    private final BeiMiMachineHandler handler;

    public Game(BeiMiMachineHandler handler) {
        this.handler = handler;
    }

    public void change(GameRoom gameRoom, String event) {
        change(gameRoom, event, 0);
    }

    public void change(GameRoom gameRoom, String event, int sleepTime) {
        handler.handleEventWithState(MessageBuilder.withPayload(event).setHeader("room", gameRoom.getId()).setHeader("interval", sleepTime).build(), event);
    }

    public void change(GameEvent gameEvent) {
        change(gameEvent, 0);
    }

    public void change(GameEvent gameEvent, int sleepTime) {
        handler.handleEventWithState(MessageBuilder.withPayload(gameEvent.getEvent()).setHeader("room", gameEvent.getRoomid()).setHeader("interval", sleepTime).build(), gameEvent.getEvent());
    }
}
