package com.beimi.config.web;

import com.lmax.disruptor.ExceptionHandler;

public class BeiMiExceptionHandler implements ExceptionHandler<Object>{

	/**
	 * @param ex
	 * @param arg1
	 * @param arg2
	 */
	@Override
	public void handleEventException(Throwable ex, long arg1, Object arg2) {
		ex.printStackTrace();
	}

	@Override
	public void handleOnShutdownException(Throwable ex) {
		
	}

	@Override
	public void handleOnStartException(Throwable ex) {
		// TODO Auto-generated method stub
		
	}

}
