package com.beimi.config.web;

import com.beimi.core.engine.game.action.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.beimi.core.engine.game.BeiMiGameEnum;
import com.beimi.core.engine.game.BeiMiGameEvent;
import com.beimi.core.statemachine.BeiMiStateMachine;
import com.beimi.core.statemachine.config.StateConfigurer;
import com.beimi.core.statemachine.config.StateMachineTransitionConfigurer;

@Configuration
public class BeiMiStateMachineConfig<T, S> {

    @Bean
    public BeiMiStateMachine<String, String> create() throws Exception {
        BeiMiStateMachine<String, String> beiMiStateMachine = new BeiMiStateMachine<String, String>();
        this.configure(beiMiStateMachine.getConfig());
        this.configure(beiMiStateMachine.getTransitions());
        return beiMiStateMachine;
    }

    public void configure(StateConfigurer<String, String> statesConfigurer)
            throws Exception {
        statesConfigurer
                .withStates()
                .initial(BeiMiGameEnum.NONE.toString())
                .state(BeiMiGameEnum.CRERATED.toString())
                .state(BeiMiGameEnum.WAITTING.toString())
                .state(BeiMiGameEnum.READY.toString())
                .state(BeiMiGameEnum.BEGIN.toString())
                .state(BeiMiGameEnum.PLAY.toString())
                .state(BeiMiGameEnum.END.toString());
    }

    public void configure(StateMachineTransitionConfigurer<String, String> stateMachineTransitionConfigurer)
            throws Exception {
        /**
         * 状态切换：这里标识了每个动作的source 与 target
         * //FIXME 至于是否顺序执行，好象不是由这个控制的 这里好象只是配制文件
         *
         */
        stateMachineTransitionConfigurer
                .withExternal()
                .source(BeiMiGameEnum.NONE.toString()).target(BeiMiGameEnum.CRERATED.toString())
                .event(BeiMiGameEvent.ENTER.toString()).action(new EnterAction<String, String>()) //这里的动作都是target后的事件
                .and()
                .withExternal()
                .source(BeiMiGameEnum.CRERATED.toString()).target(BeiMiGameEnum.WAITTING.toString())
                .event(BeiMiGameEvent.JOIN.toString()).action(new JoinAction<String, String>())
                .and()
                .withExternal()
                .source(BeiMiGameEnum.WAITTING.toString()).target(BeiMiGameEnum.READY.toString())
                .event(BeiMiGameEvent.ENOUGH.toString()).action(new EnoughAction<String, String>())
                .and()
                .withExternal()
                .source(BeiMiGameEnum.READY.toString()).target(BeiMiGameEnum.BEGIN.toString())
                //通知抢地主
                .event(BeiMiGameEvent.NOTIFYCATCH.toString()).action(new NotifyCatchAction<String, String>())
                .and()
                .withExternal()
                .source(BeiMiGameEnum.BEGIN.toString()).target(BeiMiGameEnum.CATCH.toString())
                .event(BeiMiGameEvent.CATCH.toString()).action(new CatchAction<String, String>())   //抢地主
                .and()
                .withExternal()
                .source(BeiMiGameEnum.CATCH.toString()).target(BeiMiGameEnum.LASTHANDS.toString())
                .event(BeiMiGameEvent.LASTHAND.toString()).action(new RaiseHandsAction<String, String>())
                .and()
                .withExternal()
                .source(BeiMiGameEnum.LASTHANDS.toString()).target(BeiMiGameEnum.PLAY.toString())
                .event(BeiMiGameEvent.PLAYCARDS.toString()).action(new PlayCardsAction<String, String>())
                .and()
                .withExternal()
                .source(BeiMiGameEnum.PLAY.toString()).target(BeiMiGameEnum.END.toString())
                .event(BeiMiGameEvent.ALLCARDS.toString()).action(new AllCardsAction<String, String>())
                .and()
        ;
    }
}
