package com.beimi.web.handler.api;

import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.beimi.core.BMDataContext;
import com.beimi.util.UKTools;
import com.beimi.util.cache.CacheHelper;
import com.beimi.web.handler.Handler;
import com.beimi.web.handler.api.rest.user.ApiRegisterController;
import com.beimi.web.model.PlayUser;
import com.beimi.web.service.repository.es.PlayUserESRepository;
import com.beimi.web.service.repository.jpa.PlayUserRepository;

/**
 * 用户登陆API控制处理类 Controller
 */
@RestController
@RequestMapping("/tokens")
public class ApiLoginController extends Handler {

    @Autowired
    private PlayUserESRepository playUserESRes;   //用户查询服务

    @Autowired
    private PlayUserRepository playUserRes;      //用户spring data jpa

    @SuppressWarnings("rawtypes")
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity login(HttpServletRequest request, HttpServletResponse response, @Valid PlayUser playuser) {
        ResponseEntity entity;  //要返回的值
        /**
         * 1、获得用户对象
         *    确认用户是否注册
         *    没注册进行注册，有注册继续
         */
        if (!StringUtils.isBlank(playuser.getMobile()) && !StringUtils.isBlank(playuser.getPassword())) {
            //查数据库是否存在  根据手机号及密码查用户
            PlayUser tempPlayer = playUserESRes.findByMobileAndPassword(playuser.getMobile(), UKTools.encrypt(playuser.getPassword()));
            if (tempPlayer == null) {       //用户在数据库中不存在
                playuser = new ApiRegisterController().register(playuser);  //进行注册
            } else {
                playuser = tempPlayer;      //用户存在 用最新的数据更新
            }
        }

        /**
         * 2、更新用户信息
         * 更新用户状态，援权、更新用户缓存，更新数据库
         */
        if (playuser != null) {
            playuser.setLogin(true);         //已登录
            playuser.setOnline(false);       //未在游戏状态   // FIXME 这个能这么认为吗？可能断线重连重新上线了，这个时候如果游戏还没结束应该允许回到游戏中
            playuser.setLastlogintime(new Date());

            //发布消息队列，同时存ES和数据库，或其他持久化数据系统
            UKTools.published(playuser, playUserESRes, playUserRes, BMDataContext.UserDataEventType.SAVE.toString());

            String token = UKTools.getUUID();  //产生一个援权码(即token)
            //记录缓存
            CacheHelper.getApiUserCacheBean().put(token, playuser, BMDataContext.SYSTEM_ORGI);   //FIXME 为何是存AUTH
            entity = new ResponseEntity<>(token, HttpStatus.OK);
            response.addCookie(new Cookie("authorization", token));    //增加援权cookie
        } else {
            entity = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);  //未援权
        }
        return entity;  //只返回援权码 token 或 提示未援权
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity error(HttpServletRequest request) {
        return new ResponseEntity<>(super.getUser(request), HttpStatus.OK);
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity logout(HttpServletRequest request, @RequestHeader(value = "authorization") String authorization) {
        return new ResponseEntity<>(HttpStatus.OK);
    }

}