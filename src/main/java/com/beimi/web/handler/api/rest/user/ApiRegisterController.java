package com.beimi.web.handler.api.rest.user;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.beimi.util.Base62;
import com.beimi.util.MessageEnum;
import com.beimi.util.RandomKey;
import com.beimi.util.UKTools;
import com.beimi.web.handler.Handler;
import com.beimi.web.model.PlayUser;
import com.beimi.web.model.ResultData;
import com.beimi.web.service.repository.es.PlayUserESRepository;
import com.beimi.web.service.repository.jpa.PlayUserRepository;

@RestController
@RequestMapping("/api/register")
public class ApiRegisterController extends Handler {

    @Autowired
    private PlayUserESRepository playUserESRes;

    @Autowired
    private PlayUserRepository playUserRes;

    @RequestMapping
    public ResponseEntity<ResultData> register(HttpServletRequest request, @Valid PlayUser player) {
        player = register(player);
        return new ResponseEntity<>(new ResultData(player != null,
                player != null ? MessageEnum.USER_REGISTER_SUCCESS : MessageEnum.USER_REGISTER_FAILD_USERNAME, player),
                HttpStatus.OK);
    }

    /**
     * 注册用户.
     *
     * @param player 玩家 可能是空的
     * @return 处理过的玩家对象
     */
    public PlayUser register(PlayUser player) { //FIXME: 2017-11-15 这个看得有点怪怪的

        if (player != null && !StringUtils.isBlank(player.getMobile()) && !StringUtils.isBlank(player.getPassword())) {
            //用户名为空时：产生一个用户名
            if (StringUtils.isBlank(player.getUsername())) {
                player.setUsername("Guest_" + Base62.encode(UKTools.getUUID().toLowerCase()));
            }

            //密码为空时：产生一个密码 // FIXME: 2017-11-15 为何如此？ 难道是为了更新旧有数据吗？
            if (StringUtils.isBlank(player.getPassword())) {
                player.setPassword(UKTools.encrypt(RandomKey.genRandomNumString(6)));     //随机生成一个6位数的密码 ，备用
            } else {
                player.setPassword(UKTools.encrypt(player.getPassword()));
            }

            player.setCreatetime(new Date());
            player.setUpdatetime(new Date());
            player.setLastlogintime(new Date());

            //判断用户名是否存在并保存  // FIXME: 2017-11-15 不知道有没有事务，否则这样会存在并发问题
            int users = playUserESRes.countByUsername(player.getUsername());  //FIXME 量大时速度或许有问题 ，EXIST会否更好
            if (users == 0) {
                playUserESRes.save(player);
            } else {
                player = null;
            }
        }
        return player;
    }

}