package com.beimi.util;

import com.beimi.config.web.model.Game;
import com.beimi.core.BMDataContext;
import com.beimi.core.engine.game.BeiMiGame;
import com.beimi.core.engine.game.model.Playway;
import com.beimi.core.engine.game.model.Type;
import com.beimi.util.cache.CacheHelper;
import com.beimi.util.rules.model.Board;
import com.beimi.util.rules.model.Player;
import com.beimi.web.model.*;
import com.beimi.web.service.repository.jpa.GameConfigRepository;
import com.beimi.web.service.repository.jpa.GamePlaywayRepository;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Sort;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * 游戏工具类.
 */
public final class GameUtils {
    /**
     * 随机数静态类，供其它地方直接调用.
     */
    public static final Random randomInst = new Random();


    /**
     * 随机男女.
     *
     * @return 返回随机男女性别字串 “0”：男，“1”：女
     */
    public static String randomSex() {
        return Integer.toString(randomInst.nextInt(1));
    }

    /**
     * 移除GameRoom 从队列里.
     * FIXME 为何这里是根据orgi获取的，还需要比对房间ID，为何不直接根据房间ID进行获取房间
     *
     * @param roomId 房间ID
     * @param orgi   运营ID
     */
    public static void removeGameRoomQuene(final String roomId, final String orgi) {
        GameRoom tempGameRoom;
        while ((tempGameRoom = (GameRoom) CacheHelper.getQueneCache().poll(orgi)) != null) {
            if (tempGameRoom.getId().equals(roomId)) {
                break;        //拿走，不排队了，开始增加AI
            } else {
                CacheHelper.getQueneCache().offer(tempGameRoom, orgi);    //还回去
            }
        }
    }


    /**
     * 开始斗地主游戏.
     *
     * @param playUserList 玩家list
     * @param gameRoom     游戏房间
     * @param cardsnum     牌数
     * @return Board 牌桌
     */
    public static Board playDiZhuGame(List<PlayUserClient> playUserList, GameRoom gameRoom, int cardsnum) {
        Board board = new Board();
        board.setCards(null);
        //打乱牌
        List<Byte> temp = new ArrayList<>();
        for (int i = 0; i < 54; i++) {
            temp.add((byte) i);
        }
        Collections.shuffle(temp);   //随机打乱牌
        byte[] cards = new byte[54];
        for (int i = 0; i < temp.size(); i++) {
            cards[i] = temp.get(i);
        }
        board.setCards(cards);        //设置所有的牌，顺序为随机打乱后的顺序

        board.setRatio(DiZhuConst.DEFAULT_RATIO);    //默认倍率 15
        board.setCatchtimes((byte) 0); //初始抢地主次数
        int random = playUserList.size() * gameRoom.getCardsnum();

        //按照人数计算在随机界牌 的位置，避免出现在底牌里
        //

        board.setPosition((byte) randomInst.nextInt(random));
        Player[] players = new Player[playUserList.size()];

        int inx = 0;
        for (PlayUserClient playUser : playUserList) {
            Player player = new Player(playUser.getId());
            player.setUsername(playUser.getUsername());
            player.setCards(new byte[cardsnum]);
            player.setGoldcoins(playUser.getGoldcoins());
            player.setSex(playUser.getSex());
            players[inx++] = player;
        }
        for (int i = 0; i < gameRoom.getCardsnum() * gameRoom.getPlayers(); i++) {
            int pos = i % players.length;
            players[pos].getCards()[i / players.length] = cards[i];
            if (i == board.getPosition()) {
                players[pos].setRandomcard(true);        //起到地主牌的人 获得随机幸运卡   FIXME 这段没细看
            }
        }
        //玩家手牌进行排序
        for (Player tempPlayer : players) {
            Arrays.sort(tempPlayer.getCards());  //1先正序
            tempPlayer.setCards(reverseCards(tempPlayer.getCards()));  //2、正序反转 变成为大到小
        }
        board.setRoom(gameRoom.getId());

        board.setPlayers(players);
        return board;
    }

    /**
     * 创建一个AI玩家.
     *
     * @return 创建用户
     */
    public static PlayUserClient createAiUser() {
        return createUser(new PlayUser(), null, null, BMDataContext.PlayerTypeEnum.AI.toString());
    }

    /**
     * 创建一个普通玩家.
     *
     * @param player  玩家
     * @param ipData  IP地址信息
     * @param request httpServletRequest
     * @return 返回获得的用户
     * @throws InvocationTargetException
     * @throws IllegalAccessException
     */
    public static PlayUserClient createUser(final PlayUser player, final IP ipData, final HttpServletRequest request) throws IllegalAccessException, InvocationTargetException {
        return createUser(player, ipData, request, BMDataContext.PlayerTypeEnum.NORMAL.toString());
    }

    /**
     * 反转卡牌排列顺序.
     *
     * @param cards 卡牌
     * @return 得到返转后的牌数组
     */
    public static byte[] reverseCards(final byte[] cards) {
        byte[] targetCards = new byte[cards.length];
        for (int i = 0; i < cards.length; i++) {
            // 反转后数组的第一个元素等于源数组的最后一个元素：
            targetCards[i] = cards[cards.length - i - 1];
        }
        return targetCards;
    }

    /**
     * 倒序排列 ，返回新数据数组.
     *
     * @param cards 倒序排序
     * @return 返回倒序后的字节数组
     */
    public static byte[] sortDescCard(final byte[] cards) {
        //FIXME 这个还是需要优化，循环太多次了
        byte[] targetCards = new byte[cards.length];
        System.arraycopy(cards, 0, targetCards, 0, cards.length);
        Arrays.sort(targetCards);  //正序
        return reverseCards(targetCards); //倒序
    }

    /**
     * 注册用户.
     * FIXME com.beimi.web.handler.api.rest.user.GuestRegisterController#register(com.beimi.web.model.PlayUser, com.beimi.util.IP, javax.servlet.http.HttpServletRequest)
     * fixme 这个上面有也，不知是否相同
     *
     * @param player     玩家对象
     * @param ipdata     ip地址对象
     * @param request    http请求
     * @param playerType 玩法家型
     * @return 返回一个用户
     */
    public static PlayUserClient createUser(final PlayUser player, final IP ipdata, final HttpServletRequest request,
                                            final String playerType) {

        PlayUserClient playUserClient = null;
        if (player != null) {  //FIXME 这个怎么可能为空呢，调用他的时候就NEW了一个
            if (StringUtils.isBlank(player.getUsername())) {
                player.setUsername("Guest_" + Base62.encode(UKTools.getUUID().toLowerCase()));
            }
            if (!StringUtils.isBlank(player.getPassword())) {
                player.setPassword(UKTools.encrypt(player.getPassword()));
            } else {
                player.setPassword(UKTools.encrypt(RandomKey.genRandomNumString(6))); //随机生成一个6位数的密码 ，备用
            }
            player.setPlayertype(playerType);    //玩家类型
            player.setCreatetime(new Date());
            player.setUpdatetime(new Date());
            player.setLastlogintime(new Date());

            BrowserClient browserClient = UKTools.parseClient(request);
            player.setOstype(browserClient.getOs());
            player.setBrowser(browserClient.getBrowser());
            if (request != null) {
                String userAgent = request.getHeader("User-Agent");  //FIXME 学习:搞懂记录这个有什么用
                if (!StringUtils.isBlank(userAgent)) {
                    if (userAgent.length() > 255) {
                        player.setUseragent(userAgent.substring(0, 250));
                    } else {
                        player.setUseragent(userAgent);
                    }
                }
            }
            if (ipdata != null) {     //FIXME 学习:搞懂IPDATA怎么来的
                player.setRegion(ipdata.getRegion());
                player.setCountry(ipdata.getCountry());
                player.setProvince(ipdata.getProvince());
                player.setCity(ipdata.getCity());
                player.setIsp(ipdata.getIsp());
            }


            player.setOrgi(BMDataContext.SYSTEM_ORGI);

            AccountConfig config = CacheConfigTools.getGameAccountConfig(BMDataContext.SYSTEM_ORGI);
            if (config != null) {  //这个根本不会==NULL
                player.setGoldcoins(config.getInitcoins());      //初始金币
                player.setCards(config.getInitcards());          //初始房卡数
                player.setDiamonds(config.getInitdiamonds());    //初始钻石
            }

            //FIXME 这个方法提取出去共用  可以考虑放到config里
            player.setSex(GameUtils.randomSex());        //随机男女

            if (!StringUtils.isBlank(player.getId())) {
                playUserClient = new PlayUserClient();
                try {
                    BeanUtils.copyProperties(playUserClient, player);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return playUserClient;
    }

    /**
     * 获取游戏全局配置，后台管理界面上的配置功能.
     *
     * @param orgi 运营ID
     * @return 游戏配置
     */
    public static GameConfig gameConfig(final String orgi) {
        GameConfig gameConfig = (GameConfig) CacheHelper.getSystemCacheBean().getCacheObject(BMDataContext.ConfigNames.GAMECONFIG.toString() + "." + orgi, orgi);
        if (gameConfig == null) {
            List<GameConfig> gameConfigList = BMDataContext.getContext().getBean(GameConfigRepository.class).findByOrgi(orgi);
            if (gameConfigList.size() > 0) {
                gameConfig = gameConfigList.get(0);
            } else {
                gameConfig = new GameConfig();
            }
        }
        return gameConfig;
    }

    /**
     * 获取游戏全局配置，后台管理界面上的配置功能.
     *
     * @param orgi     运营ID
     * @param gameType 游戏类型ID
     * @return 获得游戏玩法列表
     */
    public static List<GamePlayway> playwayConfig(final String gameType, final String orgi) {
        List<GamePlayway> gamePlayList = (List<GamePlayway>) CacheHelper.getSystemCacheBean().getCacheObject(gameType + "." +
                BMDataContext.ConfigNames.PLAYWAYCONFIG.toString(), orgi);
        if (gamePlayList == null) {
            gamePlayList = BMDataContext.getContext().getBean(GamePlaywayRepository.class).findByOrgiAndTypeid(orgi, gameType, new Sort(Sort.Direction.ASC, "sortindex"));
            CacheHelper.getSystemCacheBean().put(gameType + "." + BMDataContext.ConfigNames.PLAYWAYCONFIG.toString(), gamePlayList, orgi);
        }
        return gamePlayList;
    }

    /**
     * 清除游戏类型缓存.
     *
     * @param gameType 游戏类型ID
     * @param orgi     运营ID
     */
    public static void cleanPlayWayCache(final String gameType, final String orgi) {
        CacheHelper.getSystemCacheBean().delete(gameType + "." + BMDataContext.ConfigNames.PLAYWAYCONFIG.toString(), orgi);
    }

    /**
     * 封装Game信息，基于缓存操作.
     *
     * @param gameType 游戏类型ID
     * @return 获得GAME列表
     */
    public static List<BeiMiGame> games(final String gameType) {
        List<BeiMiGame> beiMiGameList = new ArrayList<>();
        if (!StringUtils.isBlank(gameType)) {
            /**
             * 找到游戏配置的 模式 和玩法，如果多选，则默认进入的是 大厅模式，如果是单选，则进入的是选场模式
             */
            String[] games = gameType.split(",");
            for (String game : games) {
                BeiMiGame beiMiGame = new BeiMiGame();
                for (SysDic sysDic : BeiMiDic.getInstance().getDic(BMDataContext.BEIMI_SYSTEM_GAME_TYPE_DIC)) {
                    if (sysDic.getId().equals(game)) {
                        beiMiGame.setName(sysDic.getName());
                        beiMiGame.setId(sysDic.getId());
                        beiMiGame.setCode(sysDic.getCode());

                        List<SysDic> gameModelList = BeiMiDic.getInstance().getDic(BMDataContext.BEIMI_SYSTEM_GAME_TYPE_DIC, game);
                        for (SysDic gameModel : gameModelList) {
                            Type type = new Type(gameModel.getId(), gameModel.getName(), gameModel.getCode());
                            beiMiGame.getTypes().add(type);
                            List<GamePlayway> gamePlayWayList = playwayConfig(gameModel.getId(), gameModel.getOrgi());
                            for (GamePlayway gamePlayway : gamePlayWayList) {
                                Playway playWay = new Playway(gamePlayway.getId(), gamePlayway.getName(), gamePlayway.getCode(), gamePlayway.getScore(), gamePlayway.getMincoins(), gamePlayway.getMaxcoins(), gamePlayway.isChangecard(), gamePlayway.isShuffle());
                                playWay.setLevel(gamePlayway.getTypelevel());
                                playWay.setSkin(gamePlayway.getTypecolor());
                                type.getPlayways().add(playWay);
                            }
                        }
                        beiMiGameList.add(beiMiGame);
                    }
                }
            }
        }
        return beiMiGameList;
    }


    /**
     * 工具类构造器隐藏.
     */
    private GameUtils() {
    }

    public static Game getGame(String playway, String orgi) {
        Game game = null;
        GamePlayway gamePlayway = (GamePlayway) CacheHelper.getSystemCacheBean().getCacheObject(playway, orgi);
        if (gamePlayway != null) {
            SysDic dic = (SysDic) CacheHelper.getSystemCacheBean().getCacheObject(gamePlayway.getGame(), gamePlayway.getOrgi());
            if (dic.getCtype().equals("dizhu")) {
                game = (Game) BMDataContext.getContext().getBean("dizhuGame");
            } else if (dic.getCtype().equals("majiang")) {
                game = (Game) BMDataContext.getContext().getBean("majiangGame");
            }
        }
        return game;
    }
}
