package com.beimi.util;

import java.util.List;

import com.beimi.core.BMDataContext;
import com.beimi.util.cache.CacheHelper;
import com.beimi.web.model.AccountConfig;
import com.beimi.web.service.repository.jpa.AccountConfigRepository;

/**
 * 用于获取缓存配置.
 *
 * @author iceworld
 */
public final class CacheConfigTools {
    /**
     * 隐藏工具类构造器.
     */
    private CacheConfigTools() {
    }

    /**
     * 获得游戏账号配置.
     *
     * @param orgi 运营ID
     * @return 返回帐号配置对象
     */
    public static AccountConfig getGameAccountConfig(final String orgi) {
        //缓存中存在就用缓存的
        AccountConfig config = (AccountConfig) CacheHelper.getSystemCacheBean().getCacheObject(BMDataContext
                .getGameAccountConfig(orgi), orgi);

        //缓存为空，读数据库的  数据库中没有就新建一个 FIXME 新建的算什么呢
        if (config == null) {
            AccountConfigRepository accountConfigRes = BMDataContext.getContext().getBean(AccountConfigRepository.class);
            List<AccountConfig> accountConfigList = accountConfigRes.findByOrgi(orgi);
            if (accountConfigList != null && accountConfigList.size() > 0) {
                config = accountConfigList.get(0);
            } else {
                config = new AccountConfig();
            }
            CacheHelper.getSystemCacheBean().put(BMDataContext.getGameAccountConfig(orgi), config, orgi);
        }
        return config;
    }
}
