package com.beimi.util;

import java.util.Random;

/**
 * 随机KEY产生工具类.
 */
public final class RandomKey {
    /**
     * 工具类隐藏构造器.
     */
    private RandomKey() {
    }

    /**
     * 生成N位随机数字字符串 .
     *
     * @param length 要生成的字串长度
     * @return 生成的字串
     */
    public static String genRandomNumString(final int length) {
        int iRandNum;               //单个生成的随机数
        int count = 0;              //生成的字串计数
        char[] str = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

        final int maxNum = str.length - 1;
        StringBuilder stringBuilder = new StringBuilder("");

        Random r = new Random();
        while (count < length) {
            // 生成随机数，取绝对值，防止生成负数
            iRandNum = Math.abs(r.nextInt(maxNum));             // 生成的数最大为36-1

            if (iRandNum >= 0 && iRandNum < str.length) {
                stringBuilder.append(str[iRandNum]);
                count++;
            }
        }

        return stringBuilder.toString();
    }
}
