package com.beimi.util.client;

import java.util.HashMap;
import java.util.Map;

import com.beimi.util.server.handler.BeiMiClient;

public class NettySystemClient implements NettyClient{
	
	private Map<String, BeiMiClient> systemClientsMap = new HashMap<String,BeiMiClient>();
	
	public BeiMiClient getClient(String key){
		return  systemClientsMap.get(key);
	}

//	public void putClient(String id , BeiMiClient client){
//		systemClientsMap.put(id, client) ;
//	}
	public void putClient(String userid , BeiMiClient client){
		//FIXME 这里session不知何意
		systemClientsMap.put(userid, client) ;
		systemClientsMap.put(client.getSession(), client) ;  //某些地方用session做为USERID
	}

//	public void removeClient(String id){
//		systemClientsMap.remove(id) ;
//	}
	public void removeClient(String id){
		//FIXME 这里移除不知为何如此麻烦
		BeiMiClient beiMiClient = getClient(id) ;
		systemClientsMap.remove(id) ;
		if(beiMiClient!=null){
			systemClientsMap.remove(beiMiClient.getUserid()) ;
		}
	}

	public void joinRoom(String userid, String roomid) {
		BeiMiClient beiMiClient = getClient(userid) ;
		if(beiMiClient!=null){
			beiMiClient.getClient().joinRoom(roomid);
		}
	}

	public void sendGameEventMessage(String userid, String event, Object data) {
		BeiMiClient beiMiClient = getClient(userid) ;
		if(beiMiClient!=null){
			beiMiClient.getClient().sendEvent(event, data);
		}
	}
}
