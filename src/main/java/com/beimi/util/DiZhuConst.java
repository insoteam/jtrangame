package com.beimi.util;

/**
 * 斗地主常用常量值.
 *
 * @author huangzh 黄志海 seenet2004@163.com
 */
public final class DiZhuConst {

    /**
     * 隐藏构造器
     */
    private DiZhuConst() {
    }


    /**
     * 系统默认游戏起始倍率.
     */
    public static final int DEFAULT_RATIO = 15;
}
