
package com.beimi.util.server.handler;

import com.beimi.core.engine.game.DiZhuCommand;
import com.beimi.core.engine.game.GameStatus;
import com.beimi.core.engine.game.impl.UserBoard;
import com.beimi.util.DiZhuConst;
import com.beimi.web.model.GamePlayway;
import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.OnEvent;

/**
 * 响应客户端请求的控制类
 * 地主游戏入口
 */
import com.alibaba.fastjson.JSON;
import com.beimi.config.web.model.Game;
import com.beimi.core.BMDataContext;
import com.beimi.core.engine.game.ActionTaskUtils;
import com.beimi.core.engine.game.state.GameEvent;
import com.beimi.util.UKTools;
import com.beimi.util.cache.CacheHelper;
import com.beimi.util.client.NettyClients;
import com.beimi.util.rules.model.Board;
import com.beimi.web.model.PlayUserClient;
import com.beimi.web.model.Token;
import com.beimi.web.service.repository.es.PlayUserClientESRepository;
import com.beimi.web.service.repository.jpa.PlayUserClientRepository;
import com.corundumstudio.socketio.AckRequest;
import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class GameEventHandler {

    private Game game;

    @Autowired
    public GameEventHandler(SocketIOServer server, Game game) {
        this.game = game;
    }

    @OnConnect
    public void onConnect(SocketIOClient client) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClient(client.getSessionId().toString()) ;
        if(beiMiClient!=null && !StringUtils.isBlank(beiMiClient.getUserid())){
            if(CacheHelper.getRoomMappingCacheBean().getCacheObject(beiMiClient.getUserid(), beiMiClient.getOrgi()) != null){
                ActionTaskUtils.sendEvent("" , beiMiClient.getUserid(), null);
            }
        }
    }

    //添加@OnDisconnect事件，客户端断开连接时调用，刷新客户端信息
    @OnDisconnect
    public void onDisconnect(SocketIOClient client) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClient(client.getSessionId().toString());
        if (beiMiClient != null) {
            PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getUserid(), beiMiClient.getOrgi());
            /**
             * 更新当前玩家状态，在线|离线
             */
            playUser.setOnline(false);
            UKTools.published(playUser, BMDataContext.getContext().getBean(PlayUserClientESRepository.class),
                    BMDataContext.getContext().getBean(PlayUserClientRepository.class));
            BMDataContext.getGameEngine().leaveRoom(playUser, beiMiClient.getOrgi());

            NettyClients.getInstance().removeClient(client.getSessionId().toString());
        }
    }


    /**
     * 加入房间开始游戏事件.
     * Token不为空，并且，验证Token有效，验证完毕即开始进行游戏撮合，房卡类型的
     * 1、大厅房间处理
     * a、从房间队列里获取最近一条房间信息
     * b、将token对应玩家加入到房间
     * c、如果房间凑齐了玩家，则将房间从等待撮合队列中移除，放置到游戏中的房间信息，如果未凑齐玩家，继续扔到队列
     * d、通知房间的所有人，有新玩家加入
     * e、超时处理，增加AI进入房间
     * f、事件驱动
     * g、定时器处理
     * 2、房卡房间处理
     * a、创建房间
     * b、加入到等待中队列
     *
     * @param sockClient 客户端连接对象
     * @param request    客户端请求
     * @param data       请求数据 客户端emit 时带的第二参数，即为这个data
     *                   //FIXME 请求中应该要有防黑技术，否则经不起攻击  没想好
     */
    @OnEvent(value = "joinroom")   //开始游戏
    public void onJoinRoom(SocketIOClient sockClient, AckRequest request, String data) {
        System.out.println("用户joinroom...");
        //较验数据
        BeiMiClient beiMiClient = JSON.parseObject(data, BeiMiClient.class);
        if (beiMiClient == null)
            return;

        String token = beiMiClient.getToken();  //TODO 搞懂 这个怎么来的
        if (StringUtils.isBlank(token))
            return;

        Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, beiMiClient.getOrgi());
        if (userToken == null)
            return;

        String userId = userToken.getUserid();
        //鉴权完毕  获得用户类
        PlayUserClient userClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userId, userToken.getOrgi());
        GamePlayway gamePlayway = (GamePlayway) CacheHelper.getSystemCacheBean().getCacheObject(beiMiClient.getPlayway(),
                beiMiClient.getOrgi());
//
//        if (userClient.getGoldcoins() < 0 ||
//                (gamePlayway != null && userClient.getGoldcoins() < gamePlayway.getScore()) ||
//                (gamePlayway != null && userClient.getGoldcoins() < gamePlayway.getMincoins())) {
//            sockClient.sendEvent( DiZhuCommand.GOLD_NOT_ENOUGH,UKTools.json(null));  //给自已发
//            return;
//        }

        beiMiClient.setClient(sockClient);
        beiMiClient.setSession(sockClient.getSessionId().toString());
        beiMiClient.setUserid(userClient.getId());
        NettyClients.getInstance().putClient(userClient.getId(), beiMiClient);  //FIXME 象是个客户端的缓存，或连接池
        //NettyClients.getInstance().putClient(beiMiClient.getSession(), beiMiClient);  //FIXME 上行不知何意,改成下行，统一用session来处理 这里估计最终还是不合适，有可能断线重连的问题

        /**
         * 更新当前玩家状态，在线|离线
         */
        userClient.setOnline(true);
        //更新数据库 采用Disruptor发布生产者
        UKTools.published(userClient,
                BMDataContext.getContext().getBean(PlayUserClientESRepository.class),
                BMDataContext.getContext().getBean(PlayUserClientRepository.class));

        BMDataContext.getGameEngine().joinRoomRequest(sockClient, userClient,beiMiClient);
    }

    @OnEvent(value = "test")   //测试native
    public void onTest(SocketIOClient sockClient, AckRequest request, String data) {
        System.out.println("server get test emit..................");
    }

    //抢地主
    @OnEvent(value = "docatch")
    public void onCatch(SocketIOClient client, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClient(client.getSessionId().toString());
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                BMDataContext.getGameEngine().catchRequest(roomid, playUser, playUser.getOrgi(), true);
            }
        }
    }

    //不抢地主
    @OnEvent(value = "giveup")
    public void onGiveup(SocketIOClient client, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClient(client.getSessionId().toString());
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                BMDataContext.getGameEngine().catchRequest(roomid, playUser, playUser.getOrgi(), false);
            }
        }
    }


    /**
     * 手动出牌
     * 对应客户端的：出牌按钮
     */
    @OnEvent(value = "doplaycards")
    public void onPlayCards(SocketIOClient client, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClient(client.getSessionId().toString());
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token) && !StringUtils.isBlank(data)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String[] cards = data.split(",");

                byte[] playCards = new byte[cards.length];
                for (int i = 0; i < cards.length; i++) {
                    playCards[i] = Byte.parseByte(cards[i]);
                }
                BMDataContext.getGameEngine().takeCardsRequest(roomid, userToken.getUserid(), userToken.getOrgi(), false, playCards);
            }
        }
    }

    /**
     * 手动不出牌
     * 对应客户端的：不出牌按钮
     */
    @OnEvent(value = "nocards")
    public void onNoCards(SocketIOClient client, String data) {
        BeiMiClient beiMiClient = NettyClients.getInstance().getClient(client.getSessionId().toString());
        String token = beiMiClient.getToken();
        if (!StringUtils.isBlank(token)) {
            Token userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(token, BMDataContext.SYSTEM_ORGI);
            if (userToken != null) {
                PlayUserClient playUser = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(playUser.getId(), playUser.getOrgi());
                BMDataContext.getGameEngine().takeCardsRequest(roomid, userToken.getUserid(), userToken.getOrgi(), false, null);
            }
        }
    }

    /**
     * 客户端请求游戏状态.
     * 说明：客户端在连接之后就立即请求游戏状态
     *
     */
    @OnEvent(value = "gamestatus")
    public void onGameStatus(SocketIOClient client , String data)
    {
        BeiMiClient beiMiClient = JSON.parseObject(data , BeiMiClient.class) ;
        Token userToken ;
        GameStatus gameStatus = new GameStatus() ;
        gameStatus.setGamestatus(BMDataContext.GameStatusEnum.NOTREADY.toString());
        if(beiMiClient!=null && !StringUtils.isBlank(beiMiClient.getToken()) && (userToken = (Token) CacheHelper.getApiUserCacheBean().getCacheObject(beiMiClient.getToken(), beiMiClient.getOrgi()))!=null){
            //鉴权完毕
            PlayUserClient userClient = (PlayUserClient) CacheHelper.getApiUserCacheBean().getCacheObject(userToken.getUserid(), userToken.getOrgi()) ;
            if(userClient!=null){
                gameStatus.setGamestatus(BMDataContext.GameStatusEnum.READY.toString());
                String roomid = (String) CacheHelper.getRoomMappingCacheBean().getCacheObject(userClient.getId(), userClient.getOrgi()) ;
                if(!StringUtils.isBlank(roomid) && CacheHelper.getBoardCacheBean().getCacheObject(roomid, userClient.getId())!=null){
                    gameStatus.setUserid(userClient.getId());
                    gameStatus.setGamestatus(BMDataContext.GameStatusEnum.PLAYING.toString());
                }
            }
        }
        client.sendEvent(DiZhuCommand.GAME_STATUS, UKTools.json(gameStatus));
    }
}  