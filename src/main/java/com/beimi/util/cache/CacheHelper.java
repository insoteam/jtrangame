package com.beimi.util.cache;

import com.beimi.util.cache.hazelcast.impl.PlayerCache;
import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;
import org.cache2k.CacheEntry;
import org.cache2k.event.CacheEntryExpiredListener;
import org.cache2k.expiry.ExpiryPolicy;
import org.cache2k.expiry.ValueWithExpiryTime;

import com.beimi.core.engine.game.IGameTask;
import com.beimi.util.cache.hazelcast.HazlcastCacheHelper;
import com.beimi.util.cache.hazelcast.impl.MultiCache;
import com.beimi.util.cache.hazelcast.impl.QueneCache;

/**
 * 学习：
 * <p>
 * offer，add区别：
 * 一些队列有大小限制，因此如果想在一个满的队列中加入一个新项，多出的项就会被拒绝。
 * 这时新的 offer 方法就可以起作用了。它不是对调用 add() 方法抛出一个 unchecked 异常，而只是得到由 offer() 返回的 false。
 * <p>
 * poll，remove区别：
 * remove() 和 poll() 方法都是从队列中删除第一个元素。remove() 的行为与 Collection 接口的版本相似，
 * 但是新的 poll() 方法在用空集合调用时不是抛出异常，只是返回 null。因此新的方法更适合容易出现异常条件的情况。
 * <p>
 * peek，element区别：
 * element() 和 peek() 用于在队列的头部查询元素。与 remove() 方法类似，在队列为空时， element() 抛出一个异常，而 peek() 返回
 */
public class CacheHelper {
    private static CacheHelper instance = new CacheHelper();
    private final Cache<String, ValueWithExpiryTime> expireCache;

    public CacheHelper() {
        expireCache = new Cache2kBuilder<String, ValueWithExpiryTime>() {
        }
                .sharpExpiry(true)
                .eternal(false)
                .expiryPolicy(new ExpiryPolicy<String, ValueWithExpiryTime>() {
                    @Override
                    public long calculateExpiryTime(String key, ValueWithExpiryTime value,
                                                    long loadTime, CacheEntry<String, ValueWithExpiryTime> oldEntry) {
                        return value.getCacheExpiryTime();
                    }
                })
                .addListener(new CacheEntryExpiredListener<String, ValueWithExpiryTime>() {
                    @Override
                    public void onEntryExpired(Cache<String, ValueWithExpiryTime> cache,
                                               CacheEntry<String, ValueWithExpiryTime> task) {
                        /**
                         *
                         */
                        ((IGameTask) task.getValue()).execute();
                    }
                })
                .build();
    }

    /**
     * 获取缓存实例
     */
    public static CacheHelper getInstance() {
        return instance;
    }

    private static CacheInstance cacheInstance = new HazlcastCacheHelper();

    public static CacheBean getRoomMappingCacheBean() {
        //获得在线用户
        return cacheInstance.getOnlineCacheBean(); //FIXME 这里这么写有没有多余呢  cacheInstance 怎么会==null?
    }

    public static CacheBean getSystemCacheBean() {
        return cacheInstance.getSystemCacheBean();
    }

    public static CacheBean getGameRoomCacheBean() {
        return cacheInstance.getGameRoomCacheBean();
    }

    public static MultiCache getGamePlayerCacheBeanx() {
        return cacheInstance.getGamePlayerCacheBean();
    }

    public static PlayerCache getGamePlayerCacheBean2() {
        return cacheInstance.getGamePlayerCacheBean2();
    }

    public static CacheBean getApiUserCacheBean() {
        return cacheInstance.getApiUserCacheBean();
    }

    /**
     * 存放游戏数据的 ，Board
     *
     * @return
     */
    public static CacheBean getBoardCacheBean() {
        return cacheInstance.getGameCacheBean();
    }

    public static QueneCache getQueneCache() {
        return cacheInstance.getQueneCache();
    }

    public static Cache<String, ValueWithExpiryTime> getExpireCache() {
        return instance.expireCache;
    }
}
