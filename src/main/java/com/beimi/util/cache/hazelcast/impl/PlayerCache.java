package com.beimi.util.cache.hazelcast.impl;

import com.beimi.util.cache.CacheBean;
import com.beimi.web.model.PlayUserClient;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.locks.Lock;

@Service("player_cache")
public class PlayerCache {
    @Autowired
    public HazelcastInstance hazelcastInstance;

    private String cacheName;

    public HazelcastInstance getInstance() {
        return hazelcastInstance;
    }

    public PlayerCache getCacheInstance(String cacheName) {
        this.cacheName = cacheName;
        return this;
    }

    public void put(String key, List<PlayUserClient> value) {
        getInstance().getMap(getName()).put(key, value);
    }

    public void put(String key, PlayUserClient value) {
        List<PlayUserClient> playUserList = this.getCacheObject(key);
        boolean isFind = false;
        for (int i = 0; i < playUserList.size(); i++) {
            if (playUserList.get(i).getId().equals(value.getId())) {
                isFind = true;
                playUserList.set(i, value);
                break;
            }
        }
        if (isFind == false) {
            playUserList.add(value);
        }
        getInstance().getMap(getName()).put(key, playUserList);

//        for (PlayUserClient playUserClient : playUserList) {
//            if (playUserClient.getId().equals(value.getId())){
//                playUserClient = value;
//                break;
//            }
//        }

    }

    public String getName() {
        return cacheName;
    }

    public void clear() {
        getInstance().getMap(getName()).clear();
    }

    public List<PlayUserClient> delete(String key) {
        return (List<PlayUserClient>) getInstance().getMap(getName()).remove(key);
    }

    public void delete(String key, PlayUserClient playUser) {
        List<PlayUserClient> playUserList = this.getCacheObject(key);
        boolean isFind = false;
        int idx = -1;
        for (int i = 0; i < playUserList.size(); i++) {
            if (playUserList.get(i).getId().equals(playUser.getId())) {
                isFind = true;
                idx = i;
                playUserList.set(i, playUser);
                break;
            }
        }

        if (isFind) {
            playUserList.remove(idx);
        }
    }

    public void update(String key, List<PlayUserClient> value) {
        getInstance().getMap(getName()).put(key, value);
    }

    public List<PlayUserClient> getCacheObject(String key) {
        List<PlayUserClient> playList = (List<PlayUserClient>) getInstance().getMap(getName()).get(key);
        if (playList==null)
            playList = new ArrayList<PlayUserClient>();
        return playList;
    }

    public Collection<?> getAllCacheObject() {
        return getInstance().getMap(getName()).keySet();
    }

    public Object getCache() {
        return getInstance().getMap(cacheName);
    }

    public Lock getLock(String lock, String orgi) {
        return getInstance().getLock(lock);
    }

    public long getSize() {
        return getInstance().getMap(getName()).size();
    }

    public long getAtomicLong(String cacheName) {
        return getInstance().getAtomicLong(getName()).incrementAndGet();
    }

    public void setAtomicLong(String cacheName, long start) {
        getInstance().getAtomicLong(getName()).set(start);
    }


}
