package com.beimi.util.rules.model;

import com.beimi.core.engine.game.GameBoard;
import com.beimi.core.engine.game.impl.UserBoard;

import java.util.ArrayList;
import java.util.List;

public class RecoveryData implements java.io.Serializable{
	private String command ;
	private String userid ;
	private Player player ;
	private byte[] lasthands;
	private TakeCards last ;
	private String banker ;
	private String nextplayer ;//正在出牌的玩家
	private CardsInfo[] cardsnum ;
	private int time ;		//计时器剩余时间
	private boolean automic ;	//本轮第一个出牌，不允许出现不出按钮
	private GameBoard data ;
	private int ratio ;
	private byte[] hiscards ;
	
	private SelectColor selectcolor ;
	
	private UserBoard userboard ;
	
	
	public RecoveryData(Player player , byte[] lasthands , String nextplayer , int time , boolean automic , Board board){
		this.player = player ;
		this.userid = player.getPlayuser() ;
		this.lasthands = lasthands ;
		this.nextplayer = nextplayer ;
		this.banker = board.getBanker();
		this.time = time ;
		this.automic = automic;
		//底牌数据  FIXME 改名
		this.data = new GameBoard(board.getBanker(), board.getLasthands(), board.getRatio(),board.getLastHandRatio());
		
		this.hiscards = player.getHistory() ;
		
		this.ratio = board.getRatio() ;
		//玩家数据
		this.userboard = new UserBoard(board, player.getPlayuser()) ;
		//玩家选择的花色 麻将用到，缺一门的
		this.selectcolor =  new SelectColor(board.getBanker(), player.getPlayuser()) ;
		this.selectcolor.setColor(player.getColor());
		
		this.last = board.getLast() ;
		//其它玩家手牌
		List<CardsInfo> tempList = new ArrayList<CardsInfo>();
		for(Player tempPlayer : board.getPlayers()){
			if(!tempPlayer.getPlayuser().equals(player.getPlayuser())){
				CardsInfo cardsInfo = new CardsInfo(tempPlayer.getPlayuser(), tempPlayer.getCards().length,
						tempPlayer.getHistory(),
						tempPlayer.getActions(),
						board, tempPlayer);
				tempList.add(cardsInfo) ;
			}
		}
		cardsnum = tempList.toArray(new CardsInfo[0]) ;
	}
	
	
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public Player getPlayer() {
		return player;
	}
	public void setPlayer(Player player) {
		this.player = player;
	}
	public byte[] getLasthands() {
		return lasthands;
	}
	public void setLasthands(byte[] lasthands) {
		this.lasthands = lasthands;
	}
	public String getNextplayer() {
		return nextplayer;
	}

	public void setNextplayer(String nextplayer) {
		this.nextplayer = nextplayer;
	}

	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public boolean isAutomic() {
		return automic;
	}
	public void setAutomic(boolean automic) {
		this.automic = automic;
	}


	public CardsInfo[] getCardsnum() {
		return cardsnum;
	}


	public void setCardsnum(CardsInfo[] cardsnum) {
		this.cardsnum = cardsnum;
	}

	public String getUserid() {
		return userid;
	}


	public void setUserid(String userid) {
		this.userid = userid;
	}


	public TakeCards getLast() {
		return last;
	}


	public void setLast(TakeCards last) {
		this.last = last;
	}


	public GameBoard getData() {
		return data;
	}


	public void setData(GameBoard data) {
		this.data = data;
	}


	public int getRatio() {
		return ratio;
	}


	public void setRatio(int ratio) {
		this.ratio = ratio;
	}


	public UserBoard getUserboard() {
		return userboard;
	}


	public void setUserboard(UserBoard userboard) {
		this.userboard = userboard;
	}


	public SelectColor getSelectcolor() {
		return selectcolor;
	}


	public void setSelectcolor(SelectColor selectcolor) {
		this.selectcolor = selectcolor;
	}


	public byte[] getHiscards() {
		return hiscards;
	}


	public void setHiscards(byte[] hiscards) {
		this.hiscards = hiscards;
	}

	public String getBanker() {
		return banker;
	}

	public void setBanker(String banker) {
		this.banker = banker;
	}
}
