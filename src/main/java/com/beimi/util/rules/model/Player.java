package com.beimi.util.rules.model;

import java.util.List;

/**
 * 传给客户端的玩家类.
 * 说明：
 * 不是全部的用户信息，只是根据游戏需要给相应的数据
 * 也可以根据需要增加一些虚拟字段，用于传数据
 *
 * @author huangzh 黄志海 seenet2004@163.com
 * @date 2017-11-15 9:30
 */
public class Player implements java.io.Serializable, Cloneable {
    private static final long serialVersionUID = 1L;
    private List<Action> actions;
    private int color;

    public Player(String id) {
        this.playuser = id;

    }

    private String playuser;        //userid对应
    private String username;        //玩家名
    private byte[] cards;           //玩家手牌，顺序存储 ， 快速排序（4个Bit描述一张牌，玩家手牌 麻将 13+1/2 = 7 byte~=long）
    private byte[] history;         //出牌历史 ， 特权可看
    private byte info;              //复合信息存储，用于存储玩家位置（2^4,占用4个Bit，最大支持16个玩家）（是否在线1个Bit），是否庄家/地主（1个Bit），是否当前出牌玩家（1个Bit）（是否机器人1个Bit）
    private boolean randomcard;     //发牌时最初被指定为地主
    private boolean docatch;        //抢过庄（地主）  //FIXME 好象没用 改成用board 的catchtimes 计次方式
    private boolean recatch;        //补抢            //FIXME 好象没用
    private boolean accept;         //抢地主 : 过地主     //FIXME 好象没用
    private byte[] played;          //杠碰吃胡
    private int goldcoins;          //玩家当前金币      //每次开房时先扣除房费，这里存的是总金币
    private int goldchangs;         //金币增减
    private boolean isWin;          //是否赢牌了  牌结束时赋值
    private String sex;             //性别 "0":男 \ "1":女


    public byte[] getCards() {
        return cards;
    }

    public void setCards(byte[] cards) {
        this.cards = cards;
    }

    public byte getInfo() {
        return info;
    }

    public void setInfo(byte info) {
        this.info = info;
    }

    public byte[] getPlayed() {
        return played;
    }

    public void setPlayed(byte[] played) {
        this.played = played;
    }

    public byte[] getHistory() {
        return history;
    }

    public void setHistory(byte[] history) {
        this.history = history;
    }

    public String getPlayuser() {
        return playuser;
    }

    public void setPlayuser(String playuser) {
        this.playuser = playuser;
    }

    public boolean isRandomcard() {
        return randomcard;
    }

    public void setRandomcard(boolean randomcard) {
        this.randomcard = randomcard;
    }

    public boolean isDocatch() {
        return docatch;
    }

    public void setDocatch(boolean docatch) {
        this.docatch = docatch;
    }

    public boolean isAccept() {
        return accept;
    }

    public void setAccept(boolean accept) {
        this.accept = accept;
    }

    @Override
    public Player clone() {
        try {
            return (Player) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean isRecatch() {
        return recatch;
    }

    public void setRecatch(boolean recatch) {
        this.recatch = recatch;
    }

    public int getGoldchangs() {
        return goldchangs;
    }

    public void setGoldchangs(int goldchangs) {
        this.goldchangs = goldchangs;
    }

    public boolean isWin() {
        return isWin;
    }

    public void setWin(boolean win) {
        isWin = win;
    }

    public void setGoldcoins(int goldcoins) {
        this.goldcoins = goldcoins;
    }

    public int getGoldcoins() {
        return goldcoins;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
