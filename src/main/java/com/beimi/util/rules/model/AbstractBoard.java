package com.beimi.util.rules.model;

public abstract class AbstractBoard implements java.io.Serializable {

    /**
     * 翻底牌，每一种游戏的实现方式不同
     *
     * @return
     */
    public abstract byte[] pollLastHands();

    /**
     * 计算倍率， 规则每种游戏不同，斗地主 翻到底牌 大小王 翻倍
     *
     * @return
     */
    public abstract int calcLastHandRatio();

    /**
     * 计算金币
     */
    public abstract void caleGold();


    /**
     * 找到玩家的 位置
     *
     * @param userid
     * @return
     */
    public abstract int index(String userid);

    /**
     * 是否游戏打完了
     *
     * @param playuser
     * @return
     */
    public abstract boolean isGameOver(String playuser);


    /**
     * 找到下一个玩家
     *
     * @param index
     * @return
     */
    protected abstract Player next(int index);


    public abstract Player nextPlayer(int index);

    /**
     * 玩家随机出牌，能管住当前出牌的 最小牌
     *
     * @param player
     * @return
     */
    public abstract TakeCards takecard(Player player, boolean isAuto, TakeCards last, byte[] playCards);

    private static final long serialVersionUID = 1L;

    private byte[] cards;    //4个Bit描述一张牌，麻将：136+2/2 = 69 byte ; 扑克 54/2 = 27 byte
    private Player[] players;//3~10人(4 byte)
    private TakeCards last;
    private boolean finished;
    private String nextplayer;
    private String room;            //房间ID（4 byte）
    private byte[] lasthands;       //底牌
    private byte position;          //地主牌  （起牌者，即第一个开始干活的人，int 指向第X个玩家）

    //抢地主从第一个系统默认起牌者开始，轮一圈后即可确认谁是最终地主，这里用于记录抢地主的次数
    private byte catchtimes;       //默认为1，当累加到4时，即表示抢地主结束
    private boolean docatch;        //叫地主 OR 抢地主
    private int ratio = 1;            //倍数
    private int lastHandRatio = 1;    //底牌倍数 默认为1
    private String banker;          //庄家|地主
    private String currplayer;      //当前出牌人
    private byte currcard;          //当前出牌
    private boolean isBankerWin;    //是否庄家赢

    /**
     * 找到玩家数据.
     *
     * @param userid
     * @return
     */
    public Player getPlayer(String userid) {
        if (this.players != null) {
            for (Player player : players) {
                if (player.getPlayuser() != null && player.getPlayuser().equals(userid)) {
                    return player;
                }
            }
        }
        return null;
    }

    public byte[] getCards() {
        return cards;
    }

    public void setCards(byte[] cards) {
        this.cards = cards;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getBanker() {
        return banker;
    }

    public void setBanker(String banker) {
        this.banker = banker;
    }

    public String getCurrplayer() {
        return currplayer;
    }

    public void setCurrplayer(String currplayer) {
        this.currplayer = currplayer;
    }

    public byte getCurrcard() {
        return currcard;
    }

    public void setCurrcard(byte currcard) {
        this.currcard = currcard;
    }

    public byte getPosition() {
        return position;
    }

    public void setPosition(byte position) {
        this.position = position;
    }

    public boolean isDocatch() {
        return docatch;
    }

    public void setDocatch(boolean docatch) {
        this.docatch = docatch;
    }

    public int getRatio() {
        return ratio;
    }

    public void setRatio(int ratio) {
        this.ratio = ratio;
    }

    public byte[] getLasthands() {
        return lasthands;
    }

    public void setLasthands(byte[] lasthands) {
        this.lasthands = lasthands;
    }

    public TakeCards getLast() {
        return last;
    }

    public void setLast(TakeCards last) {
        this.last = last;
    }

    public String getNextplayer() {
        return nextplayer;
    }

    public void setNextplayer(String nextplayer) {
        this.nextplayer = nextplayer;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public abstract int getPlayerIndex(Player randomCatchPlayer);

    public byte getCatchtimes() {
        return catchtimes;
    }

    public void setCatchtimes(byte catchtimes) {
        this.catchtimes = catchtimes;
    }

    public boolean isBankerWin() {
        return isBankerWin;
    }

    public void setBankerWin(boolean bankerWin) {
        isBankerWin = bankerWin;
    }

    public int getLastHandRatio() {
        return lastHandRatio;
    }

    public void setLastHandRatio(int lastHandRatio) {
        this.lastHandRatio = lastHandRatio;
    }
}
