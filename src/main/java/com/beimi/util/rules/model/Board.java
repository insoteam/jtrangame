package com.beimi.util.rules.model;

import com.beimi.util.GameUtils;
import org.apache.commons.lang.ArrayUtils;

/**
 * 牌局，用于描述当前牌局的内容 ，
 * 1、随机排序生成的 当前 待起牌（麻将、德州有/斗地主无）
 * 2、玩家 手牌
 * 3、玩家信息
 * 4、当前牌
 * 5、当前玩家
 * 6、房间/牌桌信息
 * 7、其他附加信息
 * 数据结构内存占用 78 byte ， 一副牌序列化到 数据库 占用的存储空间约为 78 byt， 数据库字段长度约为 20
 *
 * @author iceworld
 */
public class Board extends AbstractBoard implements java.io.Serializable {
    private static final long serialVersionUID = 6143646772231515350L;

    /**
     * 大王牌ID常量.
     */
    private static final int WAN_BIG = 53;

    /**
     * 小王牌ID常量.
     */
    private static final int WAN_SMILL = 52;

    /**
     * 翻底牌 ， 斗地主
     */
    @Override
    public byte[] pollLastHands() {
        return ArrayUtils.subarray(this.getCards(), this.getCards().length - 3, this.getCards().length);
    }

    /**
     * 获得发牌时初始指定的地主玩家
     *
     * @return
     */
    public Player getRandomPlay() {
        for (int i = 0; i < getPlayers().length; i++) {
            Player player = getPlayers()[i];
            if (player.isRandomcard()) {
                return player;
            }
        }
        return null;
    }

    /**
     * 计算底牌倍数.
     * 大王*2，小王*2，大小王*4，顺子*3，同花*3
     */
    @Override
    public int calcLastHandRatio() {
        int resultRatio = 1;
        byte[] tmpLastHands = GameUtils.sortDescCard(getLasthands());  //倒序排序，不影响原有的数据

        //大王*2，小王*2，大小王*4
        for (byte card : tmpLastHands) {
            switch (card) {
                case WAN_BIG:  //大王*2
                    resultRatio = resultRatio * 2;
                    break;
                case WAN_SMILL:  //小王*2
                    resultRatio = resultRatio * 2;
                    break;
                default:
                    break;
            }
        }

        //顺子*2
        boolean orderedCards = isOrderedCards(tmpLastHands);

        if (orderedCards) {
            resultRatio = resultRatio * 3;  //顺子2倍
        }
        //同花*3
        boolean sameColorCards = isSameColorCards(tmpLastHands);
        if (sameColorCards) {
            resultRatio = resultRatio * 3; //同花3倍
        }
        //同花并且顺子再*2
        if (orderedCards && sameColorCards) {
            resultRatio = resultRatio * 2;
        }
        return resultRatio;
    }

    /**
     * 是否顺子.
     *
     * @param tmpLastHands 三张底牌
     * @return
     */
    private boolean isOrderedCards(byte[] tmpLastHands) {
        //2的特别处理 防止2与A,K成顺子
        int one = tmpLastHands[0] / 4;
        int two = tmpLastHands[1] / 4;
        int three = tmpLastHands[1] / 4;
        if (one == 12)   //只需判断第一个是不是2就好了，因为前面已经排过序了
            one = -1;

        return one - 1 == two && two - 1 == three  //这样判断顺子，因为前面已经排过序了
                && !(tmpLastHands[0] == WAN_BIG || tmpLastHands[1] == WAN_BIG || tmpLastHands[2] == WAN_BIG)
                && !(tmpLastHands[0] == WAN_SMILL || tmpLastHands[1] == WAN_SMILL || tmpLastHands[2] == WAN_SMILL);
    }

    /**
     * 是否同花.
     *
     * @param tmpLastHands 三张底牌
     * @return
     */
    private boolean isSameColorCards(final byte[] tmpLastHands) {
        return (tmpLastHands[0] % 4 == tmpLastHands[1] % 4
                && tmpLastHands[1] % 4 == tmpLastHands[2] % 4
                && !(tmpLastHands[0] == WAN_BIG || tmpLastHands[1] == WAN_BIG || tmpLastHands[2] == WAN_BIG)
                && !(tmpLastHands[0] == WAN_SMILL || tmpLastHands[1] == WAN_SMILL || tmpLastHands[2] == WAN_SMILL)
        );
    }

    @Override
    public void caleGold() {
        for (Player player : getPlayers()) {
            /**
             * =X*N+A     X=游戏基数=游戏底分*游戏倍数=B*ratio N=房间系数
             * 说明：A=50 ,B=10,N=10;
             * 最低值 ：15*1*10+50 = 200
             */
            int B = 10, A = 50, N = 1;
            int X = B * getRatio();
            int tmpGold = X * N;

            if (player.getPlayuser().equals(getBanker())) {  //
                if (isBankerWin()) {
                    tmpGold = tmpGold * 2;
                    player.setWin(true);
                } else {
                    tmpGold = tmpGold * 2 * -1;
                    player.setWin(false);
                }
            } else {
                if (isBankerWin()) {
                    tmpGold = tmpGold * -1;
                    player.setWin(false);
                } else {
                    tmpGold = tmpGold * 1;
                    player.setWin(true);
                }
            }
            player.setGoldchangs(tmpGold);

        }
    }

    public TakeCards takecard(Player player, boolean isAuto, TakeCards lastTakeCards, byte[] currPlayCards) {
        return new TakeCards(player, isAuto, lastTakeCards, currPlayCards);
    }

    @Override
    public int getPlayerIndex(Player player) {
        return index(player.getPlayuser());
    }

    /**
     * 找到玩家的 位置
     *
     * @param userid
     * @return
     */
    public int index(String userid) {
        int index = 0;
        for (int i = 0; i < this.getPlayers().length; i++) {
            Player temp = this.getPlayers()[i];
            if (temp.getPlayuser().equals(userid)) {
                index = i;
                break;
            }
        }
        return index;
    }

    /**
     * 找到下一个玩家
     * FIXME 这个为何要跟地主扯在一起？
     *
     * @param index
     * @return
     */
    public Player next(int index) {
        Player catchPlayer = null;
        if (index == 0 && this.getPlayers()[index].isRandomcard()) {    //fixed
            index = this.getPlayers().length - 1;
        }
        for (int i = index; i >= 0; i--) {
            Player player = getPlayers()[i];
            if (player.isDocatch() == false) {
                catchPlayer = player;
                break;
            } else if (player.isRandomcard()) {    //重新遍历一遍，发现找到了地主牌的人，终止查找
                break;
            } else if (i == 0) {
                i = getPlayers().length;
            }
        }
        return catchPlayer;
    }

    public Player nextPlayer(int index) {
        //这个为何是倒着转的 0，1，2   : 0->2->1->0->2->1->0
        if (index == 0) {
            index = getPlayers().length - 1;
        } else {
            index = index - 1;
        }
        return getPlayers()[index];
    }

    @Override
    public boolean isGameOver(String playuser) {
        if (this.getLast() != null && this.getLast().getCardsnum() == 0) {//出完了
            //根据最后一个出牌者是否地主来决定是不是地主赢
            setBankerWin(playuser.equals(getBanker()));
            return true;
        }
        return false;
    }

}
