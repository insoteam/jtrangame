package com.beimi.util.rules.model;

import com.beimi.core.engine.game.ActionTaskUtils;
import com.beimi.core.engine.game.CardType;
import com.beimi.core.engine.game.SearchCardType;
import com.beimi.util.rules.model.card.Card;

import java.util.Map;

/**
 * 出牌信息
 * 出牌人
 * 牌
 *
 * @author zhangtianyi
 */
public class TakeCards implements java.io.Serializable {

    private static final long serialVersionUID = 8718778983090104033L;

    private String banker;          //庄家|地主的ID
    private boolean allow;          //符合出牌规则 ，
    private boolean donot;          //出 OR 不出
    private String userid;          //当前出牌者ID
    private String sex;             //当前出牌者性别
    private byte[] cards;           //当前出牌者出的牌
    private long time;              //FIXME 暂时没用，不知是否用于记录第几次出牌
    //出牌类型 ： 1:单张 | 2:对子 | 3:三张 | 4:四张（炸） | 5:单张连 | 6:连对 | 7:飞机 : 8:4带2 | 9:王炸
    private int type;               //FIXME 这个意义是乎不大，如果前端要语音播报的话只按这几种还不够
    private CardType cardType;      //出牌的牌型
    private boolean sameside;       //同一伙
    private int cardsnum;           //当前出牌的人 剩下多少张 牌
    private String nextplayer;      // 下一个出牌玩家


    private boolean isFirst;        //是否新一轮的第一个 默认为true  hzh 用于语音播报时决定如何播报  判断依据 不上家出牌，且上家


    public TakeCards() {
    }

    //改成只用一个构造器，三个参数传入，允许为空
    public TakeCards(Player player, boolean isAuto, TakeCards last, byte[] playCards) {
        takeCards(player, isAuto, last, playCards);
    }

    /**
     * FIXME 这个逻辑上应该有问题
     *
     * @param player    玩家类
     * @param isAuto    是否自动出牌（指超时，由系统出)
     * @param last      上次出牌（可能是上家，也可能是自已）
     * @param playCards 本次出牌（有可能为空，也有可能有指定出的牌）
     */
    private void takeCards(Player player, boolean isAuto, TakeCards last, byte[] playCards) {
        //调试输出
        String lstStr = "";
        if (last != null) {
            if (!last.getUserid().equals(player.getPlayuser())) {
                lstStr = Card.getInstance().cardsToString(last.getCards());
                System.out.println(String.format("tstsupper(new byte[]%s, new byte[]%s);",
                        Card.getInstance().cardBytesToString(player.getCards()),
                        Card.getInstance().cardBytesToString(last.getCards())));
            }
        }
        System.out.println("本次客户端:" + "isauto:" + isAuto + " 上家出牌【" + lstStr + "】 手动出牌：【" + Card.getInstance().cardsToString(playCards) + "】");


        //1、出牌算法
        this.userid = player.getPlayuser();   //设置当前出牌者ID
        this.sex = player.getSex();           //设置当前出牌者性别
        this.setAllow(true);

        if (isAuto) {   //playCards必等NULL
            if (last != null) {
                if (!last.getUserid().equals(player.getPlayuser())) {  //上次是别的玩家出的牌
                    this.cards = SearchCardType.getInstance().searchSupperCardsByCardType(player.getCards(), last.getCardType());
                    setFirst(false);   //设置是否一轮的第一个 决定语音播报方式
                } else {
                    this.cards = getAIMostSmall(player);
                    setFirst(true);
                }
            } else {
                this.cards = getAIMostSmall(player);
                setFirst(true);
            }
        } else {
            if (playCards != null) {   //玩家自已出牌

                if (last != null) {    //上家有出牌的情况下(且出牌方不是自已)
                    if (!last.getUserid().equals(player.getPlayuser())) {
                        if (ActionTaskUtils.allow(playCards, last.getCardType())) {
                            this.cards = playCards;
                        } else {
                            this.setAllow(false);//返回一个空的，并设置为不合规
                        }
                        setFirst(false);
                    } else {           //上家是自已
                        if (ActionTaskUtils.identification(playCards) != null) {
                            this.cards = playCards;
                        } else {
                            System.out.println("出牌不符合规则：" + Card.getInstance().cardsToString(playCards) + Card.getInstance().cardBytesToString(playCards));
                            this.setAllow(false);
                        }
                        setFirst(true);
                    }
                } else {               //上家无出牌的情况下
                    if (ActionTaskUtils.identification(playCards) != null) {
                        this.cards = playCards;
                    } else {
                        System.out.println("出牌不符合规则：" + Card.getInstance().cardsToString(playCards) + Card.getInstance().cardBytesToString(playCards));
                        this.setAllow(false);
                    }
                    setFirst(true);
                }

            } else {                   //玩家放弃出牌  正常如果是地主且第一次出牌，不该允许不出
                if (last != null) {      //有上家
                    if (!last.getUserid().equals(player.getPlayuser())) {  //上家有出牌且上家不是自已，允许不出
                        setFirst(false);
                    } else {            //上家有出牌且上家是自已 还是自已出，所以必须出牌
                        setFirst(true);
                    }
                } else {               //没有上家
                    setFirst(true);
                }

                this.setAllow(!isFirst());  //根据是否新一轮来决定是否允许出牌
            }
        }

        if (this.cards != null) {
            System.out.println("remove Cards: ");
            System.out.println("src:" + Card.getInstance().cardBytesToString(playCards) + "  " + Card.getInstance().cardsToString(playCards) + "");
            System.out.println("wile remove:" + Card.getInstance().cardBytesToString(this.cards) + "  " + Card.getInstance().cardsToString(this.cards) + "");
            player.setCards(this.removeCards(player.getCards(), this.cards));
            this.cardType = ActionTaskUtils.identification(this.cards);
            if (this.cardType != null) {
                this.type = cardType.getCardtype();
            } else {
                System.out.println("怪事:");
            }
        }
        this.cardsnum = player.getCards().length;  //剩余牌数
    }

    /**
     * * 找到机器人或托管的最小的牌
     * FIXME 这个逻辑应该是
     * 获得最小的牌，并打出最小牌的整组 比如：3，3对，3个3，4个3
     * 5、5、4、3、3、3、
     *
     * @param player
     * @return
     */
    public byte[] getAIMostSmall(Player player) {
        byte[] takeCards = null;
        byte[] cards = player.getCards();
        //将当前玩家剩下的牌进行分类
        Map<Integer, Integer> types = ActionTaskUtils.type(cards);

        //最小的类型
        Integer minCardType = cards[cards.length - 1] / 4;

        if (types.get(minCardType) != null) {
            takeCards = getCardTypeCards(cards, minCardType, types.get(minCardType));
        }
        return takeCards;
    }

    /**
     * 获得手牌上某个牌值的NUM张牌，
     *
     * @param cards
     * @param cardValue
     * @param num
     * @return
     */
    private byte[] getCardTypeCards(byte[] cards, Integer cardValue, Integer num) {
        byte[] resultCards = null;
        resultCards = new byte[num];
        int index = 0;
        for (byte card : cards) {
            if (card / 4 == cardValue) {
                resultCards[index++] = card;
            }
            if (index >= num) {
                break;
            }
        }
        return resultCards;
    }

    /**
     * 移除出牌
     *
     * @param cards
     * @param playcards
     * @return
     */
    public byte[] removeCards(byte[] cards, byte[] playcards) {
        byte[] retCards = new byte[cards.length - playcards.length];
        System.out.println("cards:" + Card.getInstance().cardBytesToString(cards));
        System.out.println("playcards:" + Card.getInstance().cardBytesToString(playcards));
        int cardsindex = 0;
        for (int i = 0; i < cards.length; i++) {
            if (!isFound(cards[i], playcards)) {  //没找到就把牌记下，做为下次的手牌
                retCards[cardsindex++] = cards[i];
            }
        }
        return retCards;
    }

    private boolean isFound(byte card, byte[] playcards) {
        boolean found = false;
        for (int inx = 0; inx < playcards.length; inx++) {
            if (card == playcards[inx]) {
                found = true;
                break;
            }
        }
        return found;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public byte[] getCards() {
        return cards;
    }

    public void setCards(byte[] cards) {
        this.cards = cards;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCardsnum() {
        return cardsnum;
    }

    public void setCardsnum(int cardsnum) {
        this.cardsnum = cardsnum;
    }

    public String getNextplayer() {
        return nextplayer;
    }

    public void setNextplayer(String nextplayer) {
        this.nextplayer = nextplayer;
    }

    public CardType getCardType() {
        return cardType;
    }

    public void setCardType(CardType cardType) {
        this.cardType = cardType;
    }

    public boolean isAllow() {
        return allow;
    }

    public void setAllow(boolean allow) {
        this.allow = allow;
    }

    public boolean isDonot() {
        return donot;
    }

    public void setDonot(boolean donot) {
        this.donot = donot;
    }

    public boolean isSameside() {
        return sameside;
    }

    public void setSameside(boolean sameside) {
        this.sameside = sameside;
    }

    public String getBanker() {
        return banker;
    }

    public void setBanker(String banker) {
        this.banker = banker;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
