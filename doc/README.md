#beimi
开源棋牌游戏，包含麻将、德州、斗地主
首个版本会采用当前最流行的房卡模式

贝密游戏是一系列棋牌游戏的名称，其中包含麻将、斗地主、德州，目前正在进行UI设计以及后台系统（JAVA）开发，7月中发布0.1.0版本，仅包含前端UI方案，预计8月份发布首个版本，敬请关注！


1. 开发工具：Cocos Creater
1. 开发语言：Java + JavaScript
1. 服务端框架：Spring Boot + MySQL + JPA + Netty+SocketIO+Disruptor(并发框架) +状态机+MessageBuilder
              hazelcast + Cache2k
              Spring Data ElasticSearch  分库分表和历史数据迁移\实时的全文检索引擎，能够快速的查询海量数据
              disruptor 消息队列技术
1. 客户端语言：Cocos Creater/JavaScript

说明：hazelcast 分布式缓存
知识点：cocos Creater 采用 Electron 编写
    前端建议开发工具：webstorm 
      
      
加入QQ群，了解最新开发进度 ，QQ群号： 529311623

[![输入图片说明](https://git.oschina.net/uploads/images/2017/0609/233259_8ab02715_1387891.png "在这里输入图片标题")](http:////shang.qq.com/wpa/qunwpa?idkey=3735ebb729ef696009be07fa2e2eba7feee6acf89c07e6e68a9b56504d9fabd0)

UI效果图：
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192826_1d6f397f_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0709/131509_9a969010_1387891.jpeg "在这里输入图片标题")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192845_5526c6bf_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192900_a0dee563_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192913_d65bc3cd_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192926_298b49ff_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192938_28a2548f_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/192954_eeba8b49_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0719/193004_066f1ad5_1387891.png "屏幕截图.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0709/131509_9a969010_1387891.jpeg "在这里输入图片标题")


修复问题列表：
1、对方出2，我方也可出2的问题
2、王炸不可压对方炸的问题
3、破产图标显示问题
4、破产后离开房间问题
5、隔天送币问题
6、金币不足不能进场问题 自上金币不足房间最小金币不允许进入，已在房间中的也不允许继续。
7、每天进来送金币5000，破产后还可送金币5000
8、明牌功能， 及加倍
9、女声问题
10、动画效果问题
11、输的时候显示女黑脸
12、王家头像显示功能
