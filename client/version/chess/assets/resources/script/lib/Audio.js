cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //    default: null,      // The default value will be used only when the component attaching
        //                           to a node for the first time
        //    url: cc.Texture2D,  // optional, default is typeof default
        //    serializable: true, // optional, default is true
        //    visible: true,      // optional, default is true
        //    displayName: 'Foo', // optional
        //    readonly: false,    // optional, default is false
        // },
        // ...
        bgVolume:0.2,           // 背景音量

        deskVolume:1.0,         //   房间 房间音量  == 音效

        yinxiaoVolume:1.0,     // 音效 FIXME 怀疑deskVolume没什么用
        
        bgAudioID:-1            //   背景 音乐  id
    },

    // use this for initialization
    init: function () {
        var t = cc.sys.localStorage.getItem("bgVolume");  //背景音量
        if(t != null){
            this.bgVolume = parseFloat(t);    
        }
        
        var t = cc.sys.localStorage.getItem("deskVolume"); //房间 房间音量

        if(t != null){
            this.deskVolume = parseFloat(t);
        }

        var t = cc.sys.localStorage.getItem("yinxiaoVolume"); //音效
        if (t!=null){
            this.yinxiaoVolume = parseFloat(t);
        }

        cc.game.on(cc.game.EVENT_HIDE, function () {
            cc.audioEngine.pauseAll();
        });
        cc.game.on(cc.game.EVENT_SHOW, function () {
            cc.audioEngine.resumeAll();
        });
    },

    getUrl:function(url){
        return cc.url.raw("resources/sounds/" + url);
    },
    
    playBGM(url){  //播放背景音 调用的
        var audioUrl = this.getUrl(url);
        if(this.bgAudioID >= 0){
            cc.audioEngine.stop(this.bgAudioID);
        }
        this.bgAudioID = cc.audioEngine.play(audioUrl,true,this.bgVolume);
    },
    
    playSFX(url){  //播放??
        console.log("enter playSFX" );
        var audioUrl = this.getUrl(url);
        if(this.deskVolume > 0){
            console.log("play SFX" + audioUrl );
            var audioId = cc.audioEngine.play(audioUrl,false,this.deskVolume);
        }
    },

    playYinXiao(url){  //播放音效
        console.log("enter playYinXiao" );
        var audioUrl = this.getUrl(url);
        if(this.yinxiaoVolume > 0){
            console.log("playYinXiao" + audioUrl );
            var audioId = cc.audioEngine.play(audioUrl,false,this.yinxiaoVolume);  //第一个参数为播放的文件地址，第二个参数为是否循环，第三个参数为音量
        }
    },
    
    setSFXVolume:function(v){
        if(this.deskVolume != v){
            cc.sys.localStorage.setItem("deskVolume",v);
            this.deskVolume = v;
        }
    },
    getState:function(){
        return cc.audioEngine.getState(this.bgAudioID);
    },
    setBGMVolume:function(v,force){
        if(this.bgAudioID >= 0){
            if(v > 0 && cc.audioEngine.getState(this.bgAudioID) === cc.audioEngine.AudioState.PAUSED){
                cc.audioEngine.resume(this.bgAudioID);
            }else if(v == 0){
                cc.audioEngine.pause(this.bgAudioID);
            }
        }
        if(this.bgVolume != v || force){
            cc.sys.localStorage.setItem("bgVolume",v);
            this.bgmVolume = v;
            cc.audioEngine.setVolume(this.bgAudioID,v);
        }
    },
    
    pauseAll:function(){
        cc.audioEngine.pauseAll();
    },
    
    resumeAll:function(){
        cc.audioEngine.resumeAll();
    }
});
