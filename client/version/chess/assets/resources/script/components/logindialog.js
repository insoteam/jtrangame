var beiMiCommon = require("BeiMiCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {},

    // use this for initialization
    onLoad: function () {
        //当手机触摸到屏幕时
        this.node.on(cc.Node.EventType.TOUCH_START, function (e) {
            e.stopPropagation();
        });
    },
    onCloseClick: function () {
        /**
         * *  对象池返回， 释放资源 ，  同时 解除 事件绑定
         *
         * */
        let common = this.getCommon("common");
        if (common != null) {
            common.loginFormPool.put(common.dialog);
        }
    }
});
