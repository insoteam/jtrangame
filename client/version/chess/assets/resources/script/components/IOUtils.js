/**
 * 封装了cc.sys.localStorage .
 * ConponentName  = IOUtils.js
 */
cc.Class({
    extends: cc.Component,

    properties: {},

    /**
     * 静态类成员，提供get,put,remove.
     * FIXME 为何不直接是个方法呢 有没有区别
     */
    statics: {
        get: function (key) {
            return cc.sys.localStorage.getItem(key);
        },
        put: function (key, value) {
            cc.sys.localStorage.setItem(key, value);
        },
        remove: function (key) {
            cc.sys.localStorage.removeItem(key);
        }
    }
});
