var beiMiCommon = require("BeiMiCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        text: {
            default: null,
            type: cc.Label
        }
    },

    // use this for initialization
    onLoad: function () {

    },
    init:function(text , time , target){
        let self = this ;
        this.remaining = time ;
        this.text.string = text +"（"+ this.remaining +"）" ;
        this.schedule(function() {
            this.remaining = this.remaining - 1 ;
            if(this.remaining < 0){
                self.unschedule(this);
            }else{
                self.text.string = text +"（"+ this.remaining +"）" ;
            }
        }, 1 , time);
    },
    stop:function(target){
        this.remaining = 0 ;
        target.destroy();
    }

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
