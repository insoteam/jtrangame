var beiMiCommon = require("BeiMiCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
    },

    // use this for initialization
    onLoad: function () {
        this.node.on(cc.Node.EventType.TOUCH_START, function(e){
            e.stopPropagation();
        });
    },
    onClose:function(){
        let dialog = cc.find("Canvas/alert") ;
        cc.beimi.dialog.put(dialog);
    }

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
