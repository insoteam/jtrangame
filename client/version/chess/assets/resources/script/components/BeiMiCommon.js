cc.Class({
    extends: cc.Component,

    properties: {},

    onLoad: function () {
    },
    /** FIXME 用来鉴定是否登陆的好象  应改名为 isLogin*/
    ready: function () {
        var check = false;
        if (cc.beimi) {
            check = true;
        } else {
            this.scene("login", this);
        }
        return check;
    },
    /**规范在目录下放脚本用于加载脚本的*/
    getCommon: function (common) {
        var object = cc.find("Canvas/script/" + common);
        return object.getComponent(common);
    },
    loadding: function () {
        if (cc.beimi.loadding.size() > 0) {
            this.loaddingDialog = cc.beimi.loadding.get();
            this.loaddingDialog.parent = cc.find("Canvas");

            this._animCtrl = this.loaddingDialog.getComponent(cc.Animation);
            var animState = this._animCtrl.play("loadding");
            animState.wrapMode = cc.WrapMode.Loop;
        }
    },
    alert: function (message) {
        if (cc.beimi.dialog.size() > 0) {
            this.alertdialog = cc.beimi.dialog.get();
            this.alertdialog.parent = cc.find("Canvas");
            let node = this.alertdialog.getChildByName("message");
            if (node != null && node.getComponent(cc.Label)) {
                node.getComponent(cc.Label).string = message;
            }
        }
    },
    closeloadding: function () {
        cc.beimi.loadding.put(cc.find("Canvas/loadding"));
    },
    closealert: function () {
        cc.beimi.dialog.put(cc.find("Canvas/alert"));
    },
    scene: function (name, self) {
        cc.director.preloadScene(name, function () {
            if (cc.beimi) {
                self.closeloadding(self.loaddingDialog);
            }
            cc.director.loadScene(name);
            //多分辨率适配方案 EXACT_FIT 拉伸变形，使铺满屏幕
            let win = cc.director.getWinSize();
            cc.view.setDesignResolutionSize(win.width, win.height, cc.ResolutionPolicy.EXACT_FIT);

        });
    },
    root: function () {
        return cc.find("Canvas");
    },
    decode: function (data) {
        var cards = [];

        if (!cc.sys.isNative) { //web
            var Base64 = require("Base64");
            var strArray = Base64.decode(data);
            if (strArray && strArray.length > 0) {
                for (var i = 0; i < strArray.length; i++) {
                    cards[i] = strArray[i];
                }
            }
            //var dataView = new DataView(strArray);
            //for (var i = 0; i < dataView.byteLength; i++) {
            //    cards[i] = dataView.getInt8(i);
            //}
        } else {                //native
            var Base64 = require("Base64");
            var strArray = Base64.decode(data);

            if (strArray && strArray.length > 0) {
                for (var i = 0; i < strArray.length; i++) {
                    cards[i] = strArray[i];
                }
            }
        }

        return cards;
    },
    parse(result){
        var data;
        if (cc.sys.isNative) {
            data = JSON.parse(JSON.parse(result));
        } else {
            data = JSON.parse(result);
        }
        return data;
    },
    reset: function (data, result) {
        //放在全局变量
        cc.beimi.authorization = data.token.id;   //授权
        cc.beimi.user = data.data;
        cc.beimi.games = data.games;

        cc.beimi.playway = null;
        this.io.put("userinfo", result);  //存本地记录
    },
    logout: function () {
        if (cc.beimi.dialog != null) {
            cc.beimi.dialog.destroy();
            cc.beimi.dialog = null;
        }
        cc.beimi.authorization = null;
        cc.beimi.user = null;
        cc.beimi.games = null;

        cc.beimi.playway = null;
    },
    /**
     * 这个是在每个调用email的时候都先要调的方法
     * @returns {null|*}
     */
    connect: function () {
        //cc.log("common   connect ....")
        //cc.beimi.socketstatus == "connecting";
        //cc.beimi.socket = window.io.connect(cc.beimi.http.wsURL + '/jtrangame');
        //cc.beimi.socket

        //加入了游戏状态参数 用于恢复游戏
        //var param = {
        //    token: cc.beimi.authorization,
        //    orgi: cc.beimi.user.orgi
        //};
        //cc.log("common   gamestatus...")
        //cc.beimi.socket.emit("gamestatus", JSON.stringify(param));
        //cc.beimi.socket.on("gamestatus", function (data) {
        //    cc.log("on gamestatus...");
        //    if (data != null) {
        //        cc.beimi.gamestatus = data.gamestatus;
        //        console.log(data);
        //    }
        //});
        //return cc.beimi.socket;
    },
    disconnect: function () {
        if (cc.beimi.socket != null) {
            cc.beimi.socket.disconnect();
            cc.beimi.socket = null;
        }
    },

    //socket: function () {
    //    if (cc.beimi.socketstatus == "none"){
    //
    //        cc.beimi.socket = window.io.connect(cc.beimi.http.wsURL + '/jtrangame');
    //    }
    //    return cc.beimi.socket;
    //},

    //map: function (command, callback) {
    //    if (cc.routes[command] == null) {
    //        cc.routes[command] = callback || function () {
    //            };
    //    }
    //},
    //route: function (command) {
    //    return cc.routes[command] || function () {
    //        };
    //}
});
