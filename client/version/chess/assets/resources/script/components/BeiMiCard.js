var beiMiCommon = require("BeiMiCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        card: cc.Integer,   //牌的ID 0-53
        initcard: {
            default: null,
            type: cc.Node
        },
        normal: {
            default: null,
            type: cc.Node
        },
        lefttop: {
            default: null,
            type: cc.Node
        },
        leftcolor: {
            default: null,
            type: cc.Node
        },
        rightbottom: {
            default: null,
            type: cc.Node
        },
        rightcolor: {
            default: null,
            type: cc.Node
        },
        kingbg: {
            default: null,
            type: cc.Node
        },
        king: {
            default: null,
            type: cc.Node
        },
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        }
    },
    proxy: function (data) {
        this.game = data;
    },
    // use this for initialization
    onLoad: function () {
        this.showOpposite();
        this.selected = false;
    },
    /**
     * 显示反面
     */
    showOpposite: function () {
        this.initcard.active = true;  //默认为背景 进行ORDER时显示正面
        this.normal.active = false;
        this.kingbg.active = false;
    },
    /**
     * 显示正面
     */
    showCorrectSide: function () {
        this.order();
    },
    setCard: function (card) {
        this.card = card;
        this.showOpposite();
    },
    unselected: function () {
        if (this.selected) {
            this.normal.y = 0;  //普通牌
            this.kingbg.y = 0;  //大小鬼牌
        }
        this.selected = false;
    },
    order: function () {
        //0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,
        // 29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,
        // 52,53  小王，大王
        var frame, cardframe;
        if (this.card < 52) {
            var cardvalue = this.card + 1;
            if (cardvalue % 4 == 0) {
                frame = this.atlas.getSpriteFrame('牌-方片');
            } else if (cardvalue % 4 == 1) {
                frame = this.atlas.getSpriteFrame('牌-黑桃');
            } else if (cardvalue % 4 == 2) {
                frame = this.atlas.getSpriteFrame('牌-红心');
            } else if (cardvalue % 4 == 3) {
                frame = this.atlas.getSpriteFrame('牌-梅花');
            }
            var src = (this.card - this.card % 4 ) / 4 + 1 + 2;
            if (src == 14) {
                src = 1;
            } else if (src == 15) {
                src = 2;
            }
            if (this.card % 2 == 0) {
                cardframe = this.atlas.getSpriteFrame('牌-' + src);
            } else {
                cardframe = this.atlas.getSpriteFrame('牌-r' + src);
            }
            this.leftcolor.getComponent(cc.Sprite).spriteFrame = frame;
            this.lefttop.getComponent(cc.Sprite).spriteFrame = cardframe;
            this.rightcolor.getComponent(cc.Sprite).spriteFrame = frame;
            this.rightbottom.getComponent(cc.Sprite).spriteFrame = cardframe;

            this.initcard.active = false;
            this.normal.active = true;
            this.normal.y = 0;
            this.kingbg.active = false;
        } else if (this.card == 52) {
            frame = this.atlas.getSpriteFrame('牌-小王_大');
            this.king.getComponent(cc.Sprite).spriteFrame = frame;
            this.initcard.active = false;
            this.normal.active = false;
            this.kingbg.active = true;
            this.kingbg.y = 0;
        } else if (this.card == 53) {
            frame = this.atlas.getSpriteFrame('牌-大王_大');
            this.king.getComponent(cc.Sprite).spriteFrame = frame;
            this.initcard.active = false;
            this.normal.active = false;
            this.kingbg.active = true;
            this.kingbg.y = 0;
        }
    }
});
