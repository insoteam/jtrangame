cc.Class({
    extends: cc.Component,

    properties: {


    },

    onLoad: function () {
    },
    /**
     * @param source            调用的源
     * @param timerNode         计时器所在的节点
     * @param atlas             计时器图集
     * @param timer_first       计时器首个计时字母
     * @param timer_sec         计时器第二个数字
     * @param times             计时器执行次数
     */
    runtimer:function(source , timerNode  , atlas, timer_first , timer_sec , times){

        let self = this ;

        //1设置时钟倒计时画面显示  FIXME 这段代码似科可以与后面的回调合并
        if(timerNode){
            timerNode.active = false ;
        }
        this.remaining = times ;   //倒计时总数
        //获得十位数的对应图集
        let timer_first_num = atlas.getSpriteFrame('jsq'+parseInt(self.remaining/10))
        //获得个位数的对应图集
        let timer_sec_num = atlas.getSpriteFrame('jsq'+self.remaining % 10) ;
        //将图集赋线图像以显示十位数的画面
        timer_first.getComponent(cc.Sprite).spriteFrame = timer_first_num;
        //将图集赋线图像以显示个位数的画面
        timer_sec.getComponent(cc.Sprite).spriteFrame = timer_sec_num;
        if(timerNode){
            timerNode.active = true ;
        }

        this.timersrc = function() {
            self.remaining = self.remaining - 1 ;
            if(self.remaining < 0){
                source.unschedule(this);
                timerNode.active = false ;
            }else{
                timer_first_num = atlas.getSpriteFrame('jsq'+parseInt(self.remaining/10))
                timer_sec_num = atlas.getSpriteFrame('jsq'+self.remaining % 10) ;
                timer_first.getComponent(cc.Sprite).spriteFrame = timer_first_num;
                timer_sec.getComponent(cc.Sprite).spriteFrame = timer_sec_num;
            }
        } ;
        //每秒执行一次回调，执行times次，0延迟
        source.schedule(this.timersrc, 1 , times , 0);

        return this.timersrc ;
    },
    /**
     *
     * @param source       调用的源
     * @param timernode    计时器所在的节点
     * @param timer        target
     */
    stoptimer:function(source , timernode , timer){
        if(timernode){
            timernode.active = false ;
        }
        let self = this ;
        this.remaining = 0;
        if(timer){
            source.unschedule(timer);
        }
    }

    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});
