cc.Class({
    extends: cc.Component,

    properties: {
        summary_win:{
            default: null,
            type: cc.Node
        },
    },

    // use this for initialization
    onLoad: function () {

    },
    rePlayGame(){
        let aa = this.summary_win.getComponent("DizhuWinLossRander");
        if (aa){
            aa.rePlayGame();
        }
    }
    ,
    rePlayGameDeal()
    {
        let aa = this.summary_win.getComponent("DizhuWinLossRander");
        if (aa) {
            aa.rePlayGameDeal();
        }
    }
});
