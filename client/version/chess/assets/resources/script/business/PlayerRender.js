/**
 * 玩家渲染JS
 * 地主dizbu_player.prefab 调用
 * 每个玩家一份
 */
cc.Class({
    extends: cc.Component,

    properties: {
        username: {  //用户名
            default: null,
            type: cc.Label
        },
        goldcoins: { //金币
            default: null,
            type: cc.Label
        },
        dizhu: {    //地主标志
            default: null,
            type: cc.Node
        },
        pokertag: {  //扑克背景标记
            default: null,
            type: cc.Node
        },
        pokercards: {//显示余牌张数
            default: null,
            type: cc.Label
        },
        timer: {     //计时包 包含 jsq,
            default: null,
            type: cc.Node
        },
        headimg: {  //头像
            default: null,
            type: cc.Node
        },
        atlas: {  //所用txtTure
            default: null,
            type: cc.SpriteAtlas
        },
        jsq: {       //计时器
            default: null,
            type: cc.Node
        },
        timer_first: {  //第一个时钟数字
            default: null,
            type: cc.Node
        },
        timer_sec: {   //第二个时钟数字
            default: null,
            type: cc.Node
        },
        result: {     //叫地主
            default: null,
            type: cc.Node
        },
        tackCardsLayout: { // 其它玩家放置出牌的地方
            default: null,
            type: cc.Node
        },
        cannot: {    //要不起
            default: null,
            type: cc.Node
        },
        donot: {     //不出
            default: null,
            type: cc.Node
        }
    },

    // use this for initialization
    onLoad: function () {
        //手上扑克牌张数
        this.cardcount = 0;
        //手上扑克牌列表
        this.cardslist = [];
        //TODO ？？
        this.isRight = false;
    },
    /**
     * 初始化玩家信息
     * @param data  后台传过来的数据包
     * @param isRight 是否右边玩家
     */
    initplayer: function (data, isRight) {
        this.username.string = data.username;  //用户名称，用于显示在图象上
        this.userid = data.id;  //用户ID，

        if (isRight == true) {
            //默认这些值都为-10 这里都乘以-1变成正数 ，是否表示转成可显示
            this.pokertag.x = this.pokertag.x * -1;
            this.timer.x = this.timer.x * -1;   //FIXME 这个要理一下
            this.headimg.x = this.headimg.x * -1;
            this.result.x = this.result.x * -1;
            this.cannot.x = this.cannot.x * -1;
            this.donot.x = this.donot.x * -1;
            this.jsq.x = this.jsq.x * -1;

            this.dizhu.x = this.dizhu.x * -1;
            this.tackCardsLayout.x = this.tackCardsLayout.x * -1;
            this.tackCardsLayout.getComponent(cc.Layout).horizontalDirection = 1;  //右对齐

            this.isRight = isRight;
        }
        this.refreshGoldcoins(data.goldcoins);

        if (this.dizhu) {      //判断地主图像元存在
            //设置为不显示
            this.dizhu.active = false;
        }
        if (this.jsq) {        //判断时钟  图像元存在
            //设置为不显示
            this.jsq.active = false;
        }
        if (this.result) {    //判断叫地主 图像元存在
            //设置为不显示
            this.result.active = false;
        }
        if (this.cannot) {    //判断要不起 图像元存在
            //设置为不显示
            this.cannot.active = false;
        }
        if (this.donot) {     //判断不出 图象元存在
            //设置为不显示
            this.donot.active = false;
        }
        if (this.tackCardsLayout) {
            //设置为不显示
            this.tackCardsLayout.active = false;
        }
    },
    refreshGoldcoins: function (goldcoins) {
        if (this.goldcoins) { //判断金币图像元是否存在
            //金币超过万，显示万，否则显示原始值
            if (goldcoins > 10000) {
                var num = goldcoins / 10000;
                this.goldcoins.string = num.toFixed(2) + '万';
            } else {
                this.goldcoins.string = goldcoins;
            }
        }
    },
    countcards: function (cardsnum) {
        this.cardcount = this.cardcount + cardsnum;
        this.pokercards.string = this.cardcount;
    },
    resetCardCount: function (cardsnum) {  //FIXME 这个名字应该直接叫setCardCounts
        this.cardcount = cardsnum;
        this.pokercards.string = this.cardcount;
    },
    catchtimer: function (times) {
        if (this.jsq) {
            this.jsq.active = true;
        }
        if (this.result) {
            this.result.active = false;
        }
        if (this.cannot) {
            this.cannot.active = false;
        }
        if (this.donot) {   //不抢
            this.donot.active = false;
        }

        var gameTimer = require("GameTimer");
        this.beimitimer = new gameTimer();
        this.timesrc = this.beimitimer.runtimer(this, this.jsq, this.atlas, this.timer_first, this.timer_sec, times);
    },
    /**
     * 是否显示地主标志
     * @param isShow true显示，false 不显示
     */
    showDizhuFlag: function (isShow) {
        if (this.dizhu) {
            this.dizhu.active = isShow;
        }
    },
    catchresult: function (data) {
        cc.log("抢地主");

        //先都不显示
        this.hideresult();

        if (data.grab) {   // grab = true 表示 抢地主
            //抢地主
            if (this.result) {
                this.result.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame('提示_抢地主');
                this.result.active = true;
            }
        } else {    //不抢
            if (this.result) {
                this.result.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame('提示_不抢');
                this.result.active = true;
            }
        }
    },

    hideresult: function () {
        if (this.beimitimer && this.timesrc) {
            this.beimitimer.stoptimer(this, this.jsq, this.timesrc); //时间停止
        }
        if (this.jsq) {
            this.jsq.active = false;
        }
        if (this.result) {
            this.result.active = false;
        }
        if (this.cannot) {
            this.cannot.active = false;
        }
        if (this.donot) {
            this.donot.active = false;
        }
    },
    lasthands: function (self, game, data) {     //其它玩家
        this.hideresult();
        if (this.beimitimer && this.timesrc) {
            this.beimitimer.stoptimer(this, this.jsq, this.timesrc);
        }
        if (this.userid == data.userid) {//设置地主
            if (this.pokercards) {  //加三张牌
                this.countcards(3);
            }
            /**
             * 开始计时
             */
            this.playtimer(game, 25); //出牌计时
        }
        this.setDizhuFlag(data);
    },
    setDizhuFlag: function (data) {
        cc.log("enter setDizhuFlag...................")
        cc.log(this.userid == data.userid);
        this.dizhu.active = this.userid == data.userid; //设置地主
    },
    clearLastTakeCards: function (game) {
        for (var i = 0; i < this.cardslist.length; i++) {
            game.pokerpool.put(this.cardslist[i]);//回收回去
        }
        this.cardslist.splice(0, this.cardslist.length);//删除数组里的所有内容
    },
    /**
     * 出的牌
     * @param game  游戏数据对象 DizhuDataBind.js
     * @param self  游戏主对象 DizhuBegin.js
     * @param cardsnum 当前玩家剩余牌数
     * @param tackCards 当前玩家要出的牌
     * @param data  当前玩家后台传来的data数据
     */
    tackCards: function (game, self, cardsnum, _tackCards, data) {
        this.hideresult();

        if (this.tackCardsLayout) {//出的牌
            this.tackCardsLayout.active = true;
        }

        //清除之前出的牌
        this.clearLastTakeCards(game);

        if (data.donot == true) {//不出

            if (data.sameside == "1") {
                self.getPlayer(data.userid).js.tipdonot();
            } else {
                self.getPlayer(data.userid).js.tipcannot();
            }
        } else {//出牌

            this.resetCardCount(cardsnum);
            //排序一下
            var sort = function bubbleSort(arr) {   //冒泡排序
                for (var i = 0; i < arr.length - 1; i++) {
                    for (var j = 0; j < arr.length - 1; j++) {
                        if (arr[j + 1].card > arr[j].card) {
                            let temp = arr[j + 1];
                            arr[j + 1] = arr[j];
                            arr[j] = temp;
                        }
                    }
                }
                return arr;
            };
            _tackCards = sort(_tackCards);

            //出牌
            for (var i = 0; i < _tackCards.length; i++) {
                this.playcards(game, i, _tackCards, _tackCards[i]);
            }

            ////这个好象也没效果
            for (var i = 0; i < _tackCards.length; i++) {
                var poker = this.cardslist[i];
                poker.zIndex = 54 - poker.card;
            }


        }
    },
    tipcannot: function () {
        if (this.result) {
            this.result.active = false;
        }
        if (this.cannot) {
            this.cannot.active = true;
        }
        if (this.donot) {
            this.donot.active = false;
        }
    },
    tipdonot: function () {
        if (this.result) {
            this.result.active = false;
        }
        if (this.cannot) {
            this.cannot.active = false;
        }
        if (this.donot) {
            this.donot.active = true;
        }
    },
    /**
     * 当前玩家出牌（每张牌的出牌处理）
     * @param game 游戏数据对象
     * @param index 本次出牌第几张  对应lastcards的数组index
     * @param tackCards 本次出牌总列表
     * @param card  本次出的牌值
     */
    playcards: function (game, index, tackCards, card) {
        cc.log("enter playerRender playcards");
        let cardPoker = game.createPoker(this.tackCardsLayout);
        //this.tackCardsLayout.addChild(cardPoker);
        cardPoker.card = card;
        cardPoker.js.setCard(card);
        cardPoker.js.showCorrectSide();
        cardPoker.setScale(0.5, 0.5);
        cardPoker.setPositionY(0);
        //cardPoker.setPositionX((index + 1) * 40 - 30);
        //cardPoker.setPosition(0, 0);

        //if (this.isRight) {
        //    cardPoker.zIndex = 54 - cardPoker.card; //FIXME 这个好象也没多大意义 要排应该用后台的排比较好
        //} else {
        //    cardPoker.zIndex = cardPoker.card; //FIXME 这个好象也没多大意义 要排应该用后台的排比较好
        //}
        this.cardslist[this.cardslist.length] = cardPoker;  //添加当前出的牌至cardslist中//FIXME 这个LIST是乎可以不要，用控件就好了
    },
    playtimer: function (game, times) {
        if (this.result) {
            this.result.active = false;
        }
        if (this.cannot) {
            this.cannot.active = false;
        }
        if (this.donot) {
            this.donot.active = false;
        }
        if (this.tackCardsLayout) {
            this.tackCardsLayout.active = false;
        }
        for (var i = 0; i < this.cardslist.length; i++) {
            game.pokerpool.put(this.cardslist[i]);//回收回去
        }
        var gameTimer = require("GameTimer");
        this.beimitimer = new gameTimer();
        this.timesrc = this.beimitimer.runtimer(this, this.jsq, this.atlas, this.timer_first, this.timer_sec, times);
    }

});
