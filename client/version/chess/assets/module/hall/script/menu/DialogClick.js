cc.Class({
    extends: cc.Component,

    properties: {},

    onLoad: function () {
    },
    onClick: function (event) {
        event.stopPropagation();
    },
    onCloseClick: function () {
        cc.beimi.dialog.destroy();
        cc.beimi.dialog = null;
    }
});
