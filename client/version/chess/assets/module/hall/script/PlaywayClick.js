var beiMiCommon = require("BeiMiCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        playway: {
            default: null,
            type: cc.Node
        },
    },

    onLoad: function () {

    },
    //经典斗地主的选玩法
    onClick: function () {
        let self = this;
        this.loadding();    //等待框
        var selectPlayway = this.getCommon("SelectPlayway");

        let thisplayway = this.playway.getComponent("Playway");
        setTimeout(function () {
            /**
             * 优化交互，预加载场景完毕后再回收资源
             */
            selectPlayway.collect();           //回收资源 playwayPool
            self.scene(thisplayway.data.code, self);
        }, 200);
    },
    createRoom: function (event, data) {
        let self = this;
        this.loadding();
        setTimeout(function () {
            self.scene(data, self);
        }, 200);
    }

});
