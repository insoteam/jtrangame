var beiMiCommon = require("BeiMiCommon");
cc.Class({
    extends: beiMiCommon,
    properties: {
        tag: {                   //洗牌不洗牌
            default: null,
            type: cc.Node
        },
        score: {                  //底分
            default: null,
            type: cc.Label
        },
        onlineusers: {           //以线用户
            default: null,
            type: cc.Label
        },
        scorelimit: {           //金币上下限
            default: null,
            type: cc.Label
        },
        atlas: {                //图集对象
            default: null,
            type: cc.SpriteAtlas
        }
    },
    onLoad: function () {
    },
    init: function (playway) {
        /**
         * 需要预先请求 在线人数
         */
        if (playway) {

            this.data = playway;

            if (playway.shuffle == false) {
                this.tag.active = false;
            } else {
                this.tag.active = true;
            }

            var frameName = "初级";
            if (playway.level == '2') {                 //级别
                frameName = "高级"
            }
            frameName = frameName + playway.skin;

            this.getComponent(cc.Sprite).spriteFrame = this.atlas.getSpriteFrame(frameName);

            //在线人数
            this.onlineusers.string = playway.onlineusers + " 人 ";

            //上下限
            var min = parseInt(playway.mincoins / 1000) + "千";
            if (playway.mincoins >= 10000) {
                min = parseInt(playway.mincoins / 10000) + "万";
            }

            var max = parseInt(playway.maxcoins / 1000) + "千";
            if (playway.maxcoins >= 10000) {
                max = parseInt(playway.maxcoins / 10000) + "万";
            }
            this.scorelimit.string = min + " - " + max;

            //底分
            this.score.string = playway.score;
        }
    }
});
