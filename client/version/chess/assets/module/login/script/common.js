/**
 * 游戏入口，因为这里的login按钮为登陆按
 */
var beiMiCommon = require("BeiMiCommon");

cc.Class({
    extends: beiMiCommon,

    // use this for initialization
    onLoad: function () {
        /**
         * 游客登录，无需弹出注册对话框，先从本地获取是否有过期的对话数据，如果有过期的对话数据，则使用过期的对话数据续期
         * 如果没有对话数据，则重新使用游客注册接口
         */
        cc.beimi.game = {
            model: null,   //哪种游戏
            playway: null,  //游戏方式
            type: function (name) {//游戏类型
                if (cc.beimi.game.model != null) {
                    cc.log(cc.beimi.game.model.types);
                    for (var i = 0; i < cc.beimi.game.model.types.length; i++) {
                        var type = cc.beimi.game.model.types[i];
                        if (type.code == name) {
                            return type;
                        }
                    }
                }
                return temp;
            }
        };
    },
    login: function () {  //按钮事件
        cc.info("start game................");
        if (this.io == window.io) {
            cc.log("如果这个成立，那就是个重大错误！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！")
        }
        this.io = require("IOUtils");    //IOUtils 就是个读取cocos 本地存储的封装工具
        this.loadding();
        if (this.io.get("userinfo") == null) {   //居然是get本地存储的内容，不知道在哪存储了
            cc.info("loading local info fail.");
            //发送游客注册请求  后台响应类：com.beimi.web.handler.api.rest.user.GuestRegisterController
            //com.beimi.web.handler.ApplicationController
            cc.info("remote request ok")
            var xhr = cc.beimi.http.httpGet("/api/guest", this.sucess, this.error, this);

        } else {
            //通过ID获取 玩家信息
            var data = JSON.parse(this.io.get("userinfo"));
            if (data.token != null) {     //获取用户登录信息
                var xhr = cc.beimi.http.httpGet("/api/guest?token=" + data.token.id, this.sucess, this.error, this);
            }
        }
    },

    //loginWeiXin: function () {
    //    var self = this;
    //    cc.beimi.anysdkMgr.login();
    //    //var self = this;
    //    //this.loadding();
    //    //cc.beimi.http.httpGet("/api/login?account=" + this.account + ",sign=" + this.sign, this.sucessWeiXin, this.error, this);
    //},


//sucessWeiXin: function (result, contex) {
//        var data = JSON.parse(result);
//        if (data != null && data.token != null && data.data != null) {
//            cc.beimi.gamestatus = data.data.gamestatus;
//            //预加载场景 这里根据后台设置默认游戏获得
//            contex.gamestart(contex);
//        }
//    },
    sucess: function (result, contex) {
        cc.log("sucess ....");
        var data = JSON.parse(result);
        cc.log(data);
        if (data != null && data.token != null && data.data != null) {
            //放在全局变量
            contex.reset(data, result);
            cc.beimi.gamestatus = data.data.gamestatus;
            //预加载场景 这里根据后台设置默认游戏获得
            contex.gamestart(contex);
        }
    },
    error: function (object) {
        object.closeloadding(object.loaddingDialog);  //看不懂为何是PUT
        object.alert("网络异常，服务访问失败");
    },
    gamestart: function (contex) {
        //只定义了单一游戏类型 ，否则 进入游戏大厅
        if (cc.beimi.games && cc.beimi.games.length == 1) {
            contex.scene(cc.beimi.games[0].code, contex);
            /**
             * 传递全局参数，当前进入的游戏类型，另外全局参数是 选场
             */
            cc.beimi.game.model = cc.beimi.games[0];
        } else {
            /**
             * 暂未实现功能
             */
        }
    }
});
