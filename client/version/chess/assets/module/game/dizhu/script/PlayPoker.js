cc.Class({
    extends: cc.Component,

    properties: {
        posy: cc.Integer,
        card: {
            default: null,
            type: cc.Node
        }
    },

    onLoad: function () {
        this.posy = this.card.y;
    },
    takecard: function (event) {
        var beiMiCard = event.target.parent.getComponent("BeiMiCard");
        if (event.target.y == this.posy) {
            event.target.y = event.target.y + 30;
            cc.log("zindex:" + event.target.parent.zIndex)
            cc.log("x" + event.target.parent.x);
            beiMiCard.selected = true;
        } else {
            cc.log(event.target.parent.x);
            event.target.y = event.target.y - 30;
            beiMiCard.selected = false;
        }
    }

});
