var beiMiCommon = require("BeiMiCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        winorloss: {            //输或赢
            default: null,
            type: cc.Node,
        },
        rulePanel: {            //人像
            default: null,
            type: cc.Node,
        },
        goldNotEnoughPanel: {    //破产
            default: null,
            type: cc.Node,
        },
        selfNode: {              //自已的节点面板
            default: null,
            type: cc.Node,
        },
        other1Node: {           //第一个其它玩家的面板
            default: null,
            type: cc.Node,
        },
        other2Node: {           //第二个其它玩家的面板
            default: null,
            type: cc.Node,
        },
    },

    // use this for initialization
    onLoad: function () {

    },
    initRander: function (sf, data, user) {
        cc.log("enter DizhuWinLossRander initRander");
        cc.log(data);
        this.dizHander = sf;   //FIXME 这个先用这个记录下主游戏句柄
        cc.log("dizbeg");
        cc.log(this.dizHander);
        var otheridx = 0;     //第几个其它玩家 用于获得要显示数据的节点
        for (var i = 0; i < data.players.length; i++) {
            let player = data.players[i];
            let isDizu = player.playuser == data.banker;         //标识是否显示地主标计
            let isGoldNotEnought = player.goldcoins <= 0;        //金币是否不足，用于标识破产
            let isMe = user.id == player.playuser;               //是本人

            //金币的改变及颜色 变量
            let add = "";                                       //金币变化值前面的+-符号
            let changeColor;                                    //金币变化相应变化的颜色
            if (player.win) {
                add = "+";
                changeColor = new cc.Color(255, 222, 49, 255);  //黄色
            } else {
                add = "";                                       //负数时本身有一个负号了
                changeColor = new cc.Color(255, 255, 255, 255); //白色
            }

            changeColor = new cc.Color(255, 222, 49, 255);  //都设一样算了，FIXME 前面的判断先留着

            let node = null;          //用户对应的节点
            cc.log(this);
            if (isMe) {   //自已
                if (player.win) {
                    this._showHini(this.winorloss, "你赢了");
                    this._showHini(this.rulePanel, "胜利角色");
                } else {
                    this._showHini(this.winorloss, "你输了");
                    this._showHini(this.rulePanel, "失败角色");
                }
                node = this.selfNode;
                //显示用户信息
            } else {                            //其它玩家
                otheridx = otheridx + 1;
                if (otheridx == 1) {   //
                    node = this.other1Node;
                }
                if (otheridx == 2) {
                    node = this.other2Node;
                }
            }

            //显示玩家数据
            this._showScore(node, isMe, isDizu, player.username, add + player.goldchangs, changeColor,
                isGoldNotEnought, player.goldcoins);

        }
    }
    ,
    _showScore(node, isMe, isDiZhu, userName, goldChanges, changeColor, isGoldNotEnought, goldcoins){
        cc.log(node);
        if (node) {
            node.getChildByName("地主标志").active = isDiZhu;                                  //地主标识
            if (!isMe) {  //自已就只显示我自已
                node.getChildByName("username").getComponent(cc.Label).string = userName;      //用户名
            }
            node.getChildByName("gold").getComponent(cc.Label).string = goldChanges;           //数值改变
            node.getChildByName("gold").setColor(changeColor);                                 //颜色改变
            node.getChildByName("破产").active = isGoldNotEnought;                             //破产标识
            node.getChildByName("goldcoins").getComponent(cc.Label).string = "" + goldcoins;   //金币余额
        }
    },
    rePlayGame()
    {
        if (this.dizHander) {
            this.dizHander.summaryNode.active = false;
            this.dizHander.replay(false);
        }
    }
    ,
    rePlayGameDeal()
    {
        if (this.dizHander) {
            this.dizHander.summaryNode.active = false;
            this.dizHander.replay(true);
        }
    }
    ,
    _showHini: function (node, name) {
        if (node) {
            for (var i = 0; i < node.children.length; i++) {
                node.children[i].active = false;
                if (node.children[i].name == name) {
                    node.children[i].active = true;
                }
            }
        }
    }
    ,

    setDizhuFlag: function (data) {
        if (data.id == data.userid) {//设置地主
            this.dizhu.active = true;
        } else {
            this.dizhu.active = false;
        }
    }
    ,

})
;
