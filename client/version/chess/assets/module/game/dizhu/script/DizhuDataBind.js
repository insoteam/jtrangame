var beiMiCommon = require("BeiMiCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        goldcoins: {
            default: null,
            type: cc.Label
        },
        cards: {
            default: null,
            type: cc.Label
        },
        player: {
            default: null,
            type: cc.Prefab
        },
        poker: {
            default: null,
            type: cc.Prefab
        },
        myself: {
            default: null,
            type: cc.Prefab
        },
        atlas: {
            default: null,
            type: cc.SpriteAtlas
        },
        catchbtn: {
            default: null,
            type: cc.Node
        },
        timer: {
            default: null,
            type: cc.Node
        },
        timer_first: {
            default: null,
            type: cc.Node
        },
        timer_sec: {
            default: null,
            type: cc.Node
        },
        taskCardsPanel: {  //玩家出牌堆放位置节点
            default: null,
            type: cc.Node
        },
        playbtn: {
            default: null,
            type: cc.Node
        },
        notallow: {
            default: null,
            type: cc.Node
        },
        operesult: {
            default: null,
            type: cc.Node
        }
    },
    init(){
        if (this.timer) {
            this.timer.active = false;
        }
        if (this.catchbtn) {
            this.catchbtn.active = false;
        }
        if (this.playbtn) {
            this.playbtn.active = false;
        }
        if (this.notallow) {
            this.notallow.active = false;
        }
        if (this.operesult) {
            this.operesult.active = false;
        }
    },

    // use this for initialization
    onLoad: function () {
        this.init();

        this.playerspool = new cc.NodePool();   //二其它玩家
        this.myselfpool = new cc.NodePool();    //玩家自已
        this.pokerpool = new cc.NodePool();     //牌

        this.selectedcards = [];      //存放当前玩家 选中 的牌  记录的是牌的值 {53,52,1,0}

        this.cardslist = [];           //存放当前玩家出的牌  记录的是牌的对象 this.poker cc.Prefab

        //二个其它玩家
        for (var i = 0; i < 2; i++) {
            this.playerspool.put(cc.instantiate(this.player)); // 创建节点
        }
        ////开牌时的显示背面
        //for (i = 0; i < 54; i++) {
        //    this.pokerpool.put(cc.instantiate(this.poker));     //牌-背面
        //}
        //玩家自已prefab
        this.myselfpool.put(cc.instantiate(this.myself));

        //设置金币
        this.refreshGoldcoins();

        if (this.myselfpool.size() > 0 && cc.beimi) {//判断存在
            this.playermyself = this.myselfpool.get();
            this.playermyself.parent = this.root();
            this.playermyself.setPosition(-520, -180);
            this.playermyself.js = this.playermyself.getComponent("PlayerRender");
            this.playermyself.js.initplayer(cc.beimi.user);
        }
    },

    refreshGoldcoins: function () {
        if (this.ready()) {
            if (cc.beimi.user.goldcoins > 9999) {
                var num = cc.beimi.user.goldcoins / 10000;
                this.goldcoins.string = num.toFixed(2) + '万';
            } else {
                this.goldcoins.string = cc.beimi.user.goldcoins;
            }
            this.cards.string = cc.beimi.user.cards + "张";
        }
    },

    /**
     * 获得其它玩家
     * 这里进行一次封装 ,省得到处理都一堆代码
     * onePlayer为Prefab
     * js为代码
     */
    createPlayer: function (parentNode) {
        var onePlayer = null;
        if (this.playerspool.size() > 0) {
            onePlayer = this.playerspool.get();

        } else {
            onePlayer = cc.instantiate(this.player);
        }
        onePlayer.parent = parentNode;
        onePlayer.js = onePlayer.getComponent("PlayerRender");  //放到JS里，省得到处都要getComponent()
        return onePlayer;
    },
    /**
     * 创建一个扑克牌对象
     * @param parent  指定创建完的牌的父对象
     * @returns {*}
     */
    createPoker: function (parentNode) {
        var onePoker = null;
        if (this.pokerpool.size() > 0) {
            cc.log("get pool");
            onePoker = this.pokerpool.get();  //从缓冲池中取一个没用的牌
        } else {
            cc.log("get instantiate");
            onePoker = cc.instantiate(this.poker);
        }
        onePoker.parent = parentNode;
        onePoker.js = onePoker.getComponent("BeiMiCard");  //放到JS里，省得到处都要getComponent()
        onePoker.js.showOpposite();
        return onePoker;
    },
    catchtimer: function (times) {

        if (this.playbtn) {
            this.playbtn.active = false;
        }
        if (this.timer) {
            this.timer.active = true;
        }
        if (this.catchbtn) {
            this.catchbtn.active = true;
        }
        if (this.operesult) {
            this.operesult.active = false;
        }
        let self = this;
        var gameTimer = require("GameTimer");
        this.beimitimer = new gameTimer();
        this.timesrc = this.beimitimer.runtimer(this, this.timer, this.atlas, this.timer_first, this.timer_sec, times);
    },
    catchresult: function (data) {
        if (this.timer) {
            this.timer.active = false;
        }
        if (this.catchbtn) {
            this.catchbtn.active = false;
        }
        if (this.playbtn) {
            this.playbtn.active = false;
        }
        if (this.timesrc) {
            this.beimitimer.stoptimer(this, this.timer, this.timesrc);
        }

        this.operesult.active = true;
        if (true == data.grab) {
            //cc.beimi.dizhuAudio.playDoCatch();
            this.showHini(this.operesult, "提示_叫地主");
        } else {
            //cc.beimi.dizhuAudio.playNoDoCatch();
            this.showHini(this.operesult, "提示_不抢");
        }
        //this.doOperatorResult("catch", data.grab, false);
    },
    clearDesk: function () {
        for (var i = 0; i < this.operesult.children.length; i++) {
            this.operesult.children[i].active = false;
        }
        this.operesult.active = false;

        this.playermyself.js.dizhu.active = false;
    },
    lasthands: function (self, game, data) {   //设置地主
        //var render = this.playermyself.getComponent("PlayerRender");
        this.playermyself.js.setDizhuFlag(data);
        if (this.operesult) {
            this.operesult.active = false;
        }
    },

    setControlNoActive: function () {

        if (this.result) {
            this.result.active = false;
        }
        if (this.playbtn) {
            this.playbtn.active = false;
        }
        if (this.catchbtn) {
            this.catchbtn.active = false;
        }
        if (this.jsq) {
            this.jsq.active = false;
        }
        if (this.taskCardsPanel) {
            this.taskCardsPanel.active = true;
        }
        if (this.timesrc) {
            this.beimitimer.stoptimer(this, this.timer, this.timesrc);
        }
    },
    /**
     *
     * @param game
     * @param self
     * @param cardsnum
     * @param lastcards
     * @param data
     */
    myTackCards: function (game, self, cardsnum, lastcards, data) {//aa

        //设置按钮及时钟不可操作
        this.setControlNoActive();

        this.clearUserTackCards();

        if (data.donot == false) {    //出牌
            for (var i = 0; i < lastcards.length; i++) {
                this.playcards(self, i, lastcards[i]);
            }
        } else {                       //不出
            this.doOperatorResult("myTackCards", true, data.sameside);
        }
    }
    ,
//清除用户所出的牌
    clearUserTackCards: function () {
        for (var i = 0; i < this.cardslist.length; i++) {
            this.pokerpool.put(this.cardslist[i]);//回收回缓冲池，以后用  回收回缓冲池会自已移除父节点
        }
        this.cardslist.splice(0, this.cardslist.length);//删除数组里的所有内容
    }
    ,

    /**
     * 打出的牌
     * @param self  dizhubegin.js
     * @param index 第几张
     * @param card
     */
    playcards: function (self, index, card) {
        var cardPoker;
        for (var inx = 0; inx < self.handCards.length; inx++) {
            let pc = self.handCards[inx];
            if (pc.card == card) {
                cardPoker = pc;
                break;
            }
        }
        if (cardPoker != null) {
            cardPoker.x = index * 30 - 30;
            cardPoker.y = 0;
            cardPoker.zIndex = index;
            cardPoker.setScale(0.5, 0.5);
            cardPoker.parent = this.taskCardsPanel;
            this.cardslist[this.cardslist.length] = cardPoker;
        }
    }
    ,
    playtimer: function (game, times) {
        if (this.timer) {
            this.timer.active = true;
        }
        if (this.playbtn) {
            this.playbtn.active = true;
            this.playbtn.getChildByName("不出").active = true;
        }
        if (this.catchbtn) {
            this.catchbtn.active = false;
        }
        if (this.taskCardsPanel) {
            this.taskCardsPanel.active = false;
        }
        if (this.operesult) {
            this.operesult.active = false;
        }

        for (var i = 0; i < this.cardslist.length; i++) {
            this.cardslist[i].setScale(1, 1);
            game.pokerpool.put(this.cardslist[i]);//回收回去
        }
        let self = this;
        var gameTimer = require("GameTimer");
        this.beimitimer = new gameTimer();
        this.timesrc = this.beimitimer.runtimer(this, this.timer, this.atlas, this.timer_first, this.timer_sec, times);
    }
    ,
    showHini: function (node, name) {
        for (var i = 0; i < node.children.length; i++) {
            node.children[i].active = false;
            if (node.children[i].name == name) {
                node.children[i].active = true;
            }
        }
    }
    ,
    doOperatorResult: function (oper, resvalue, sameside) {
        this.operesult.active = true;
        if (oper == "catch") {
            if (resvalue == true) {
                this.showHini(this.operesult, "提示_叫地主");
            } else {
                this.showHini(this.operesult, "提示_不抢");
            }
        } else if (oper == "myTackCards") {
            console.log("myTackCards" + sameside);
            if (sameside == true) {
                this.showHini(this.operesult, "不要");
            } else {
                this.showHini(this.operesult, "要不起");
            }
        }
    }
})
;
