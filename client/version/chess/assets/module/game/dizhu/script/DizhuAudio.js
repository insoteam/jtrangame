cc.Class({
    extends: cc.Component,

    properties: {},
    init: function () {
        this.basePath = "doudizhu/";
    },

    /**
     * 播放出牌音效
     * @param data
     * FIXME 这里的CARDS已在data中了，可考虑不再传进来
     */
    playAudioTackCard: function (data, cards) {
        //1、如果出牌不符合规则，先返回，以后看要不要加不符合规则的音效  FIXME出错牌音效
        if (!data.allow) {
            return;
        }

        let sexName = this._getSexName(data.sex);             //获得性别

        //2、播放音效
        if (data.donot == false) {          //出牌
            //读牌，或压死
            this._playByCardType(data.cardType, cards, sexName, data.first);

            //报余牌
            let self = this;
            if (data.cardsnum == 2 || data.cardsnum == 1) {
                setTimeout(function () {  //延时一下，防出牌与报余牌声音混在一起
                    //报余牌
                    self._playRemain(sexName, data.cardsnum);
                }, 500);
            }

        } else {  //不出
            this._playPass(sexName); //播放音效 要不起
        }
    },


    /**
     * 播放发牌声音.
     * 说明：不分男女
     */
    playAudiodeal: function () {
        //FIXME 发牌声音太长  这个还要找个合适的声音 声音太小
        //cc.beimi.audio.playYinXiao(this.basePath + "其它/" + "特别发牌" + ".ogg");
        cc.beimi.audio.playYinXiao(this.basePath + "playcard/" + "playcard" + ".wav");
    },

    /**
     * 抢地主  时的音效播放.
     * 说明：单元外调用
     */
    playAudioCatch: function (data) {
        let sexName = this._getSexName(data.sex);
        if (data.grab) {
            this._playAudioCatch(data.catchtimes, sexName);
        } else {
            this._playAudioNoCatch(data.catchtimes, sexName);
        }
    }
    ,
    /**
     *   整数范围随机 随机范围 0 ~ num - 1
     *   例： num=4 ,随机值为：0,1,2,3
     * @param num
     * @returns {number}
     */
    _ramdomForInt: function (num) {
        return Math.floor(Math.random() * num);
    }
    ,

    /**
     * 播放要不起
     * 日前一共有男女，各4组发音
     */
    _playPass: function (sexName) {
        cc.beimi.audio.playYinXiao(this.basePath + "tackcard/no/" + sexName + "_" + this._ramdomForInt(4) + ".ogg");
    }
    ,

    /**
     * 播放压死
     * 目前一共有男女，各三组发音
     */
    _playBig: function (sexName) {
        cc.beimi.audio.playYinXiao(this.basePath + "tackcard/have/" + sexName + "_" + this._ramdomForInt(3) + ".ogg");
    }
    ,

    /**
     * 播放抢地主声音
     * @param catchIdx 为第几轮抢地主   0：第一次 1：第二次 2:第三次 3：轮到第一个人，再来一次
     * @param sexName 性别男女字符 “0”:男 "1" 女
     */
    _playAudioCatch: function (catchIdx, sexName) {
        switch (catchIdx) {
            case 1:  //叫地主
                //cc.beimi.audio.playYinXiao(this.basePath + "catch/say/" + sexName + "_叫地主" + ".ogg");
                cc.beimi.audio.playYinXiao(this.basePath + "catch/say/" + sexName + "_say" + ".ogg");
                break;
            case 2:  //抢地主
            case 3:  //抢地主
            case 4:  //我抢
                cc.beimi.audio.playYinXiao(this.basePath + "catch/catch/" + sexName + "_" + catchIdx + ".ogg");
                break;
            default:
                break;
        }
    }
    ,

    /**
     * 播放不抢地主
     * @param catchIdx 为第几轮抢地主   0：第一次 1：第二次 2:第三次 3：轮到第一个人，再来一次
     * @param sexName 性别男女字符 “0”:男 "1" 女
     */
    _playAudioNoCatch: function (catchIdx, sexName) {
        switch (catchIdx) {
            case 1:  //不叫
                //cc.beimi.audio.playYinXiao(this.basePath + "catch/nosay/" + sexName + "_不叫" + ".ogg");
                cc.beimi.audio.playYinXiao(this.basePath + "catch/nosay/" + sexName + "_nosay" + ".ogg");
                break;
            case 2:
            case 3:
            case 4:  //不抢
                //cc.beimi.audio.playYinXiao(this.basePath + "catch/nocatch/" + sexName + "_不抢" + ".ogg");
                cc.beimi.audio.playYinXiao(this.basePath + "catch/nocatch/" + sexName + "_nocatch" + ".ogg");
                break;
            default:
                break;
        }
    }
    ,

    /**
     * 获得姓别对应的中文
     * @param sexStr 0：男 1：女
     * @param sexStr
     * @return 返回"男"或者"女"
     * FIXME javascript 有没有 sexStr=="0"?"男":"女"
     */
    _getSexName: function (sexStr) {
        if (sexStr == "0") {
            //return "男";
            return "man";
        } else {
            //return "女";
            return "wman";
        }

    }
    ,
    /**
     * 播放具体播音
     * @param catchIdx
     */
    _playAudio: function (path) {
        cc.beimi.audio.playYinXiao(this.basePath + path + ".ogg");
    }
    ,

    /**
     * 播放具体播音
     * 规则：1、单，对，三正常播音，2、3以上第一家正常播音，第二家开始随机播压死
     * @param cardType  牌型
     * @param cards     牌数组
     * @param sexName   性别男女
     * @param isFirst   是否是一轮的开始
     */
    _playByCardType: function (cardType, cards, sexName, isFirst) {
        cc.log("enter _playByCardType");
        //传入出牌类型 是个对象
        //默认男音
        cc.log(cardType);

        switch (cardType.cardTypeEnum) {
            case "SINGLE":                          //单张
                var maxvalue = cardType.maxValue;   // cardtype.maxValue 为最大组，最大值
                if (cardType.maxValue == 13) {      //区分大小王    //FIXME 这个考虑是否放在后台一起写了，也可不用特殊处理大王压小王的情况
                    if (cards[0] == 53) {
                        maxvalue = maxvalue + 1;    //大王=14
                    }
                }
                cc.beimi.audio.playYinXiao(this.basePath + "tackcard/single/" + sexName + "_" + maxvalue + ".ogg");
                break;
            case "SINGLE_LINK":                     //顺子
                if (isFirst) {
                    //this._playAudio("tackcard/other/" + sexName + "_" + "顺子");
                    this._playAudio("tackcard/other/" + sexName + "_" + "SINGLE_LINK");
                } else
                    this._playBig(sexName);
                break;
            case "PAIR":                            //两张
                cc.beimi.audio.playYinXiao(this.basePath + "tackcard/pair/" + sexName + "_" + cardType.maxValue + ".ogg");
                break;
            case "PAIR_LINK":
                if (isFirst)
                //cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "连对.ogg");
                    cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "PAIR_LINK.ogg");
                else
                    this._playBig(sexName);              //播放音效 大你 压死 等
                break;
            case "THREE":
                cc.beimi.audio.playYinXiao(this.basePath + "tackcard/three/" + sexName + "_" + cardType.maxValue + ".ogg");
                break;
            case "THREE_SINGLE":
                if (isFirst)
                //cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "三带一" + ".ogg");
                    cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "THREE_SINGLE" + ".ogg");
                else
                    this._playBig(sexName);              //播放音效 大你 压死 等
                break;
            case "THREE_PAIR":
                if (isFirst)
                //cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "三带一对" + ".ogg");
                    cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "THREE_PAIR" + ".ogg");
                else
                    this._playBig(sexName);              //播放音效 大你 压死 等
                break;
            case "THREE_LINK":
            case "THREE_LINK_SINGLE":
            case "THREE_LINK_PAIR":
                if (isFirst)
                //cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "飞机" + ".ogg");
                    cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "THREE_LINK_PAIR" + ".ogg");
                else
                    this._playBig(sexName);              //播放音效 大你 压死 等
                break;
            case "FOUR_TWOSINGLE":
                if (isFirst)
                //cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "四带二" + ".ogg");
                    cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "FOUR_TWOSINGLE" + ".ogg");
                else
                    this._playBig(sexName);              //播放音效 大你 压死 等
                break;
            case "FOUR_LINK_TWOSINGLE":
                if (isFirst)
                // cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "四带两对" + ".ogg");
                    cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "FOUR_TWOPAIR" + ".ogg");
                else
                    this._playBig(sexName);              //播放音效 大你 压死 等
                break;
            case "BOM":
                cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "BOM" + ".ogg");
                cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + "SPECIL_BOM" + ".ogg");
                break;
            case "BOM_WANG":
                cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + sexName + "_" + "BOM_WANG" + ".ogg");
                cc.beimi.audio.playYinXiao(this.basePath + "tackcard/other/" + "SPECIL_BOM" + ".ogg");
                break;
            default:
                //以上牌型以外的，就播压死
                this._playBig(sexName);   //播放音效 大你 压死 等
                break;
        }
    }
    ,
    /**
     *报余牌
     * @param iSex 性别 ：0 为男 1为女
     * @param nRemain 余牌数 2,1
     */
    _playRemain: function (sexName, nRemain) {
        if (nRemain == 2 || nRemain == 1)   //只允许在剩2,1张牌时报牌  防乱调用
            cc.beimi.audio.playYinXiao(this.basePath + "tackcard/saycard/" + sexName + "_" + "saycard" + nRemain + ".ogg");
    }
    ,


    onLoad: function () {

    }
})
;
