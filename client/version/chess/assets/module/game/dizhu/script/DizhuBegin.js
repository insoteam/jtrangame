/**
 * 斗地主游戏主入口
 */
var beiMiCommon = require("BeiMiCommon");
cc.Class({
    extends: beiMiCommon,

    properties: {
        gamebtn: {
            default: null,
            type: cc.Node
        },
        handCardsPanel: { //手牌的放置位置
            default: null,
            type: cc.Node
        },
        lastCardsPanel: {   //底牌显示区
            default: null,
            type: cc.Node
        },
        lastCardsRatioPanel: { //底牌倍数
            default: null,
            type: cc.Node
        },
        ratioLable: {        //底部倍数
            default: null,
            type: cc.Label
        },
        waitting: {
            default: null,
            type: cc.Prefab
        },
        summary: {
            default: null,
            type: cc.Prefab
        },
    },

    // use this for initialization
    onLoad: function () {
        //初始化全局控制
        var DizhuAudio = require("DizhuAudio");
        cc.beimi.dizhuAudio = new DizhuAudio();
        cc.beimi.dizhuAudio.init();

        //本游戏控制
        this.players = [];      //存放其它两玩家对象
        this.handCards = [];   //当前玩家手牌对象
        this.lastHandCards = [];   //底牌数据存放
        this.lastCardsPanel.active = false;  //底牌显示区不显示
        this.lastCardsRatioPanel.active = false;
        this.gamebtn.active = true;

        //输赢记分
        this.summaryNode = cc.instantiate(this.summary);
        this.summaryNode.active = false;

        this.isRegisted = false;

        if (cc.beimi != null && cc.beimi.gamestatus != null && cc.beimi.gamestatus == "playing") {
            this.gamebtn.active = false;            //开始按钮不显示
            //恢复数据  其实就是重新加入牌局
            this.recovery();
        }
    },

    testconnect: function () {
        if (cc.sys.isNative) {
            cc.log("i is Native");
            cc.beimi.net = SocketIO;
        } else {
            cc.log("i is web");
            cc.beimi.net = require("socket.io");
        }

        cc.log("i create netSocket 5");
        cc.beimi.hSocket = cc.beimi.net.connect(cc.beimi.http.wsURL + "/jtrangame");
        cc.beimi.hSocketConnected = false;
        let self = this;
        cc.beimi.hSocket.on("connect", function (result) {
            cc.beimi.hSocketConnected = true;
            cc.log("恭喜连霎成功");
            var data = self.parse(result);
            cc.log(data);
            self.testemit();

        });
    },
    testemit: function () {
        cc.log("i emit test");
        var param = {
            token: cc.beimi.authorization,
            playway: '402888815e21d735015e21d995680000',
            //playway: cc.beimi.playway,
            orgi: cc.beimi.user.orgi
        };
        cc.beimi.hSocket.emit("joinroom", JSON.stringify(param));
    },

    begin: function () {  //正常开局
        //this.testconnect();
        //this.testemit();
        this.initgame(false);
        this.statictimer("正在匹配玩家", 5);

    },
    /**明牌开局*/
    opendeal: function () {
        //this.testemit();
        this.initgame(false);
        this.statictimer("正在匹配玩家", 5);
    },
    recovery: function () {
        this.initgame(false);
        this.statictimer("正在恢复数据，请稍候", 5);
    },
    /**重新玩一局*/
    replay: function (opendeal) {  //重新玩一局
        this.cleardesk();           //清除桌面
        this.initgame(opendeal);         //加入房间
        this.statictimer("正在匹配玩家", 5);
    },
    socket: function () {
        let self = this;
        cc.log("get socket");
        if (cc.beimi.socketstatus == "disconnect") {
            cc.log("connecting ....");
            cc.beimi.socketstatus = "connecting";
            cc.log(cc.beimi.http.wsURL);
            cc.beimi.socket = window.io.connect(cc.beimi.http.wsURL + '/jtrangame');
            cc.beimi.socket.on("disconnect", function () {
                cc.log("断开连接了.....");
                cc.beimi.socketstatus = "disconnect";
            });
            cc.beimi.socket.on("connect", function (result) {
                cc.log("连接成功");
                self.registCommand();
                if (cc.beimi.socketstatus == "connecting") {
                    //开始游戏
                    self.doJoinRoom();
                }
                cc.beimi.socketstatus = "connected";

            });
        }
        return cc.beimi.socket;
    },
    /**
     * 注册事件
     * FIXME 代码有点乱，先这样，回头一起整理
     */
    registCommand: function () {
        let self = this;
        //封装一下命令回调
        var register = function (commType, callbackfn) {
            if (!cc.sys.isNative) {   //web端防止多次注册监听，产生翻倍响应
                cc.beimi.socket.removeAllListeners(commType);
            }
            cc.beimi.socket.on(commType, function (result) {
                if (self.isRegisted == true) {
                    cc.log("receive common:" + commType);
                    cc.beimi.gamestatus = "playing";
                    var data = self.parse(result);
                    cc.log(data);
                    callbackfn(self, data);
                }
            });
        };

        register("joinroom", self.onJoinRoom);           //加入房间
        register("players", self.onPlayers);             //玩家
        register("catch", self.onCatch);                 //开始抢地主
        register("catchfail", self.onCatchFail);         //流局，无人想当地主，需要重新发牌
        register("catchresult", self.onCatchResult);     //通知抢地主结果
        register("lasthands", self.onLasthands);         //底牌
        register("takecards", self.onTackcards);         //出牌
        register("play", self.onPlay);                   //发牌
        register("bomb", self.onBomb);                   //bom
        register("allcards", self.onAllcards);           //输赢结算
        register("recovery", self.onRecovery);             //恢复数据
        register("goldnotenough", self.onGoldNotEnough); //goldnotenough 金币不足
    },
    initgame: function (opendeal) {
        let self = this;
        this.lastCardsPanel.active = false;     //底牌不显示
        this.gamebtn.active = false;
        if (self.ready()) {
            self.game = self.getCommon("DizhuDataBind");
            if (cc.beimi && cc.beimi.authorization && cc.beimi.user) {
                self.isRegisted = true;
                if (cc.beimi.socketstatus && cc.beimi.socketstatus == "connected") {
                    self.registCommand();
                    self.doJoinRoom();                                 //发送加入加入房间
                } else {
                    self.socket();  //启动socket ，只有在disconnect 时才有效，且connect时自动doJoinRoom
                }
            }

        }
    },

    /**
     * 清除桌面，回复到开局时状态
     *
     */
    cleardesk: function () {
        cc.log("on enter cleardesk");
        cc.log(this);
        let self = this;
        //清除其它玩家所出的牌
        if (self.players) {
            cc.log("clear players ...");
            for (var i = 0; i < self.players.length; i++) {
                self.players[i].js.clearLastTakeCards(self.game);
                self.players[i].js.resetCardCount(0);  //手牌张数设置为零

                //附加不显示抢地主结果
                self.players[i].js.hideresult();
                self.players[i].js.showDizhuFlag(false);
            }
        }

        if (self.game) {
            //清除用户所出的牌
            cc.log("clear clearUserTackCards ...");
            self.game.clearUserTackCards();
            //设置用户按钮、等待不可用
            cc.log("clear set control  ...");
            self.game.setControlNoActive();
            cc.log("clear game desk  ...");
            self.game.clearDesk();
            //清除用户手牌（余牌） FIXME 这个居然不行，会报好多无父的错误，可能把父对象都释放了
            cc.log("clear handCards  ...");
            for (var i = 0; i < this.handCards.length; i++) {
                self.game.pokerpool.put(self.handCards[i]);//回收回缓冲池
            }
            //self.handCardsPanel.removeAllChildren();
            self.handCards.splice(0, self.handCards.length);
            //清除底牌
            cc.log("clear clearLastCards  ...");
            self.clearLastCards();

        }

        self.lastCardsPanel.active = false;  //底牌不显示
        self.lastCardsRatioPanel.active = false; //底牌倍数不显示

        self.changRatio(self, 15);           //重新开始时设置默认倍数  FIXME 这个好象应该 根据房间来设置比较好
    }
    ,


//响应加入房间的服务器事件
    onJoinRoom: function (context, data) {
        cc.log("onJoinRoom");
        let self = context;
        if (data.id && data.id == cc.beimi.user.id) {
            //本人
            cc.log("本人加入房间");
        } else {
            //其他玩家加入，初始化
            cc.log("其它人加入房间");
            var inroom = false;
            for (var i = 0; i < self.players.length; i++) {
                if (self.players[i].js.userid == data.id) {
                    inroom = true;
                }
            }
            if (inroom == false) {
                self.newplayer(self.players.length, self, data);
            }
        }
    }
    ,
    onPlayers: function (context, data) {
        cc.log("onPlayers");
        let self = context;
        if (data.length > 1) {   //FIXME 这个为什么要>1 好象没什么意思
            var inx = 0;
            for (var i = 0; i < data.length; i++) {
                var player = data[i];
                var inroom = false;
                for (var j = 0; j < self.players.length; j++) {
                    if (self.players[j].js.userid == player.id) {
                        inroom = true;
                    }
                }
                if (inroom == false && player.id !== cc.beimi.user.id) {
                    self.newplayer(self.players.length, self, player);
                }
            }
        }
    }
    ,
    onCatch: function (context, data) {
        cc.log("onCatch");
        let self = context;
        if (data.userid == cc.beimi.user.id) {    //该我抢
            self.game.catchtimer(15);
        } else {                              //该别人抢
            for (var inx = 0; inx < self.players.length; inx++) {
                var render = self.players[inx].js;
                if (render.userid && render.userid == data.userid) {
                    render.catchtimer(15);
                    break;
                }
            }
        }
    }
    ,
    /**
     * 没人要当地主 流局
     * onCatchFail
     */
    onCatchFail: function (context, data) {
        cc.log("enter onCatchFail");
        cc.log(data);
        let self = context;
        //清除桌面
        self.cleardesk();
        self.statictimer("正在重新发牌，请稍候", 5);
    }
    ,
    /**
     * 处理服务端消息：抢地主 （抢地主，或不抢地主，都会收到catchresult消息)
     * @param data
     * @param context
     */
    onCatchResult: function (context, data) {
        cc.log("enter on Catchresult");
        cc.log(data);

        let self = context;

        /**修改倍数*/
        self.changRatio(context, data.ratio);


        if (data.userid == cc.beimi.user.id) {    //我抢到地主
            self.game.catchresult(data);
        } else {                                    //别人抢到地主
            setTimeout(function () {
                self.getPlayer(data.userid).js.catchresult(data);
            }, 1500);
        }

        //播放音效
        cc.beimi.dizhuAudio.playAudioCatch(data);  //音效
    }
    ,
    /**
     * 发底牌 服务端消息响应
     * @param data
     * @param context
     */
    onLasthands: function (context, data) {
        cc.log("on Enter onLastHands");
        cc.log(data);
        let self = context;
        cc.log("show ration");
        cc.log(data.ratio);
        context.changRatio(context, data.ratio);
        /**
         * 底牌 ， 顶部的 三张底牌显示区域
         */
        let lasthandCards = self.decode(data.lasthands);
        cc.log(self.lastHandCards.length);
        self.showLastHandCardCorrectSide(lasthandCards);


        //底牌倍率显示
        if (data.lastHandRatio > 1) {
            self.lastCardsRatioPanel.active = true;
            self.lastCardsRatioPanel.getChildByName("lastHandRatioLable").getComponent(cc.Label).string = data.lastHandRatio + "倍";
        }

        /**
         * 当前玩家的 底牌处理
         */
        if (data.userid == cc.beimi.user.id) {
            self.game.lasthands(self, self.game, data);
            /**
             * 隐藏 其他玩家的 抢地主/不抢地主的 提示信息
             */
            for (var inx = 0; inx < self.players.length; inx++) {
                self.players[inx].js.hideresult();
            }

            for (var i = 0; i < lasthandCards.length; i++) {
                //发牌三张分别插在三块不同位置
                let cardObj = self.playcard(self.game, self, 2 * 300 + (6 + i) * 50 - 300, lasthandCards[i]);
                cardObj.js.showCorrectSide();
                self.registerProxy(cardObj);
            }
            self.game.playtimer(self.game, 25);
            self.game.playbtn.getChildByName("不出").active = false;
        } else {  //其它玩家的底牌处理
            //设置地主、关闭抢地主
            for (var inx = 0; inx < self.players.length; inx++) {
                self.players[inx].js.lasthands(self, self.game, data);  //设置底牌处理
            }
        }

        //当前玩家 起到排序
        self._handCardUpdate();
    }
    ,
    /**
     * 更新牌面 主要是用于有顺显示 防重叠无序等问题
     * @param pockerCardArray 对象数组pocker
     * @param panel   显示面板
     * @param isShowCorrectSide  是否显示正面
     * @param scale  缩放倍数
     * @private
     * FIXME 需要一些工具类
     */
    _cardsUpdate:function (pockerCardArray, panel, isShowCorrectSide, scale) {
        var sort = function bubbleSort(arr) {   //冒泡排序
            for (var i = 0; i < arr.length - 1; i++) {
                for (var j = 0; j < arr.length - 1; j++) {
                    if (arr[j + 1].card > arr[j].card) {
                        let temp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
            return arr;
        }

        //获得牌值
        var tmpCards = sort(pockerCardArray);
        var card = [];
        for (var inx = 0; inx < tmpCards.length; inx++) {
            card[card.length] = tmpCards[inx].card;
        }
        //清除原有对象 说明：这里先清掉，下面再重新给上，这个对于handCardsPanel这种对象cocos没处理好，这样处理才能正常排序
        for (var i = 0; i < pockerCardArray.length; i++) {
            this.game.pokerpool.put(pockerCardArray[i]);//回收回缓冲池
        }
        pockerCardArray.splice(0, pockerCardArray.length);  //删除底牌

        //重新给手牌面板赋值
        for (var inx = 0; inx < card.length; inx++) {
            let currpoker = this.game.createPoker(panel);
            this.registerProxy(currpoker);
            currpoker.card = card[inx];
            currpoker.js.setCard(card[inx]);
            if (isShowCorrectSide == true)
                currpoker.js.showCorrectSide();  //显示正面
            else
                currpoker.js.showOpposite();  //显示背面

            currpoker.setScale(scale, scale);
            //currpoker.setPositionY(-180);
            currpoker.zIndex = 54 - card[inx];
            pockerCardArray[pockerCardArray.length] = currpoker;
        }
    },
    //玩家手牌重新排序
    _handCardUpdate: function () {
        this._cardsUpdate(this.handCards, this.handCardsPanel, true, 1);
    }
    ,
    onAllcards: function (context, data) {
        cc.log("on all cards enter");
        cc.beimi.gamestatus = "notready";
        let self = context;
        //展示未出完牌的玩家手牌
        for (var i = 0; i < data.players.length; i++) {
            var player = data.players[i];
            //输牌者剩余牌展示
            if (player.playuser != cc.beimi.user.id) {
                var cards = self.decode(player.cards);        //解释DATA，获得玩家剩余手牌
                var playObj = self.getPlayer(player.playuser);
                playObj.js.refreshGoldcoins(player.goldcoins);  //设置金币
                for (var inx = 0; inx < cards.length; inx++) {
                    playObj.js.tackCards(self.game, self, cards.length, cards, data); //设置玩家最后出牌，将所有手牌全出了
                }
            } else {  //玩家自已 设置金币
                cc.beimi.user.goldcoins = player.goldcoins;
                self.game.refreshGoldcoins();
            }
        }

        setTimeout(function () {
            cc.log("onAllcards settimeout2");
            self.summaryNode.parent = self.root();
            self.summaryNode.setPosition(0, 0);
            self.summaryNode.active = true;
            var dizhuWinLossRander = self.summaryNode.getComponent("DizhuWinLossRander")
            if (dizhuWinLossRander) {
                dizhuWinLossRander.initRander(self, data, cc.beimi.user);
            } else {
                cc.log("no find DizhuWinLossRander");
            }
        }, 2000);
    }
    ,

    /**
     * 响应后台消息 恢复牌局
     * @param context
     * @param data
     */
    onRecovery: function (context, data) {
        cc.log("onRecovery");
        let self = context;
        cc.log(self);
        if (self.waittimer != null) {
            let timer = self.waittimer.getComponent("BeiMiTimer");
            if (timer) {
                timer.stop(self.waittimer);
            }
        }

        self.changRatio(self, data.ratio);

        self.showLastCards(self.game, self, 3, 0);  //这里给的底牌还是假的，只是起到给底牌数组创建对象的做用
        self.lastCardsPanel.active = true;

        //将玩家的牌恢复
        var mycards = self.decode(data.player.cards);
        for (var inx = 0; inx < mycards.length; inx++) {
            let pokencard = self.playcard(self.game, self, inx * 50 - 300, mycards[inx]);
            self.registerProxy(pokencard);
            pokencard.js.showCorrectSide();
        }
        self._handCardUpdate();

        if (data.lasthands) {   //底牌已展示
            /**顶部三张底牌*/
            var lasthands = self.decode(data.lasthands);        //底牌解码后台数据
            self.showLastHandCardCorrectSide(lasthands);         //底牌 ， 顶部的 三张底牌显示区域

            /**设置地主标志*/
            if (data.banker == cc.beimi.user.id) {
                self.game.lasthands(self, self.game, data.data);
            } else {
                self.getPlayer(data.banker).js.lasthands(self, self.game, data.data);
            }
            //底牌倍率显示
            if (data.data.lastHandRatio > 1) {
                self.lastCardsRatioPanel.active = true;
                self.lastCardsRatioPanel.getChildByName("lastHandRatioLable").getComponent(cc.Label).string = data.data.lastHandRatio + "倍";
            }
        }
        /**
         * 恢复最后的出牌
         */
        if (data.last != null) {
            let lastcards = self.decode(data.last.cards);        //解析牌型
            if (data.last.userid == cc.beimi.user.id) {
                self.game.myTackCards(self.game, self, data.last.cardsnum, lastcards, data.last);
            } else {
                self.getPlayer(data.last.userid).js.tackCards(self.game, self, data.last.cardsnum, lastcards, data.last);
            }

            //下个玩家显示等待时钟   //FIXME 如果是本玩家的话， 时钟25可能不对
            if (data.nextplayer == cc.beimi.user.id) {
                self.game.playtimer(self.game, 25);
            } else {
                self.getPlayer(data.nextplayer).js.playtimer(self.game, 25);
            }
        }
        /**显示其它玩家手牌数*/
        if (data.cardsnum != null && data.cardsnum.length > 0) {
            for (var i = 0; i < data.cardsnum.length; i++) {
                self.getPlayer(data.cardsnum[i].userid).js.resetCardCount(data.cardsnum[i].cardsnum);
            }
        }
    }
    ,
    /**
     * 响应后台消息： 出牌
     * @param data
     * @param context
     * FIXME 这里data里的某些数据可能需要隐藏掉不发送到前台 1、节省流量，2、防做弊
     */
    onTackcards: function (context, data) {  //这里的DATA = 后台的Tastcard
        cc.log("on enter onTackcards");
        cc.log(data);

        let self = context;
        if (data.allow == true) {
            var tackCards;
            if (data.donot == false) {//出牌
                tackCards = self.decode(data.cards); //获得所出的牌
            }


            if (data.userid == cc.beimi.user.id) {       //等于当前用户
                cc.log("当前用户出牌.....................................")
                self.game.myTackCards(self.game, self, data.cardsnum, tackCards, data);
                self.game.selectedcards.splice(0, self.game.selectedcards.length); //清空 FIXME 这个有需要吗？
            } else {
                cc.log("其它用户出牌.....................................")
                cc.log(data.userid)
                self.getPlayer(data.userid).js.tackCards(self.game, self, data.cardsnum, tackCards, data);
            }

            if (data.nextplayer == cc.beimi.user.id) {    //如果下次还是自已出牌，那么显示玩家自已的等待画面
                self.game.playtimer(self.game, 25);
            } else {  //否则显示其它玩家的等待画面
                if (data.nextplayer != null) {
                    self.getPlayer(data.nextplayer).js.playtimer(self.game, 25);
                }
            }

            //播放出牌音效
            cc.beimi.dizhuAudio.playAudioTackCard(data, tackCards);
        } else {//出牌不符合规则，需要进行提示
            self.game.notallow.active = true;
            setTimeout(function () {
                self.game.notallow.active = false;
            }, 2000);
        }

    }
    ,
    /**
     * 发牌
     * 说明：响应服务器的发牌消息。 处理界面显示
     * @param data
     * @param context
     */
    onPlay: function (context, data) {  //开牌
        cc.log("onPlay....");
        cc.beimi.gamestatus = "playing";
        let self = context;
        var mycards = self.decode(data.player.cards);
        cc.log(mycards);

        //刷新金币 因为这里扣除了房间收费
        cc.beimi.user.goldcoins = data.player.goldcoins;
        self.game.refreshGoldcoins();

        //停止等待时钟
        if (self.waittimer) {
            let timer = self.waittimer.getComponent("BeiMiTimer");
            if (timer) {
                timer.stop(self.waittimer);
            }
        }

        let center = self.game.createPoker(self.root());
        let left = self.game.createPoker(self.root());
        let right = self.game.createPoker(self.root());
        center.setPosition(0, 200);
        left.setPosition(0, 200);
        right.setPosition(0, 200);

        center.setScale(1, 1);
        left.setScale(1, 1);
        left.setScale(1, 1);


        //完成分牌后的回调函数，处理分牌完成的后的事情
        var finished = cc.callFunc(function (target, param) {
            //界面上移除并放回缓冲池  发牌时的左中右三张牌
            param.current.stopAllActions();
            param.left.stopAllActions();
            param.right.stopAllActions();
            param.game.pokerpool.put(param.current);
            param.game.pokerpool.put(param.left);
            param.game.pokerpool.put(param.right);

            /**
             * 赋值，解码牌面
             */
            cc.log("handCards" + self.handCards.length);
            for (var i = 0; i < self.handCards.length; i++) {
                self.handCards[i].js.showCorrectSide();  //显示正面
                self.registerProxy(self.handCards[i]);   //FIXME 是否必须？
                self.handCards[i].setPositionY(-180);   // 强行再写死一次，防止乱串
            }

            param.self.lastCardsPanel.active = true;  //底牌面板可视  //FIXME 这是什么？
            self._handCardUpdate();  //手牌排序

        }, this, {game: self.game, self: self, left: left, right: right, current: center});

        //显示三张背面底牌，为抢地主做准备
        self.showLastCards(self.game, self, 3, 0);

        /**
         *  发牌动作，每次6张，本人保留总计17张，其他人发完即回收
         */
        setTimeout(function () {
            self.dealingCards(self.game, 6, self, 0, left, right, mycards);
            setTimeout(function () {
                self.dealingCards(self.game, 6, self, 1, left, right, mycards);
                setTimeout(function () {
                    self.dealingCards(self.game, 5, self, 2, left, right, mycards, finished);
                }, 500);
            }, 500);
        }, 0);

        //播放发牌音效
        cc.beimi.dizhuAudio.playAudiodeal();
    }
    ,
    onGoldNotEnough: function (context, data) {
        cc.log("on GoldNotEnough enter");
        cc.log(data);
        this.cleardesk();
        if (context.waittimer) {
            let timer = context.waittimer.getComponent("BeiMiTimer");
            if (timer) {
                timer.stop(context.waittimer);
            }
        }
        this.alert("金币不足");
    }
    ,
    onBomb: function (context, data) {
        cc.log("on Bomb enter");
        cc.log(data);
        /**
         * 修改倍率
         */
        context.changRatio(context, data.ratio);
        cc.log("on Bomb end");
    }
    ,


    getPlayer: function (userid) {
        for (var inx = 0; inx < this.players.length; inx++) {
            var player = this.players[inx];
            if (player.js.userid && player.js.userid == userid) {
                return player;
            }
        }
        return null;
    }
    ,

    /**
     * 分牌
     * @param game  Dizhudatabind
     * @param num  分三轮发牌效果，这里NUM指本次发牌数量
     * @param self dizhubegin
     * @param times
     * @param left
     * @param right
     * @param cards
     * @param finishedCallBack
     */
    dealingCards: function (game, num, self, times, left, right, cards, finishedCallBack) {
        /**
         * 处理当前玩家的 牌， 发牌 ，  17张牌， 分三次动作处理完成
         */
        for (var i = 0; i < num; i++) {

            var myCard;
            if (finishedCallBack == null && i <= num - 1)   //只有最后一张牌发完时才需要进行回调finished
                self.playcard(game, self, times * 300 + i * 50 - 300, cards[times * 6 + i]);
            else {
                self.playcard(game, self, times * 300 + i * 50 - 300, cards[times * 6 + i], finishedCallBack);
            }
            self.registerProxy(myCard);   //FIXME 没搞明白为什么  没这个就选不了牌了，真是怪
        }
        //左边右边玩家分牌效果
        self.otherplayer(left, 0, num, game, self);
        self.otherplayer(right, 1, num, game, self);

    }
    ,
    otherplayer: function (currpoker, inx, num, game, self) {
        if (inx == 0) {  //右玩家
            let seq = cc.sequence(
                cc.spawn(cc.moveTo(0.2, -350, 50), cc.scaleTo(0.2, 0.3, 0.3)), cc.moveTo(0, 0, 200), cc.scaleTo(0, 1, 1)
            );
            currpoker.runAction(seq);
        } else {       //左玩家
            let seq = cc.sequence(
                cc.spawn(cc.moveTo(0.2, 350, 50), cc.scaleTo(0.2, 0.3, 0.3)), cc.moveTo(0, 0, 200), cc.scaleTo(0, 1, 1)
            );
            currpoker.runAction(seq);
        }

        var render = self.players[inx].js;
        for (var i = 0; i < num; i++) {
            render.countcards(1);
        }
    }
    ,
    /**
     * 发牌
     * @param game
     * @param self
     * @param posx
     * @param card
     * @returns {*}
     */
    playcard: function (game, self, posx, card, func) {
        let currpoker = game.createPoker(self.handCardsPanel);
        currpoker.js.setCard(card);
        currpoker.card = card;  //FIXME 这个似乎没鸟用
        currpoker.setLocalZOrder(54 - currpoker.card);  //设置层关系
        cc.log(posx + "- card" + card);
        currpoker.setPosition(0, 200);
        currpoker.setScale(1, 1);
        self.handCards[self.handCards.length] = currpoker;
        if (func == null) {
            let action1 = cc.moveTo(0.2, posx, -180);
            currpoker.runAction(action1);
        } else {
            let action2 = cc.sequence(cc.moveTo(0.2, posx, -180), func);
            currpoker.runAction(action2);
        }

        return currpoker;
    }
    ,
    /**
     * 清除底牌的对象，还给缓冲池
     */
    clearLastCards: function () {
        for (var i = 0; i < this.lastHandCards.length; i++) {
            this.lastHandCards[i].setScale(0.5, 0.5);
            this.game.pokerpool.put(this.lastHandCards[i]);//回收回缓冲池
        }

        //this.lastCardsPanel.removeAllChildren();
        this.lastHandCards.splice(0, this.lastHandCards.length);  //删除底牌
    },
    clearHandCards: function (cardArray) {
        for (var i = 0; i < cardArray.length; i++) {
            cardArray[i].setScale(0.5, 0.5);
            this.game.pokerpool.put(cardArray[i]);//回收回缓冲池
        }
        cardArray.splice(0, cardArray.length);  //删除底牌
    }
    ,
    registerProxy: function (myCard) {
        if (myCard) {
            myCard.js.proxy(this.game);
        }
    }
    ,

    reordering: function (self) {
        for (var i = 0; i < self.handCards.length; i++) {
            self.handCards[i].parent = self.handCardsPanel;
            //self.handCards[i].setPosition(i * 50 - 300, -180);
        }
    }
    ,
    newplayer: function (inx, self, data) {
        var isRight = false;

        if (self.players.length == 1) {//第一个其它玩家坐右边
            isRight = true;
        }

        var pos = cc.v2(520, 100);
        if (isRight == false) {
            pos = cc.v2(-520, 100);
        }

        let game = self.game;
        cc.log("newplayer x");
        let player = self.game.createPlayer(self.root());

        if (player) {
            cc.log("newplayer ok");
            player.setPosition(pos);
            player.js.initplayer(data, isRight);
            self.players[inx] = player;   //赋给数组  //FIXME 这里又放一个数组，似乎多余
        }
    },

    /**
     * 加入房间
     */
    doJoinRoom: function () {
        cc.log("doJoinRoom");
        var param = {
            token: cc.beimi.authorization,
            playway: '402888815e21d735015e21d995680000',
            //playway: cc.beimi.playway,
            orgi: cc.beimi.user.orgi
        };
        cc.log("do joinroom emit");
        cc.beimi.socket.emit("joinroom", JSON.stringify(param));   //发送加入房间
    }
    ,

    /**
     * 不抢/叫地主
     */
    doGivupCatch: function () {

        this.socket().emit("giveup", JSON.stringify(""));
    }
    ,
    /**
     * 抢/叫地主触发事件
     */
    doCatch: function () {
        cc.log("docat ch");
        if (this.ready()) {
            let socket = this.socket();
            cc.log("docatch send");
            socket.emit("docatch", JSON.stringify(""));
            cc.log("docatch send end");
        }
    }
    ,
    /**
     * 底牌处理  给存底牌的数据三张牌，默粉
     * 说明：这里显示的是背面底牌
     * @param game
     * @param self
     * @param num
     * @param card
     */
    showLastCards: function (game, self, num, card) {//发三张底牌
        for (var i = 0; i < num; i++) {
            var width = i * 80 - 80;
            let currpoker = game.createPoker(this.lastCardsPanel);
            currpoker.js.setCard(card); //FIXME 为何是调用beimicard？
            currpoker.card = card;
            currpoker.setPosition(width, 0);
            currpoker.setScale(0.5, 0.5);
            self.lastHandCards[self.lastHandCards.length] = currpoker;
        }
    }
    ,
    /**
     * 给三张底牌赋值，并显示
     * @param lasthandCards
     */
    showLastHandCardCorrectSide: function (lasthandCards) {
        for (var i = 0; i < this.lastHandCards.length; i++) {
            this.lastHandCards[i].js.setCard(lasthandCards[i]);
            this.lastHandCards[i].js.showCorrectSide();
        }
    }
    ,
    /**
     * 修改倍率
     */
    changRatio: function (context, ratio) {
        cc.log("on enter changRatio");
        if (context.ratioLable) {
            cc.log("ratio=" + ratio);
            context.ratioLable.string = ratio + "倍";
        }
    }
    ,
    /**
     * 出牌
     */
    doPlayCards: function () {
        if (this.ready()) {
            this.game.selectedcards.splice(0, this.game.selectedcards.length);//清除选牌数组
            for (var i = 0; i < this.handCards.length; i++) {
                var card = this.handCards[i];
                var temp = card.js;
                if (temp.selected == true) {
                    this.game.selectedcards.push(temp.card);   //添加
                }
                temp.unselected();
            }
            this.socket().emit("doplaycards", this.game.selectedcards.join());
        }
    },
    /**
     * 不出牌
     */
    noCards: function () {
        this.socket().emit("nocards", JSON.stringify(""));  //发送给服务器，不出牌
    }
    ,

    statictimer: function (message, time) {
        this.waittimer = cc.instantiate(this.waitting);
        this.waittimer.parent = this.root();

        let timer = this.waittimer.getComponent("BeiMiTimer");
        if (timer) {
            timer.init(message, time, this.waittimer);
        }
    }
    ,

    onDestroy: function () {
        this.isRegisted = false;
    }
})
;
